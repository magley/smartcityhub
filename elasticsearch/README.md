# Installing

1) Download elasticsearch [here](https://www.elastic.co/downloads/elasticsearch).

2) Extract zip anywhere.

3) Disable SSL. Locate `/config/elasticsearch.yml`, and find all security fields (there should be 4 of them uncommented, default to `true` - set it to `false`).

4) Reload gradle to fetch all dependencies.

5) Run `elasticsearch.bat` in `/bin`.

# Development guide (integration with Spring Boot)

The class `ElasticSearchConfig` creates a bean for communicaitng with the elasticsearch service and database. 

You can write Repo interfaces like with SQL data (extend `ElasticsearchRepository`).
Elasticsearch also supports [query methods](https://docs.spring.io/spring-data/elasticsearch/reference/elasticsearch/repositories/elasticsearch-repository-queries.html).

Every interface has to be "declared". We have to help `ElasticSearchConfig` in finding the interfaces by adding the package path to the
`@EnableElasticsearchRepositories` annotation.

Elasticsearch works with documents, so instead of `@Entity`, it's `@Document`. The fields you want to search by should probably be `text` fields, as those provide the best performance.

When saving an entity to the SQL database, you have to save it to elasticsearch as well.