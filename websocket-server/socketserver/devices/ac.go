package devices

import (
	"context"
	"fmt"
	"smartcityhub/socketserver/common"
	"smartcityhub/socketserver/settings"
	"smartcityhub/socketserver/userdata"
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
)

type AcMode string
type AcCommandType string

const (
	Heat AcMode = "heat"
	Cool AcMode = "cool"
	Auto AcMode = "auto"
	Fan  AcMode = "fan"

	Temperature AcCommandType = "temperature"
	IsOn        AcCommandType = "is_on"
	Mode        AcCommandType = "mode"
	Config      AcCommandType = "config"
)

var (
	AcModeMap = map[string]AcMode{
		"heat": Heat,
		"cool": Cool,
		"auto": Auto,
		"fan":  Fan,
	}

	AcCommandTypeMap = map[string]AcCommandType{
		"temperature": Temperature,
		"is_on":       IsOn,
		"mode":        Mode,
		"config":      Config,
	}
)

// TODO: Instead of this 'maybe' stuff, try pointers.

type AcCommandOneOf struct {
	IsOn        bool    `json:"isOn"`
	Mode        AcMode  `json:"mode"`
	Temperature float64 `json:"temperature"`
	Config      string  `json:"config"`
}

type AcCommand struct {
	Timestamp time.Time   `json:"timestamp"`
	User      common.User `json:"user"`
	Status    bool        `json:"status"`
	UUID      string      `json:"uuid"`

	Type  AcCommandType  `json:"type"`
	OneOf AcCommandOneOf `json:"oneOf"`
}

// _convertStringToBool converts a string encoding a boolean value into a proper
// bool. A tag in InfluxDB can ONLY be a string. So when a simulator sends a tag
// value with `False` (like in Python), it the value is a string literal "False"
// which Go cannot automatically convert into `bool`.
func _convertStringToBool(s string) bool {
	if s == "False" || s == "false" || s == "0" {
		return false
	}
	if s == "True" || s == "true" || s == "1" {
		return true
	}
	return false
}

func maybeBool(m map[string]interface{}, key string) bool {
	if m[key] == nil {
		return false
	}
	return _convertStringToBool(m[key].(string))
}
func maybeAcMode(m map[string]interface{}, key string) AcMode {
	if m[key] == nil {
		return Cool
	}
	return AcModeMap[m[key].(string)]
}
func maybeFloat64(m map[string]interface{}, key string) float64 {
	if m[key] == nil {
		return 0
	}
	f, ok := m[key].(float64)
	if !ok {
		i := m[key].(int64)
		return float64(i)
	}

	return f
}
func maybeString(m map[string]interface{}, key string) string {
	if m[key] == nil {
		return ""
	}
	return m[key].(string)
}

func NewAcState(m map[string]interface{}) AcCommand {
	userId := m["user_id"].(int64)
	maybeUser := userdata.GetUserById(userId)

	return AcCommand{
		Timestamp: m["_time"].(time.Time),
		User:      maybeUser,
		Status:    m["status"].(bool),
		UUID:      m["uuid"].(string),
		Type:      AcCommandTypeMap[m["type"].(string)],
		OneOf: AcCommandOneOf{
			IsOn:        maybeBool(m, "is_on"),
			Mode:        maybeAcMode(m, "mode"),
			Temperature: maybeFloat64(m, "temperature"),
			Config:      maybeString(m, "config"),
		},
	}
}

// AcGetLastState returns the last SUCCESSFUL command of each type for the device.
// Each command is a `AcCommand` in an array. From therein you can examine the
// data and reconstruct the AC state.
func AcGetLastState(deviceId int64) []AcCommand {
	client := influxdb2.NewClient(settings.Cfg.InfluxDB.Url, settings.Cfg.InfluxDB.Token)
	defer client.Close()
	queryClient := client.QueryAPI(settings.Cfg.InfluxDB.Org)

	query := `
	from(bucket: "%s")
		|> range(start: -1d)
		|> filter(fn: (r) => r._measurement == "%s")
		|> drop(columns: ["_start", "_stop", "host", "topic"])
		|> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
		|> filter(fn: (r) => r.device_id == "%d")
		|> group(columns: ["type"])
		|> sort(columns: ["_time"])
		|> map(fn: (r) => ({ r with status: bool(v: r.status) }))
		|> filter(fn: (r) => r.status == true)
		|> last(column: "_time")
		|> group(columns: ["_measurement"])
		|> drop(columns: ["_measurement", "device_id"])
		|> map(fn: (r) => ({ r with status: bool(v: r.status) }))
	`
	query = fmt.Sprintf(query, settings.Cfg.InfluxDB.BucketNvt, "ac_w", deviceId)
	result, err := queryClient.Query(context.Background(), query)

	stateArray := make([]AcCommand, 0)
	if err != nil {
		panic(err)
	} else {
		for result.Next() {
			values := result.Record().Values()
			val := NewAcState(values)
			stateArray = append(stateArray, val)
		}
	}

	return stateArray
}

// AcGetLastCommands returns the last N commands processed by the device. They
// can be both successful and unsuccessful.
func AcGetLastCommands(deviceId int64) []AcCommand {
	const (
		N = 10
	)

	client := influxdb2.NewClient(settings.Cfg.InfluxDB.Url, settings.Cfg.InfluxDB.Token)
	defer client.Close()
	queryClient := client.QueryAPI(settings.Cfg.InfluxDB.Org)

	query := `
	from(bucket: "%s")
		|> range(start: -1d)
		|> filter(fn: (r) => r._measurement == "%s")
		|> drop(columns: ["_start", "_stop", "host", "topic"])
		|> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
		|> filter(fn: (r) => r.device_id == "%d")
		|> group(columns: ["_measurement"])
		|> drop(columns: ["_measurement", "device_id"])
		|> sort(columns: ["_time"], desc: true)
		|> limit(n: %d)
		|> map(fn: (r) => ({ r with status: bool(v: r.status) }))
	`

	query = fmt.Sprintf(query, settings.Cfg.InfluxDB.BucketNvt, "ac_w", deviceId, N)
	result, err := queryClient.Query(context.Background(), query)

	stateArray := make([]AcCommand, 0)
	if err != nil {
		panic(err)
	} else {
		for result.Next() {
			values := result.Record().Values()
			val := NewAcState(values)
			stateArray = append(stateArray, val)
		}
	}

	return stateArray
}
