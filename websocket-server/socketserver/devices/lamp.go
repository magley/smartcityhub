package devices

import (
	"context"
	"fmt"
	"smartcityhub/socketserver/common"
	"smartcityhub/socketserver/settings"
	"smartcityhub/socketserver/userdata"
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
)

type LampReading struct {
	Illuminance float64   `json:"illuminance"`
	Timestamp   time.Time `json:"timestamp"`
}

type LampMode string
type LampCommandType string

const (
	ManualLamp LampMode        = "Manual"
	AutoLamp   LampMode        = "Auto"
	IsOnLamp   LampCommandType = "is_on"
	ModeLamp   LampCommandType = "mode"
)

var (
	LampModeMap = map[string]LampMode{
		"Manual": ManualLamp,
		"Auto":   AutoLamp,
	}

	LampCommandTypeMap = map[string]LampCommandType{
		"is_on": IsOnLamp,
		"mode":  ModeLamp,
	}
)

type LampCommandOneOf struct {
	IsOn      bool     `json:"isOn"`
	Mode      LampMode `json:"mode"`
	Threshold float64  `json:"threshold"`
}

type LampCommand struct {
	Timestamp time.Time   `json:"timestamp"`
	User      common.User `json:"user"`
	Status    bool        `json:"status"`
	UUID      string      `json:"uuid"`

	Type  LampCommandType  `json:"type"`
	OneOf LampCommandOneOf `json:"oneOf"`
}

func maybeLampMode(m map[string]interface{}, key string) LampMode {
	if m[key] == nil {
		return ManualLamp
	}
	return LampModeMap[m[key].(string)]
}
func NewLampState(m map[string]interface{}) LampCommand {
	userId := m["user_id"].(int64)
	maybeUser := userdata.GetUserById(userId)
	return LampCommand{
		Timestamp: m["_time"].(time.Time),
		User:      maybeUser,
		Status:    m["status"].(bool),
		UUID:      m["uuid"].(string),
		Type:      LampCommandTypeMap[m["type"].(string)],
		OneOf: LampCommandOneOf{
			IsOn:      maybeBool(m, "is_on"),
			Mode:      maybeLampMode(m, "mode"),
			Threshold: maybeFloat64(m, "threshold"),
		},
	}
}

func NewLampReading(m map[string]interface{}) LampReading {
	return LampReading{
		Illuminance: m["illuminance"].(float64),
		Timestamp:   m["_time"].(time.Time),
	}
}

func LampGetReadings(deviceId int64) []LampReading {
	client := influxdb2.NewClient(settings.Cfg.InfluxDB.Url, settings.Cfg.InfluxDB.Token)
	defer client.Close()
	queryClient := client.QueryAPI(settings.Cfg.InfluxDB.Org)

	query := `
	from(bucket: "%s")
		|> range(start: -%s)
		|> filter(fn: (r) => r._measurement == "%s")
		|> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
		|> filter(fn: (r) => r.device_id == "%d")
	`
	query = fmt.Sprintf(query, settings.Cfg.InfluxDB.BucketNvt, "1h", "lamp_m", deviceId)
	result, err := queryClient.Query(context.Background(), query)

	readings := make([]LampReading, 0)
	if err != nil {
		panic(err)
	} else {
		for result.Next() {
			values := result.Record().Values()
			val := NewLampReading(values)
			readings = append(readings, val)
		}
	}

	return readings
}

func LampGetLastState(deviceId int64) []LampCommand {
	client := influxdb2.NewClient(settings.Cfg.InfluxDB.Url, settings.Cfg.InfluxDB.Token)
	defer client.Close()
	queryClient := client.QueryAPI(settings.Cfg.InfluxDB.Org)

	query := `
	from(bucket: "%s")
		|> range(start: 0)
		|> filter(fn: (r) => r._measurement == "%s")
		|> drop(columns: ["_start", "_stop", "host", "topic"])
		|> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
		|> filter(fn: (r) => r.device_id == "%d")
		|> group(columns: ["type"])
		|> sort(columns: ["_time"])
		|> map(fn: (r) => ({ r with status: bool(v: r.status) }))
		|> filter(fn: (r) => r.status == true)
		|> last(column: "_time")
		|> group(columns: ["_measurement"])
		|> drop(columns: ["_measurement", "device_id"])
		|> map(fn: (r) => ({ r with status: bool(v: r.status) }))
	`
	query = fmt.Sprintf(query, settings.Cfg.InfluxDB.BucketNvt, "lamp_w", deviceId)
	result, err := queryClient.Query(context.Background(), query)

	stateArray := make([]LampCommand, 0)
	if err != nil {
		panic(err)
	} else {
		for result.Next() {
			values := result.Record().Values()
			val := NewLampState(values)
			stateArray = append(stateArray, val)
		}
	}

	return stateArray
}

func LampGetLastCommands(deviceId int64) []LampCommand {
	const (
		N = 10
	)

	client := influxdb2.NewClient(settings.Cfg.InfluxDB.Url, settings.Cfg.InfluxDB.Token)
	defer client.Close()
	queryClient := client.QueryAPI(settings.Cfg.InfluxDB.Org)

	query := `
	from(bucket: "%s")
		|> range(start: 0)
		|> filter(fn: (r) => r._measurement == "%s")
		|> drop(columns: ["_start", "_stop", "host", "topic"])
		|> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
		|> filter(fn: (r) => r.device_id == "%d")
		|> group(columns: ["_measurement"])
		|> drop(columns: ["_measurement", "device_id"])
		|> sort(columns: ["_time"], desc: true)
		|> limit(n: %d)
		|> map(fn: (r) => ({ r with status: bool(v: r.status) }))
	`

	query = fmt.Sprintf(query, settings.Cfg.InfluxDB.BucketNvt, "lamp_w", deviceId, N)
	result, err := queryClient.Query(context.Background(), query)

	stateArray := make([]LampCommand, 0)
	if err != nil {
		panic(err)
	} else {
		for result.Next() {
			values := result.Record().Values()
			val := NewLampState(values)
			stateArray = append(stateArray, val)
		}
	}

	return stateArray
}
