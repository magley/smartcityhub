package devices

import (
	"context"
	"fmt"
	"smartcityhub/socketserver/settings"
	"strconv"
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
)

type PingMeasurement struct {
	DeviceId   int64     `json:"device_id"`
	PropertyId int64     `json:"property_id"`
	Timestamp  time.Time `json:"timestamp"`
}

type PropertyDevicesOnlineStatusDTO struct {
	PropertyId    int64   `json:"property_id"`
	OnlineDevices []int64 `json:"online_devices"`
}

func NewPingMeasurement(m map[string]interface{}) PingMeasurement {
	deviceId, err := strconv.ParseInt(m["device_id"].(string), 10, 64)
	if err != nil {
		panic(err)
	}
	propertyId, err := strconv.ParseInt(m["property_id"].(string), 10, 64)
	if err != nil {
		panic(err)
	}

	return PingMeasurement{
		DeviceId:   deviceId,
		PropertyId: propertyId,
		Timestamp:  m["_time"].(time.Time),
	}
}

func GetOnlineDevicesForProperty(propertyId int64) PropertyDevicesOnlineStatusDTO {
	client := influxdb2.NewClient(settings.Cfg.InfluxDB.Url, settings.Cfg.InfluxDB.Token)
	defer client.Close()
	queryClient := client.QueryAPI(settings.Cfg.InfluxDB.Org)

	query := `
	from(bucket: "%s")
		|> range(start: -%s)
		|> filter(fn: (r) => r._measurement == "%s")
		|> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
		|> filter(fn: (r) => r.property_id == "%d")
		|> group(columns: ["device_id"])
		|> last(column: "_time")
	`
	query = fmt.Sprintf(query, settings.Cfg.InfluxDB.BucketNvt, "30s", "ping", propertyId)

	queryres, err := queryClient.Query(context.Background(), query)

	if err != nil {
		panic(err)
	}

	result := PropertyDevicesOnlineStatusDTO{}
	result.PropertyId = propertyId
	result.OnlineDevices = make([]int64, 0)

	for queryres.Next() {
		values := queryres.Record().Values()
		ping := NewPingMeasurement(values)
		result.OnlineDevices = append(result.OnlineDevices, ping.DeviceId)
	}

	return result

}
