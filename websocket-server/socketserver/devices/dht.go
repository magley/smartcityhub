package devices

import (
	"context"
	"fmt"
	"smartcityhub/socketserver/settings"
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
)

type DHTReading struct {
	Humidity    float64   `json:"humidity"`
	Temperature float64   `json:"temperature"`
	Timestamp   time.Time `json:"timestamp"`
}

func NewDHTReading(m map[string]interface{}) (DHTReading, bool) {
	h, ok := m["humidity"].(float64)
	if !ok {
		return DHTReading{}, false
	}
	t, ok := m["temperature"].(float64)
	if !ok {
		return DHTReading{}, false
	}

	return DHTReading{
		Humidity:    h,
		Temperature: t,
		Timestamp:   m["_time"].(time.Time),
	}, true
}

func DHTGetReadings(deviceId int64) []DHTReading {
	client := influxdb2.NewClient(settings.Cfg.InfluxDB.Url, settings.Cfg.InfluxDB.Token)
	defer client.Close()
	queryClient := client.QueryAPI(settings.Cfg.InfluxDB.Org)

	query := `
	from(bucket: "%s")
		|> range(start: -%s)
		|> filter(fn: (r) => r._measurement == "%s")
		|> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
		|> filter(fn: (r) => r.device_id == "%d")
	`
	query = fmt.Sprintf(query, settings.Cfg.InfluxDB.BucketNvt, "1h", "dht", deviceId)
	result, err := queryClient.Query(context.Background(), query)

	readings := make([]DHTReading, 0)
	if err != nil {
		panic(err)
	} else {
		for result.Next() {
			values := result.Record().Values()
			val, ok := NewDHTReading(values)
			if ok {
				readings = append(readings, val)
			}
		}
	}

	return readings
}
