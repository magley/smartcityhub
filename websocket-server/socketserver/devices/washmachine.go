package devices

import (
	"context"
	"fmt"
	"smartcityhub/socketserver/common"
	"smartcityhub/socketserver/settings"
	"smartcityhub/socketserver/userdata"
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
)

type WashMachineMode string
type WashMachineCommandType string

const (
	off          WashMachineMode = "off"
	cotton_95    WashMachineMode = "cotton_95"
	cotton_60    WashMachineMode = "cotton_60"
	cotton_40    WashMachineMode = "cotton_40"
	synthetic_60 WashMachineMode = "synthetic_60"
	synthetic_40 WashMachineMode = "synthetic_40"
	wool_40      WashMachineMode = "wool_40"

	WashMachineCmdMode           WashMachineCommandType = "mode"
	WashMachineCmdSchedule       WashMachineCommandType = "schedule"
	WashMachineCmdRemoveSchedule WashMachineCommandType = "removeSchedule"
)

var (
	WashMachineModeMap = map[string]WashMachineMode{
		"off":          off,
		"cotton_95":    cotton_95,
		"cotton_60":    cotton_60,
		"cotton_40":    cotton_40,
		"synthetic_60": synthetic_60,
		"synthetic_40": synthetic_40,
		"wool_40":      wool_40,
	}

	WashMachineCommandTypeMap = map[string]WashMachineCommandType{
		"mode":           WashMachineCmdMode,
		"schedule":       WashMachineCmdSchedule,
		"removeSchedule": WashMachineCmdRemoveSchedule,
	}
)

type WashMachineCommandOneOf_Scheduled struct {
	Mode WashMachineMode `json:"mode"`
	Time int64           `json:"timestamp"`
}

type WashMachineCommandOneOf struct {
	Mode      WashMachineMode                   `json:"mode"`
	Scheduled WashMachineCommandOneOf_Scheduled `json:"scheduled"`
}

type WashMachineCommand struct {
	Timestamp time.Time   `json:"timestamp"`
	User      common.User `json:"user"`
	Status    bool        `json:"status"`
	UUID      string      `json:"uuid"`

	Type  WashMachineCommandType  `json:"type"`
	OneOf WashMachineCommandOneOf `json:"oneOf"`
}

func maybeWashMachineMode(m map[string]interface{}, key string) WashMachineMode {
	if m[key] == nil {
		return off
	}
	return WashMachineModeMap[m[key].(string)]
}

func maybeWashMachineScheduled(m map[string]interface{}) WashMachineCommandOneOf_Scheduled {
	if m["start_time"] == nil {
		return WashMachineCommandOneOf_Scheduled{}
	}
	return WashMachineCommandOneOf_Scheduled{
		Mode: WashMachineModeMap[m["mode"].(string)],
		Time: m["start_time"].(int64),
	}
}

func NewWashMachineCommand(m map[string]interface{}) WashMachineCommand {
	userId := m["user_id"].(int64)
	maybeUser := userdata.GetUserById(userId)
	return WashMachineCommand{
		Timestamp: m["_time"].(time.Time),
		User:      maybeUser,
		Status:    m["status"].(bool),
		UUID:      m["uuid"].(string),
		Type:      WashMachineCommandTypeMap[m["type"].(string)],
		OneOf: WashMachineCommandOneOf{
			Mode:      maybeWashMachineMode(m, "mode"),
			Scheduled: maybeWashMachineScheduled(m),
		},
	}
}

// WashMachineGetLastState returns the last SUCCESSFUL command of each type for the device.
// Each command is a `WashMachineCommand` in an array. From therein you can examine the
// data and reconstruct the WashMachine state.
func WashMachineGetLastState(deviceId int64) []WashMachineCommand {
	client := influxdb2.NewClient(settings.Cfg.InfluxDB.Url, settings.Cfg.InfluxDB.Token)
	defer client.Close()
	queryClient := client.QueryAPI(settings.Cfg.InfluxDB.Org)

	query := `
	from(bucket: "%s")
		|> range(start: -1d)
		|> filter(fn: (r) => r._measurement == "%s")
		|> drop(columns: ["_start", "_stop", "host", "topic"])
		|> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
		|> filter(fn: (r) => r.device_id == "%d")
		|> group(columns: ["type"])
		|> sort(columns: ["_time"])
		|> map(fn: (r) => ({ r with status: bool(v: r.status) }))
		|> filter(fn: (r) => r.status == true)
		|> last(column: "_time")
		|> group(columns: ["_measurement"])
		|> drop(columns: ["_measurement", "device_id"])
		|> map(fn: (r) => ({ r with status: bool(v: r.status) }))
	`
	query = fmt.Sprintf(query, settings.Cfg.InfluxDB.BucketNvt, "wash_w", deviceId)
	result, err := queryClient.Query(context.Background(), query)

	stateArray := make([]WashMachineCommand, 0)
	if err != nil {
		panic(err)
	} else {
		for result.Next() {
			values := result.Record().Values()
			val := NewWashMachineCommand(values)
			stateArray = append(stateArray, val)
		}
	}

	return stateArray
}

// WashMachineGetLastCommands returns the last N commands processed by the device.
// They can be both successful and unsuccessful.
func WashMachineGetLastCommands(deviceId int64) []WashMachineCommand {
	const (
		N = 10
	)

	client := influxdb2.NewClient(settings.Cfg.InfluxDB.Url, settings.Cfg.InfluxDB.Token)
	defer client.Close()
	queryClient := client.QueryAPI(settings.Cfg.InfluxDB.Org)

	query := `
	from(bucket: "%s")
		|> range(start: -1d)
		|> filter(fn: (r) => r._measurement == "%s")
		|> drop(columns: ["_start", "_stop", "host", "topic"])
		|> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
		|> filter(fn: (r) => r.device_id == "%d")
		|> group(columns: ["_measurement"])
		|> drop(columns: ["_measurement", "device_id"])
		|> sort(columns: ["_time"], desc: true)
		|> limit(n: %d)
		|> map(fn: (r) => ({ r with status: bool(v: r.status) }))
	`

	query = fmt.Sprintf(query, settings.Cfg.InfluxDB.BucketNvt, "wash_w", deviceId, N)
	result, err := queryClient.Query(context.Background(), query)

	stateArray := make([]WashMachineCommand, 0)
	if err != nil {
		panic(err)
	} else {
		for result.Next() {
			values := result.Record().Values()
			val := NewWashMachineCommand(values)
			stateArray = append(stateArray, val)
		}
	}

	return stateArray
}
