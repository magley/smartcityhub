package devices

import (
	"context"
	"fmt"
	"smartcityhub/socketserver/common"
	"smartcityhub/socketserver/settings"
	"smartcityhub/socketserver/userdata"
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
)

type GateMode string
type GateOverride string
type GateOpenState string
type GateCommandType string
type GateDirection string
type GateSensorEvent string

const (
	PublicGate  GateMode = "public"
	PrivateGate GateMode = "private"

	OverrideYesGate GateOverride = "yes"
	OverrideNoGate  GateOverride = "no"

	OpenGate    GateOpenState = "open"
	OpeningGate GateOpenState = "opening"
	ClosingGate GateOpenState = "closing"
	ClosedGate  GateOpenState = "closed"

	EntryGate GateDirection = "entry"
	ExitGate  GateDirection = "exit"

	ArrivedGate GateSensorEvent = "arrived"
	LeftGate    GateSensorEvent = "left"
	PassedGate  GateSensorEvent = "passed"

	OpenStateGate   GateCommandType = "open_state"
	ModeGate        GateCommandType = "mode"
	OverrideGate    GateCommandType = "override"
	SensorExitGate  GateCommandType = "sensor_exit"
	SensorEntryGate GateCommandType = "sensor_entry"
)

var (
	GateModeMap = map[string]GateMode{
		"public":  PublicGate,
		"private": PrivateGate,
	}

	GateOverrideMap = map[string]GateOverride{
		"yes": OverrideYesGate,
		"no":  OverrideNoGate,
	}

	GateOpenStateMap = map[string]GateOpenState{
		"open":    OpenGate,
		"opening": OpeningGate,
		"closing": ClosingGate,
		"closed":  ClosedGate,
	}

	GateDirectionMap = map[string]GateDirection{
		"entry": EntryGate,
		"exit":  ExitGate,
	}

	GateSensorEventMap = map[string]GateSensorEvent{
		"arrived": ArrivedGate,
		"left":    LeftGate,
		"passed":  PassedGate,
	}

	GateCommandTypeMap = map[string]GateCommandType{
		"open_state":   OpenStateGate,
		"mode":         ModeGate,
		"override":     OverrideGate,
		"sensor_exit":  SensorExitGate,
		"sensor_entry": SensorEntryGate,
	}
)

type GateCommandOneOf struct {
	OpenState GateOpenState   `json:"openState"`
	Mode      GateMode        `json:"mode"`
	Override  GateOverride    `json:"override"`
	Plate     string          `json:"plate"`
	Direction GateDirection   `json:"direction"`
	Event     GateSensorEvent `json:"event"`
}

type GateCommand struct {
	Timestamp time.Time   `json:"timestamp"`
	User      common.User `json:"user"`
	Status    bool        `json:"status"`
	UUID      string      `json:"uuid"`

	Type  GateCommandType  `json:"type"`
	OneOf GateCommandOneOf `json:"oneOf"`
}

func maybeGateMode(m map[string]interface{}, key string) GateMode {
	if m[key] == nil {
		return PrivateGate
	}
	return GateModeMap[m[key].(string)]
}
func maybeGateOverride(m map[string]interface{}, key string) GateOverride {
	if m[key] == nil {
		return OverrideNoGate
	}
	return GateOverrideMap[m[key].(string)]
}
func maybeGateOpenState(m map[string]interface{}, key string) GateOpenState {
	if m[key] == nil {
		return ClosedGate
	}
	return GateOpenStateMap[m[key].(string)]
}
func maybeGateDirection(m map[string]interface{}, key string) GateDirection {
	if m[key] == nil {
		return EntryGate
	}
	return GateDirectionMap[m[key].(string)]
}
func maybeGateSensorEvent(m map[string]interface{}, key string) GateSensorEvent {
	if m[key] == nil {
		return ArrivedGate
	}
	return GateSensorEventMap[m[key].(string)]
}
func NewGateState(m map[string]interface{}) GateCommand {
	userId := m["user_id"].(int64)
	maybeUser := userdata.GetUserById(userId)
	return GateCommand{
		Timestamp: m["_time"].(time.Time),
		User:      maybeUser,
		Status:    m["status"].(bool),
		UUID:      m["uuid"].(string),
		Type:      GateCommandTypeMap[m["type"].(string)],
		OneOf: GateCommandOneOf{
			OpenState: maybeGateOpenState(m, "open_state"),
			Mode:      maybeGateMode(m, "mode"),
			Override:  maybeGateOverride(m, "override"),
			Plate:     maybeString(m, "plate"),
			Direction: maybeGateDirection(m, "direction"),
			Event:     maybeGateSensorEvent(m, "event"),
		},
	}
}

func GateGetLastState(deviceId int64) []GateCommand {
	client := influxdb2.NewClient(settings.Cfg.InfluxDB.Url, settings.Cfg.InfluxDB.Token)
	defer client.Close()
	queryClient := client.QueryAPI(settings.Cfg.InfluxDB.Org)

	query := `
	from(bucket: "%s")
		|> range(start: 0)
		|> filter(fn: (r) => r._measurement == "%s")
		|> drop(columns: ["_start", "_stop", "host", "topic"])
		|> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
		|> filter(fn: (r) => r.device_id == "%d")
		|> group(columns: ["type"])
		|> sort(columns: ["_time"])
		|> map(fn: (r) => ({ r with status: bool(v: r.status) }))
		|> filter(fn: (r) => r.status == true)
		|> last(column: "_time")
		|> group(columns: ["_measurement"])
		|> drop(columns: ["_measurement", "device_id"])
		|> map(fn: (r) => ({ r with status: bool(v: r.status) }))
	`
	query = fmt.Sprintf(query, settings.Cfg.InfluxDB.BucketNvt, "gate_w", deviceId)
	result, err := queryClient.Query(context.Background(), query)

	stateArray := make([]GateCommand, 0)
	if err != nil {
		panic(err)
	} else {
		for result.Next() {
			values := result.Record().Values()
			val := NewGateState(values)
			stateArray = append(stateArray, val)
		}
	}

	return stateArray
}

func GateGetLastCommands(deviceId int64) []GateCommand {
	const (
		N = 10
	)

	client := influxdb2.NewClient(settings.Cfg.InfluxDB.Url, settings.Cfg.InfluxDB.Token)
	defer client.Close()
	queryClient := client.QueryAPI(settings.Cfg.InfluxDB.Org)

	query := `
	from(bucket: "%s")
		|> range(start: 0)
		|> filter(fn: (r) => r._measurement == "%s")
		|> drop(columns: ["_start", "_stop", "host", "topic"])
		|> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
		|> filter(fn: (r) => r.device_id == "%d")
		|> group(columns: ["_measurement"])
		|> drop(columns: ["_measurement", "device_id"])
		|> sort(columns: ["_time"], desc: true)
		|> limit(n: %d)
		|> map(fn: (r) => ({ r with status: bool(v: r.status) }))
	`

	query = fmt.Sprintf(query, settings.Cfg.InfluxDB.BucketNvt, "gate_w", deviceId, N)
	result, err := queryClient.Query(context.Background(), query)

	stateArray := make([]GateCommand, 0)
	if err != nil {
		panic(err)
	} else {
		for result.Next() {
			values := result.Record().Values()
			val := NewGateState(values)
			stateArray = append(stateArray, val)
		}
	}

	return stateArray
}
