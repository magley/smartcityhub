package devices

import (
	"context"
	"fmt"
	"smartcityhub/socketserver/common"
	"smartcityhub/socketserver/settings"
	"smartcityhub/socketserver/userdata"
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
)

type SprinklersCommandType string

const (
	SprinklersCmdIsOn        SprinklersCommandType = "isOn"
	SprinklersCmdSetTriggers SprinklersCommandType = "setTriggers"
)

var (
	SprinklersCommandTypeMap = map[string]SprinklersCommandType{
		"isOn":        SprinklersCmdIsOn,
		"setTriggers": SprinklersCmdSetTriggers,
	}
)

type SprinklersCommandOneOf struct {
	IsOn     bool   `json:"is_on"`
	Triggers string `json:"triggers"`
}

type SprinklersCommand struct {
	Timestamp time.Time   `json:"timestamp"`
	User      common.User `json:"user"`
	Status    bool        `json:"status"`
	UUID      string      `json:"uuid"`

	Type  SprinklersCommandType  `json:"type"`
	OneOf SprinklersCommandOneOf `json:"oneOf"`
}

func NewSprinklersCommand(m map[string]interface{}) SprinklersCommand {
	userId := m["user_id"].(int64)
	maybeUser := userdata.GetUserById(userId)
	return SprinklersCommand{
		Timestamp: m["_time"].(time.Time),
		User:      maybeUser,
		Status:    m["status"].(bool),
		UUID:      m["uuid"].(string),
		Type:      SprinklersCommandTypeMap[m["type"].(string)],
		OneOf: SprinklersCommandOneOf{
			IsOn:     maybeBool(m, "is_on"),
			Triggers: maybeString(m, "triggers"),
		},
	}
}

// SprinklersGetLastState returns the last SUCCESSFUL command of each type for the device.
// Each command is a `SprinklersCommand` in an array. From therein you can examine the
// data and reconstruct the Sprinklers state.
func SprinklersGetLastState(deviceId int64) []SprinklersCommand {
	client := influxdb2.NewClient(settings.Cfg.InfluxDB.Url, settings.Cfg.InfluxDB.Token)
	defer client.Close()
	queryClient := client.QueryAPI(settings.Cfg.InfluxDB.Org)

	query := `
	from(bucket: "%s")
		|> range(start: 0)
		|> filter(fn: (r) => r._measurement == "%s")
		|> drop(columns: ["_start", "_stop", "host", "topic"])
		|> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
		|> filter(fn: (r) => r.device_id == "%d")
		|> group(columns: ["type"])
		|> sort(columns: ["_time"])
		|> map(fn: (r) => ({ r with status: bool(v: r.status) }))
		|> filter(fn: (r) => r.status == true)
		|> last(column: "_time")
		|> group(columns: ["_measurement"])
		|> drop(columns: ["_measurement", "device_id"])
		|> map(fn: (r) => ({ r with status: bool(v: r.status) }))
	`
	query = fmt.Sprintf(query, settings.Cfg.InfluxDB.BucketNvt, "sprinklers_w", deviceId)
	result, err := queryClient.Query(context.Background(), query)

	stateArray := make([]SprinklersCommand, 0)
	if err != nil {
		panic(err)
	} else {
		for result.Next() {
			values := result.Record().Values()
			val := NewSprinklersCommand(values)
			stateArray = append(stateArray, val)
		}
	}

	return stateArray
}

// SprinklersGetLastCommands returns the last N commands processed by the device.
// They can be both successful and unsuccessful.
func SprinklersGetLastCommands(deviceId int64) []SprinklersCommand {
	const (
		N = 10
	)

	client := influxdb2.NewClient(settings.Cfg.InfluxDB.Url, settings.Cfg.InfluxDB.Token)
	defer client.Close()
	queryClient := client.QueryAPI(settings.Cfg.InfluxDB.Org)

	query := `
	from(bucket: "%s")
		|> range(start: 0)
		|> filter(fn: (r) => r._measurement == "%s")
		|> drop(columns: ["_start", "_stop", "host", "topic"])
		|> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
		|> filter(fn: (r) => r.device_id == "%d")
		|> group(columns: ["_measurement"])
		|> drop(columns: ["_measurement", "device_id"])
		|> sort(columns: ["_time"], desc: true)
		|> limit(n: %d)
		|> map(fn: (r) => ({ r with status: bool(v: r.status) }))
	`

	query = fmt.Sprintf(query, settings.Cfg.InfluxDB.BucketNvt, "sprinklers_w", deviceId, N)
	result, err := queryClient.Query(context.Background(), query)

	stateArray := make([]SprinklersCommand, 0)
	if err != nil {
		panic(err)
	} else {
		for result.Next() {
			values := result.Record().Values()
			val := NewSprinklersCommand(values)
			stateArray = append(stateArray, val)
		}
	}

	return stateArray
}
