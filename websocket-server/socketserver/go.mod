module smartcityhub/socketserver

go 1.21.4

require github.com/gorilla/websocket v1.5.1

require (
	github.com/apapsch/go-jsonmerge/v2 v2.0.0 // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/deepmap/oapi-codegen v1.12.4 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/influxdata/line-protocol v0.0.0-20200327222509-2487e7298839 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	golang.org/x/sys v0.14.0 // indirect
)

require (
	github.com/fatih/color v1.16.0
	github.com/influxdata/influxdb-client-go/v2 v2.12.4
	github.com/redis/go-redis/v9 v9.3.0
	golang.org/x/net v0.17.0 // indirect
	gopkg.in/yaml.v2 v2.4.0
)
