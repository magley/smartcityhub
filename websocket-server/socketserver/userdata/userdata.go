package userdata

import (
	"log"
	"smartcityhub/socketserver/common"
	"smartcityhub/socketserver/db"
	"sync"
)

type UserData struct {
	Map map[int64]common.User
}

var GloUserDataMutex = &sync.RWMutex{}

// Note this is initialized with "Automatic" user in main.
var GloUserData = UserData{
	Map: make(map[int64]common.User),
}
var GloUserLastID = int64(0)

func GetUserById(id int64) common.User {
	// Try the cached UserData map.
	{
		GloUserDataMutex.Lock()
		user, isInMap := GloUserData.Map[id]
		GloUserDataMutex.Unlock()

		if isInMap {
			return user
		}
	}

	// Else, ask the database.
	{
		log.Printf("User with id=%d not in the in memory map, so I'm asking the database\n", id)
		user, err := db.GetClientById(id)
		if err != nil {
			panic(err)
		}
		if user == nil {
			panic("Huh?")
		}

		GloUserDataMutex.Lock()
		GloUserData.Map[id] = *user
		GloUserDataMutex.Unlock()

		return *user
	}
}
