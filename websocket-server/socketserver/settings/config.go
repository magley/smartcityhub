package settings

// https://dev.to/ilyakaznacheev/a-clean-way-to-pass-configs-in-a-go-application-1g64

import (
	"os"

	"gopkg.in/yaml.v2"
)

type Config struct {
	InfluxDB struct {
		Url       string `yaml:"url"`
		Org       string `yaml:"org"`
		Token     string `yaml:"token"`
		BucketNvt string `yaml:"bucket_nvt"`
	} `yaml:"influxdb"`
}

var Cfg Config

func initConfigPublic() {
	f, err := os.Open("config.yaml")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&Cfg)
	if err != nil {
		panic(err)
	}
}

func initConfigPrivate() {
	/*
		influxdb:
		  	token: "..."
	*/

	f, err := os.Open("config.secret.yaml")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&Cfg)
	if err != nil {
		panic(err)
	}
}

// InitConfig initializes global server configuration.
// Call this once at program start.
func InitConfig() {
	initConfigPublic()
	initConfigPrivate()
}
