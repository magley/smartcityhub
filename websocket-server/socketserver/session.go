package main

import (
	"encoding/json"
	"log"
	"net/http"
	"smartcityhub/socketserver/devices"
	"time"

	"github.com/fatih/color"
	"github.com/gorilla/websocket"
)

// A Session encapsulates a single websocket connection.
type Session struct {
	// ID of the user for this session. Upon connection, it is set to -1.
	// When the server receives the first WsMessage for this session from the
	// client, the UserId from WsMessage is fed into Session.
	// This is done each time the user sends a WsMessage which effectively means
	// that every subscription request will update UserId. Since a Session is
	// tied between one client (hence: one UserId), it is up to the client to
	// always send the same id.
	//
	// In turn, this means that the client app must make sure to always use the
	// same UserId when sending a subscription to the server.
	//
	// If this value is -1, then the user tied to this session is not yet known.
	//
	// TODO: Circumvent this. Can we send custom headers during the handshake?
	UserId int64
	Hub    *Hub
	Conn   *websocket.Conn
	Quit   chan bool
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func printErr(err error) {
	color.Set(color.FgRed)
	defer color.Unset()
	log.Println(err)
}

func logMinor(msg string) {
	color.Set(color.FgHiBlack)
	defer color.Unset()
	log.Println(msg)
}

// sendMessage packs the payload into a WsMessage and sends it to the specified
// session subscription.
// The payload is marshaled into JSON, so its type must be compatible.
//
// If a marshalling error occurs, it is logged and nothing is sent through the
// websocket.
func sendMessage(session *Session, subscription WsMessage, payload interface{}) {
	msg := NewWsResponse(subscription)
	msg.Payload = payload
	response, err := json.Marshal(msg)

	if err != nil {
		color.Set(color.FgRed)
		log.Printf("Could not send payload %s:\n", payload)
		log.Println(err)
		color.Unset()
	} else {
		session.Conn.WriteMessage(websocket.TextMessage, response)
	}
}

func onConnect(hub *Hub, writer http.ResponseWriter, req *http.Request) {
	upgrader.CheckOrigin = func(r *http.Request) bool {
		return true
	}

	connection, err := upgrader.Upgrade(writer, req, nil)

	if err != nil {
		printErr(err)
		return
	}
	log.Println("New connection")

	quitChannel := make(chan bool)
	session := Session{
		UserId: -1,
		Hub:    hub,
		Conn:   connection,
		Quit:   quitChannel,
	}

	go loopRead(&session)
	go loopWrite(&session)
}

// loopRead parses incoming messages from the session s.
func loopRead(s *Session) {
	for {
		_, msg_raw, err := s.Conn.ReadMessage()
		if err != nil {
			if nerr, ok := err.(*websocket.CloseError); ok {
				log.Printf("Connection close, unsubbing all. Code %d\n", nerr.Code)
				NewHub().UnsubscribeAll(int(s.UserId))
				s.Quit <- true
				return
			} else {
				printErr(err)
				continue
			}
		}
		logMinor(string(msg_raw))
		msg := WsMessage{}
		err = json.Unmarshal(msg_raw, &msg)

		if err != nil {
			continue // Couldn't unmarshal -> bad format.
		}

		s.UserId = int64(msg.UserId)

		log.Println(msg)

		if msg.Type[0] != '$' {
			err = s.Hub.Subscribe(msg)
			if err != nil {
				printErr(err)
			} else {
				log.Printf("Subscribed user %d on %s for %d,%d\n", msg.UserId, msg.Type, msg.Id1, msg.Id2)
			}
		} else if msg.Type == "$getsubs" {
			subs, err := s.Hub.GetSubscriptions(msg.UserId)
			if err != nil {
				printErr(err)
			} else {
				log.Printf("Subscriptions for user %d:\n", msg.UserId)
				for _, sub := range subs {
					log.Printf("Topic %s, Ids: %d,%d\n", sub.Type, sub.Id1, sub.Id2)
				}
			}

		}

		continue
	}
}

// loopWrite sends messages to the session s.
func loopWrite(s *Session) {
	for {
		select {
		case <-s.Quit:
			return
		default:
			time.Sleep(2 * time.Second)

			if s.UserId == -1 {
				// TODO: Sleep until s.UserId is not -1?
				continue
			}

			subs, err := s.Hub.GetSubscriptions(s.UserId)

			if err != nil {
				printErr(err)
				continue
			}

			for _, sub := range subs {
				if sub.Type == "ping" {
					onlineDevices := devices.GetOnlineDevicesForProperty(sub.Id1)
					sendMessage(s, sub, onlineDevices)
				} else if sub.Type == "dht" {
					readings := devices.DHTGetReadings(sub.Id1)
					sendMessage(s, sub, readings)
				} else if sub.Type == "ac_state" {
					reading := devices.AcGetLastState(sub.Id1)
					sendMessage(s, sub, reading)
				} else if sub.Type == "ac_livelog" {
					reading := devices.AcGetLastCommands(sub.Id1)
					sendMessage(s, sub, reading)
				} else if sub.Type == "lamp_m" {
					readings := devices.LampGetReadings(sub.Id1)
					sendMessage(s, sub, readings)
				} else if sub.Type == "lamp_state" {
					readings := devices.LampGetLastState(sub.Id1)
					sendMessage(s, sub, readings)
				} else if sub.Type == "lamp_livelog" {
					readings := devices.LampGetLastCommands(sub.Id1)
					sendMessage(s, sub, readings)
				} else if sub.Type == "gate_state" {
					readings := devices.GateGetLastState(sub.Id1)
					sendMessage(s, sub, readings)
				} else if sub.Type == "gate_livelog" {
					readings := devices.GateGetLastCommands(sub.Id1)
					sendMessage(s, sub, readings)
				} else if sub.Type == "wash_state" {
					reading := devices.WashMachineGetLastState(sub.Id1)
					sendMessage(s, sub, reading)
				} else if sub.Type == "wash_livelog" {
					reading := devices.WashMachineGetLastCommands(sub.Id1)
					sendMessage(s, sub, reading)
				} else if sub.Type == "sprinklers_state" {
					reading := devices.SprinklersGetLastState(sub.Id1)
					sendMessage(s, sub, reading)
				} else if sub.Type == "sprinklers_livelog" {
					reading := devices.SprinklersGetLastCommands(sub.Id1)
					sendMessage(s, sub, reading)
				}
			}
		}
	}
}
