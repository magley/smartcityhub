package main

import (
	"fmt"
	"strconv"
	"strings"
)

type WsMessage struct {
	UserId int64  `json:"user"`
	Type   string `json:"type"`
	Id1    int64  `json:"id1"`
	Id2    int64  `json:"id2"`
}

type WsResponse struct {
	UserId  int64       `json:"user"`
	Type    string      `json:"type"`
	Id1     int64       `json:"id1"`
	Id2     int64       `json:"id2"`
	Payload interface{} `json:"payload"`
}

func NewWsResponse(sub WsMessage) WsResponse {
	return WsResponse{
		UserId: sub.UserId,
		Type:   sub.Type,
		Id1:    sub.Id1,
		Id2:    sub.Id2,
	}
}

func (sub *WsMessage) toSubscriptionRedisValue() string {
	return fmt.Sprintf("%s:%d:%d", sub.Type, sub.Id1, sub.Id2)
}

func NewMsMessageFromSub(redisValue string, userId int64) WsMessage {
	sub := WsMessage{}
	parts := strings.Split(redisValue, ":")

	sub.Type = parts[0]
	sub.Id1, _ = strconv.ParseInt(parts[1], 10, 64)
	sub.Id2, _ = strconv.ParseInt(parts[2], 10, 64)
	sub.UserId = userId

	return sub
}
