# Smartcityhub websocket server

A Go program that handles websocket communication for SmartCityHub.

# Instructions

## Redis

1) Download Redis.
    - Redis isn't supported on Windows anymore (there exist older official versions
by Microsoft), but there is a [native port for Windows](https://github.com/zkteco-home/redis-windows) that's pretty up-to-date. 
    * Otherwise, you may use Docker or WSL or just Linux directly.
    * The rest of this guide will use the native port.
2) Unzip the archive anywhere.
3) From the command line, run `redis.server.exe`. It should show a CLI splash screen.
4) There's also `redis-cli.exe` where you can test if it works (`SET key val`, `GET key` etc.)

## Go Server

1) Compile server `go build`
2) Create a `config.secret.yaml` in the same dir where `config.yaml` is. See `config.go` for contents. 
3) Run server `socketserver.exe`

# Websocket API

Smartcityhub uses a proprietary JSON based websockets protocol.

## Subscribe to a topic

Send the following JSON:
```json
{
    "user": 1,
    "type": "dht",
    "id1": 1,
    "id2": -1
}
```

`user` is the ID of the issuer in the SQL database. This is used to keep track of sessions.

`type` determines the topic to subscribe to.

`id1` and `id2` are general purpose IDs. Currently `id2` is reserved for later use and it is reccommended to send either `-1` or `0`.

The following is a list of topics:

 - `"ping"` for online/offline status for all the devices of property identified by `id1` 
 - `"dht"` for the last 1 hour of sensor data of a DHT sensor identified by `id1`
 - `"ac_state"` for the current state of an air conditioner identified by `id1`
 - `"ac_livelog"` for the last 10 commands issued in the last 24h for an air conditioner identified by `id1`
 - `"wash_state"` for the current state of a wash machine identified by `id1`
 - `"wash_livelog"` for the last 10 commands issued in the last 24h for a wash machine identified by `id1`
 - `"lamp_state"` for the current state of a lamp identified by `id1`
 - `"lamp_livelog"` for the last 10 commands issued in the last 24h for a lamp identified by `id1`
 - `"gate_state"` for the current state of a gate identified by `id1`
 - `"gate_livelog"` for the last 10 commands issued in the last 24h for a gate identified by `id1`
 - `"sprinklers_state"` for the current state of a sprinkler identified by `id1`
 - `"sprinklers_livelog"` for the last 10 commands issued in the last 24h for a sprinkler identified by `id1`