package db

// This package should not be called db and this module should not be called mariadb.
// But since I couldn't get the socketserver to connect to mariadb directly (issues with the mysql driver), I had to use HTTP.

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"smartcityhub/socketserver/common"
)

func GetClientById(id int64) (*common.User, error) {
	resp, err := http.Get(fmt.Sprintf("http://127.0.0.1:8300/api/user/%d", id))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var user common.User
	err = json.Unmarshal(body, &user)
	if err != nil {
		return nil, err
	}

	return &user, nil
}
