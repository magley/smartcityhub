package main

import (
	"context"
	"fmt"
	"time"

	"github.com/redis/go-redis/v9"
)

var ctx = context.Background()

type Hub struct {
	redis *redis.Client
}

func NewHub() *Hub {
	h := new(Hub)
	h.redis = redis.NewClient(&redis.Options{
		Addr:        "127.0.0.1:6379",
		Password:    "",
		DB:          0,
		ReadTimeout: 1 * time.Minute,
	})

	return h
}

// Subscribe memos a topic subscription s.
// s.Type must be a valid topic.
func (h *Hub) Subscribe(s WsMessage) error {
	_, err := h.redis.SAdd(
		ctx,
		fmt.Sprintf("user:%d", s.UserId),
		s.toSubscriptionRedisValue(),
	).Result()

	return err
}

// GetSubscriptions returns all subscriptions for the user with id userId.
func (h *Hub) GetSubscriptions(userId int64) ([]WsMessage, error) {
	rr, err := h.redis.SMembers(ctx, fmt.Sprintf("user:%d", userId)).Result()

	if err != nil {
		return nil, err
	}

	subs := make([]WsMessage, 0)
	for _, str := range rr {
		subs = append(subs, NewMsMessageFromSub(str, userId))
	}

	return subs, err
}

// UnsubscribeAll deletes all subscriptions for the user with id userId.
func (h *Hub) UnsubscribeAll(userId int) error {
	_, err := h.redis.Del(ctx, fmt.Sprintf("user:%d", userId)).Result()
	return err
}

// Unsubscribe removes a single subscription for the user with id userId.
func (h *Hub) Unsubscribe(userId int, sub WsMessage) error {
	_, err := h.redis.SRem(ctx, fmt.Sprintf("user:%d", userId), sub).Result()
	return err
}
