package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"smartcityhub/socketserver/common"
	"smartcityhub/socketserver/settings"
	"smartcityhub/socketserver/userdata"

	"github.com/fatih/color"

	_ "net/http/pprof"
)

const PORT = 8081

func printGuide() {
	color.Set(color.FgHiBlack)
	fmt.Printf("Establish a websocket connection:\n\tws://127.0.0.1:%d/ws\n", PORT)
	fmt.Printf("Subscribe to topic:\n\tSend JSON {\"user\": 1, \"type\": \"dht\", \"id1\": 1, \"id2\": 0}\n")
	fmt.Printf("\tThe semantics for id1 and id2 depend on the context, and you many not need both.\n")
	fmt.Printf("Get all subscriptions:\n\tSend JSON {\"user\": 1, \"type\": \"$getsubs\"}\n")
	color.Unset()
}

func main() {
	settings.InitConfig()

	// Init automatic user
	userdata.GloUserData.Map[-1] = common.User{
		Id:       -1,
		Name:     "Automatic",
		LastName: "",
	}

	color.Set(color.FgHiBlack)
	log.Println("Starting websocket server...")

	hub := NewHub()
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		onConnect(hub, w, r)
	})

	server := http.Server{
		Addr:              fmt.Sprintf(":%d", PORT),
		ReadHeaderTimeout: 3 * time.Second,
	}

	color.Set(color.FgHiGreen)
	log.Println("Websocket server ready!")
	color.Unset()
	printGuide()

	err := server.ListenAndServe()
	if err != nil {
		log.Println(err)
	}
}
