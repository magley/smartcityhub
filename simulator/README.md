Smartcityhub simulators
---

# How to run

1) `pip install -r requirements.txt`
2) `python main.py -p [PROPERTY_ID] -d [DEVICE_ID] -t [TYPE]`

Type can be:
- `dht`
- `ac`
- `wash`
- `lamp`
- `gate`
- `sprinklers`

# Generating bulk data

By issuing the command parameter `-bulk`, the simulator enters bulk mode - it will generate data for the last 3 months and print its progress in stdout. When it's finished, you may close the simulator a keyboard interrupt.