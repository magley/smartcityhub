from dataclasses import asdict, dataclass
import math
import time
import paho.mqtt.client as mqtt
import json
from properties import properties
import threading
from common.message import *
import random
from typing import NamedTuple
from uuid import uuid4
import sim.config as cfg
from datetime import datetime, timedelta

BULK_DATA = False # Set to True to quickly generate data from the last 3 months instead of real-time.


'''
WashMachineMode: (
    off,
    cotton_95,
    cotton_60,
    cotton_40,
    synthetic_60,
    synthetic_40,
    wool_40,
)

WashMachineCmdSchedule: {
    userId: int
    type: WashMachineCommandType
    value: any
    uuid: str
    time: datetime
}

WashMachineState: {
    mode: WashMachineMode
    started_at: datetime
    scheduled: WashMachineCmdSchedule[]
}

WashMachineConfig: {
    supportedModes: WashMachineMode[]
}

WashMachineUserdata: {
    device_id: int
    property_id: int
    config: WashMachineConfig
    client: mqtt.Client # Initially None, upon connection to InfluxDB it will be set to the client object
    state: WashMachineState
}

WashMachineCommandType: (
    mode,
    schedule,
    removeSchedule
)

WashMachineCmdMode: {
    userId: int
    type: WashMachineCommandType
    value: any
    uuid: str
}

'''

if __name__ == "__main__":
    print("This module cannot be run manually! Use Root main.py.")
    exit(1)

'''
MQTT
'''

def _on_connect(client: mqtt.Client, userdata: dict, flags, result_code):
    client.subscribe(f"wash/cmd/{userdata['device_id']}")
    print("Subbed to", f"wash/cmd/{userdata['device_id']}")


def _on_receive(client: mqtt.Client, userdata: dict, msg: mqtt.MQTTMessage):
    msg_dict = json.loads(msg.payload)
    if msg_dict['type'] == 'mode':
        set_mode(userdata, msg_dict)
    elif msg_dict['type'] ==  'schedule':
        schedule(userdata, msg_dict)
    elif msg_dict['type'] == 'removeSchedule':
        remove_schedule(userdata, msg_dict)
    else:
        print(f'Unknown command {msg_dict["type"]} in {msg_dict}, ignoring.')


def _on_send(client: mqtt.Client, userdata: dict, mid: any):
    pass


def _on_disconnect(client: mqtt.Client, userdata: dict, on_disconnect):
    pass


def _make_and_connect_client(userdata: dict) -> mqtt.Client:
    client = mqtt.Client()
    userdata['client'] = client
    client.on_connect = _on_connect
    client.on_disconnect = _on_disconnect
    client.on_message = _on_receive
    client.on_pub_cmd = _on_send
    client.user_data_set(userdata)
    client.username_pw_set(properties['username'], properties['password'])
    client.connect(properties['host'], port=properties['port'])
    return client


'''
Ping logic
'''

def _ping_loop(client: mqtt.Client, device_id: int, property_id: int):
    sleep_duration = properties['ping_freq_seconds']
    topic = f'ping/{property_id}'
    i = 0
    if BULK_DATA:
        now = datetime.now()
        ns_IN_s = 1000000000 # Nanoseconds in a microsecond
        three_months_ago = (now - timedelta(days=30*3)).timestamp()
        virtual_now = now - timedelta(days=30*3)
        timestamp = int(virtual_now.timestamp() * ns_IN_s)

        while True:
            msg = make_msg_at(device_id, MessageType.PING, {'ignore': 0}, {}, timestamp)
            client.publish(topic=topic, payload=msg)

            # Next tick
            virtual_now = virtual_now + timedelta(seconds=sleep_duration)
            timestamp = int(virtual_now.timestamp() * ns_IN_s)

            i += 1
            if i % 6 == 0:
                print('ping\t', virtual_now.timestamp(), datetime.now().timestamp(), f'{int((virtual_now.timestamp() - three_months_ago) / (datetime.now().timestamp() - three_months_ago) * 100)}%')

                if virtual_now.timestamp() >= datetime.now().timestamp():
                    return
    else:
        while True:
            msg = make_msg(device_id, MessageType.PING, {'ignore': 0}, {})
            #print(topic, msg)
            client.publish(topic=topic, payload=msg)
            time.sleep(sleep_duration)

'''
Simulator
'''

def calc_end(mode: str, start_time: datetime):
    return start_time + timedelta(seconds=12)


def set_mode(userdata: dict, cmd: dict, timestamp_ns = None):
    mode = cmd['value']
    cmd_uuid = cmd['uuid']
    cmd_user_id = cmd['userId']
    success = True

    if mode not in userdata['config']['supportedModes']:
        success = False

    if success:
        userdata['state']['mode'] = mode
        userdata['state']['started_at'] = datetime.now()

    fields = {
        "user_id": cmd_user_id,
        "uuid": cmd_uuid,
    }
    tags = {
        "type": "mode",
        "mode": mode,
        "status": success
    }

    if timestamp_ns is None:
        msg = make_msg(userdata['device_id'], MessageType.WASH_MACHINE_COMMAND, fields=fields, tags=tags)
        print(cmd['userId'], cmd['value'])
        userdata['client'].publish(f'wash_w/{userdata["device_id"]}', msg)
    else:
        msg = make_msg_at(userdata['device_id'], MessageType.WASH_MACHINE_COMMAND, fields=fields, tags=tags, when_nanoseconds=timestamp_ns)
        userdata['client'].publish(f'wash_w/{userdata["device_id"]}', msg)



def schedule(userdata: dict, cmd: dict, timestamp_ns = None):
    time = datetime.fromtimestamp(cmd['value']['time'])
    mode = cmd['value']['mode']
    cmd_uuid = cmd['uuid']
    cmd_user_id = cmd['userId']
    success = True

    if mode not in userdata['config']['supportedModes']:
        success = False
    if time < datetime.now():
        success = False

    if success:
        userdata['state']['mode'] = mode
        userdata['state']['started_at'] = datetime.now()
        userdata['state']['scheduled'].append(cmd)

    fields = {
        "user_id": cmd_user_id,
        "uuid": cmd_uuid,
        "start_time": int(time.timestamp()),
        "end_time": int(calc_end(mode, time).timestamp()),
    }
    tags = {
        "type": "schedule",
        "mode": mode,
        "status": success
    }

    if timestamp_ns is None:
        msg = make_msg(userdata['device_id'], MessageType.WASH_MACHINE_COMMAND, fields=fields, tags=tags)
        print(cmd['userId'], cmd['value'])
        userdata['client'].publish(f'wash_w/{userdata["device_id"]}', msg)
    else:
        msg = make_msg_at(userdata['device_id'], MessageType.WASH_MACHINE_COMMAND, fields=fields, tags=tags, when_nanoseconds=timestamp_ns)
        userdata['client'].publish(f'wash_w/{userdata["device_id"]}', msg)


def remove_schedule(userdata: dict, cmd: dict, timestamp_ns = None):
    time = datetime.fromtimestamp(cmd['value']['time'])
    mode = cmd['value']['mode']
    cmd_uuid = cmd['uuid']
    cmd_user_id = cmd['userId']
    success = True

    if mode not in userdata['config']['supportedModes']:
        success = False
    if time < datetime.now():
        success = False

    if success:
        userdata['state']['scheduled'] = [e for e in userdata['state']['scheduled'] if e['uuid'] != cmd_uuid]

    fields = {
        "user_id": cmd_user_id,
        "uuid": cmd_uuid,
        "start_time": int(time.timestamp()),
        "end_time": int(calc_end(mode, time).timestamp()),
    }
    tags = {
        "type": "removeSchedule",
        "mode": mode,
        "status": success
    }

    if timestamp_ns is None:
        msg = make_msg(userdata['device_id'], MessageType.WASH_MACHINE_COMMAND, fields=fields, tags=tags)
        print(cmd['userId'], cmd['value'])
        userdata['client'].publish(f'wash_w/{userdata["device_id"]}', msg)
    else:
        msg = make_msg_at(userdata['device_id'], MessageType.WASH_MACHINE_COMMAND, fields=fields, tags=tags, when_nanoseconds=timestamp_ns)
        userdata['client'].publish(f'wash_w/{userdata["device_id"]}', msg)



def _simulator_loop_BULK_DATA(client: mqtt.Client, userdata: dict):
    now = datetime.now()
    ns_IN_s = 1000000000 # Nanoseconds in a microsecond
    three_months_ago = (now - timedelta(days=30*3)).timestamp()
    virtual_now = now - timedelta(days=30*3)
    timestamp = int(virtual_now.timestamp() * ns_IN_s)
    i = 0
    command_uuids = []

    while True:
        command = random.choice(['set_mode', 'schedule', 'remove_schedule'])
        user_id = random.choice([-1, -1, -1, 1, 2, 3, 4, 5]) # NOTE: Access control is not considered.
        modes = [ 'off', 'cotton_95', 'cotton_60', 'cotton_40', 'synthetic_60', 'synthetic_40', 'wool_40' ]

        if command == 'set_mode':
            cmd = {
                "device_id": userdata['device_id'],
                "userId": user_id,
                "type": "mode",
                "value": random.choice(modes),
                "uuid": str(uuid4()),
            }
            set_mode(userdata, cmd, timestamp)

        elif command == 'schedule':
            uuid = str(uuid4())
            cmd = {
                "device_id": userdata['device_id'],
                "uuid": uuid,
                "userId": user_id,
                "value": {
                    "time": int((datetime.now() + timedelta(days = random.randint(0, 10), hours = random.randint(1, 12))).timestamp()),
                    "mode": random.choice(modes),
                },
            }
            command_uuids.append(uuid)
            schedule(userdata, cmd, timestamp)
        elif command == 'remove_schedule':
            if len(command_uuids) == 0:
                continue
            uuid = random.choice(command_uuids)
            cmd = {
                "device_id": userdata['device_id'],
                "uuid": uuid,
                "userId": user_id,
                "value": {
                    "time": int((datetime.now() + timedelta(days = random.randint(0, 10), hours = random.randint(1, 12))).timestamp()),
                    "mode": random.choice(modes),
                },
            }
            command_uuids.remove(uuid)
            remove_schedule(userdata, cmd, timestamp)

        # Next tick
        # Assuming: wash machine is used once a day, with 6 commands per day.
        # That's 6 * 30 = 180 commands a month, or 540 commands in the last 3 months.
        # 6cmd = 24h
        # 1cmd = 4h
        virtual_now = virtual_now + timedelta(seconds = 4 * 3600)
        timestamp = int(virtual_now.timestamp() * ns_IN_s)
        i += 1
        if i % 6 == 0:
            print('wash\t', virtual_now.timestamp(), datetime.now().timestamp(), f'{int((virtual_now.timestamp() - three_months_ago) / (datetime.now().timestamp() - three_months_ago) * 100)}%')

            if virtual_now.timestamp() >= datetime.now().timestamp():
                return


def _simulator_loop(client: mqtt.Client, userdata: dict):
    if BULK_DATA:
        _simulator_loop_BULK_DATA(client, userdata)
    else:
        sleep_duration = 2

        scheduled_uuid = None
        scheduled_latest = None
        while True:
            current_mode = userdata['state']['mode']

            # Check for scheduled modes.
            for sch in userdata['state']['scheduled']:
                start_time: datetime = datetime.fromtimestamp(sch['value']['time'])
                uuid: str = sch['value']['uuid']
                mode: str = sch['value']['mode']
                end_time = calc_end(current_mode, start_time)

                if datetime.now() >= start_time and datetime.now() <= end_time:
                    # Get the most recent scheduled that wasn't acknowledged before.
                    if (scheduled_latest is None or start_time >= scheduled_latest) and scheduled_uuid != uuid:
                        scheduled_latest = start_time
                        scheduled_uuid = uuid

                        cmd_activate_automatically = {
                            "userId": -1,
                            "type": "mode",
                            "value": mode,
                            "uuid": str(uuid4())
                        }
                        set_mode(userdata, cmd_activate_automatically)


            # Check if should be turned off.
            if current_mode != 'off':
                started_at: datetime = userdata['state']['started_at']

                # Calculate when the washing machine cycle should finish.
                finished_at = calc_end(current_mode, started_at)

                # End the washing cycle if needed.
                if (datetime.now() >= finished_at):
                    cmd_turn_off_automatically = {
                        "userId": -1,
                        "type": "mode",
                        "value": "off",
                        "uuid": str(uuid4())
                    }
                    set_mode(userdata, cmd_turn_off_automatically)
            time.sleep(sleep_duration)

'''
Main
'''

def generate_config():
    cfg = {
        'supportedModes': [
            "off",
            "cotton_95",
            "cotton_60",
            "cotton_40",
            "synthetic_60",
            "synthetic_40",
            "wool_40",
        ]
    }
    print(f"[WashMachine::generate_config()]: {cfg}")
    return cfg


def run(device_id: int, property_id: int, is_bulk: bool):
    global BULK_DATA
    BULK_DATA = is_bulk

    config, success = cfg.load_config(device_id)
    if not success:
        config = generate_config()
        cfg.save_config_dict(device_id, config)

    state = {
        'mode': 'off',
        'started_at': datetime.now(),
        'scheduled': []
    }

    userdata = {
        'device_id': device_id,
        'property_id': property_id,
        'config': config,
        'client': None,
        'state': state,
    }

    client = _make_and_connect_client(userdata)

    ping_thread = threading.Thread(target=lambda: _ping_loop(client, device_id, property_id), daemon=True)
    ping_thread.start()

    simulation_thread = threading.Thread(target = lambda: _simulator_loop(client, userdata), daemon=True)
    simulation_thread.start()

    try:
        client.loop_forever()
    except KeyboardInterrupt:
        client.disconnect()