from dataclasses import asdict, dataclass
from datetime import datetime, timedelta
import math
import time
import paho.mqtt.client as mqtt
import json
from properties import properties
import threading
from common.message import *
import random
from typing import NamedTuple
from uuid import uuid4

import sim.config as cfg


BULK_DATA = False # Set to True to quickly generate data from the last 3 months instead of real-time.


if __name__ == "__main__":
    print("This module cannot be run manually! Use Root main.py.")
    exit(1)


'''
Types
'''


class AcCommandType(str, enum.Enum):
    temperature = 'temperature',
    is_on = 'is_on',
    mode = 'mode',
    config = 'config'


class AcMode(str, enum.Enum):
    heat = 'heat',
    cool = 'cool',
    auto = 'auto',
    fan = 'fan',


@dataclass
class AcCustomModeItem():
    startHour: int
    endHour: int
    temperature: float
    mode: AcMode


@dataclass
class AcState():
    temperature: float
    is_on: bool
    mode: AcMode


@dataclass
class AcConfig():
    minTemperature: float
    maxTemperature: float
    supportedModes: list[AcMode]
    config: list[AcCustomModeItem]


@dataclass
class AcCommand():
    userId: int
    type: AcCommandType
    value: any
    uuid: str


@dataclass
class AcCommandIsOn():
    userId: int
    type: AcCommandType
    value: bool
    uuid: str


@dataclass
class AcCommandTemperature():
    userId: int
    type: AcCommandType
    value: float
    uuid: str


@dataclass
class AcCommandMode():
    userId: int
    type: AcCommandType
    value: AcMode
    uuid: str


@dataclass
class AcCommandConfig():
    userId: int
    type: AcCommandType
    value: list[AcCustomModeItem]
    uuid: str


@dataclass
class AcUserData():
    device_id: int
    property_id: int
    config: AcConfig
    client: mqtt.Client # Initially None, upon connection to InfluxDB it will be set to the client object
    state: AcState

'''
Config
'''


def generate_config() -> AcConfig:
    cfg = AcConfig(12.0, 22.0, [AcMode.cool, AcMode.auto, AcMode.heat], [])
    print(f"[AC::generate_config()]: {cfg}")
    return cfg


'''
MQTT Callbacks and helper functions. 
'''

def _on_connect(client: mqtt.Client, userdata: AcUserData, flags, result_code):
    client.subscribe(f"ac/cmd/{userdata.device_id}")
    print("Subbed to", f"ac/cmd/{userdata.device_id}")


def _on_receive(client: mqtt.Client, userdata: AcUserData, msg: mqtt.MQTTMessage):
    cmd: AcCommand = AcCommand(**json.loads(msg.payload))
    cmd.type = AcCommandType(cmd.type) # Force it to be AcCommandType, not string.

    if cmd.type == AcCommandType.is_on:
        set_is_turned_on(userdata, AcCommandIsOn(**cmd.__dict__))
    elif cmd.type == AcCommandType.temperature:
        set_temperature(userdata, AcCommandTemperature(**cmd.__dict__))
    elif cmd.type == AcCommandType.mode:
        set_mode(userdata, AcCommandMode(**cmd.__dict__))
    elif cmd.type == AcCommandType.config:
        d = AcCommandConfig(**cmd.__dict__)
        li = []
        for item in d.value:
            li.append(AcCustomModeItem(**item))
        d.value = li
        set_config(userdata, d)
    else:
        print(f'Unknown command {cmd.type}, ignoring.')


def _on_send(client: mqtt.Client, userdata: AcUserData, mid: any):
    pass


def _on_disconnect(client: mqtt.Client, userdata: AcUserData, on_disconnect):
    pass


def _make_and_connect_client(userdata: AcUserData) -> mqtt.Client:
    client = mqtt.Client()
    userdata.client = client
    client.on_connect = _on_connect
    client.on_disconnect = _on_disconnect
    client.on_message = _on_receive
    client.on_pub_cmd = _on_send
    client.user_data_set(userdata)
    client.username_pw_set(properties['username'], properties['password'])
    client.connect(properties['host'], port=properties['port'])
    return client

'''
Ping logic
'''

def _ping_loop(client: mqtt.Client, device_id: int, property_id: int):
    sleep_duration = properties['ping_freq_seconds']
    topic = f'ping/{property_id}'
    i = 0
    if BULK_DATA:
        now = datetime.now()
        ns_IN_s = 1000000000 # Nanoseconds in a microsecond
        three_months_ago = (now - timedelta(days=30*3)).timestamp()
        virtual_now = now - timedelta(days=30*3)
        timestamp = int(virtual_now.timestamp() * ns_IN_s)

        while True:
            msg = make_msg_at(device_id, MessageType.PING, {'ignore': 0}, {}, timestamp)
            client.publish(topic=topic, payload=msg)

            # Next tick
            virtual_now = virtual_now + timedelta(seconds=sleep_duration)
            timestamp = int(virtual_now.timestamp() * ns_IN_s)

            i += 1
            if i % 6 == 0:
                print('ping\t', virtual_now.timestamp(), datetime.now().timestamp(), f'{int((virtual_now.timestamp() - three_months_ago) / (datetime.now().timestamp() - three_months_ago) * 100)}%')

                if virtual_now.timestamp() >= datetime.now().timestamp():
                    return
    else:
        while True:
            msg = make_msg(device_id, MessageType.PING, {'ignore': 0}, {})
            #print(topic, msg)
            client.publish(topic=topic, payload=msg)
            time.sleep(sleep_duration)

'''
Simulator
'''


def set_is_turned_on(userdata: AcUserData, cmd: AcCommandIsOn, timestamp_ns = None):
    assert cmd.type == AcCommandType.is_on and type(cmd.value) is bool

    success = True

    if success:
        userdata.state.is_on = cmd.value

    fields = {
        "user_id": cmd.userId,
        "uuid": cmd.uuid,
    }
    tags = {
        "type": "is_on",
        "is_on": cmd.value,
        "status": success
    }
    if timestamp_ns is None:
        msg = make_msg(userdata.device_id, MessageType.AC_COMMAND, fields=fields, tags=tags)
        print(cmd.userId, cmd.value)
        userdata.client.publish(f'ac_w/{userdata.device_id}', msg)
    else:
        msg = make_msg_at(userdata.device_id, MessageType.AC_COMMAND, fields=fields, tags=tags, when_nanoseconds=timestamp_ns)
        userdata.client.publish(f'ac_w/{userdata.device_id}', msg)


def set_temperature(userdata: AcUserData, cmd: AcCommandTemperature, timestamp_ns = None):
    assert cmd.type == AcCommandType.temperature and type(cmd.value) is float

    success = True

    if cmd.value < userdata.config.minTemperature:
        success = False
    elif cmd.value > userdata.config.maxTemperature:
        success = False

    if success:
        userdata.state.temperature = cmd.value

    fields = {
        "user_id": cmd.userId,
        "temperature": cmd.value,
        "uuid": cmd.uuid,
    }
    tags = {
        "type": "temperature",
        "status": success  
    }
    if timestamp_ns is None:
        msg = make_msg(userdata.device_id, MessageType.AC_COMMAND, fields=fields, tags=tags)
        print(cmd.userId, cmd.value)
        userdata.client.publish(f'ac_w/{userdata.device_id}', msg)
    else:
        msg = make_msg_at(userdata.device_id, MessageType.AC_COMMAND, fields=fields, tags=tags, when_nanoseconds=timestamp_ns)
        userdata.client.publish(f'ac_w/{userdata.device_id}', msg)
    #print(cmd.userId, cmd.value)
    userdata.client.publish(f'ac_w/{userdata.device_id}', msg)


def set_mode(userdata: AcUserData, cmd: AcCommandMode, timestamp_ns = None):
    if type(cmd.value) == str:
        cmd.value = AcMode(cmd.value)
    assert cmd.type == AcCommandType.mode and type(cmd.value) is AcMode

    success = True

    if cmd.value not in userdata.config.supportedModes:
        success = False

    if success:
        userdata.state.mode = AcMode(cmd.value)
    
    fields = {
        "user_id": cmd.userId,
        "uuid": cmd.uuid,
    }
    tags = {
        "type": "mode",
        "mode": cmd.value.value,
        "status": success
    }
    if timestamp_ns is None:
        msg = make_msg(userdata.device_id, MessageType.AC_COMMAND, fields=fields, tags=tags)
        print(cmd.userId, cmd.value)
        userdata.client.publish(f'ac_w/{userdata.device_id}', msg)
    else:
        msg = make_msg_at(userdata.device_id, MessageType.AC_COMMAND, fields=fields, tags=tags, when_nanoseconds=timestamp_ns)
        userdata.client.publish(f'ac_w/{userdata.device_id}', msg)
    #print(cmd.userId, cmd.value.value)
    userdata.client.publish(f'ac_w/{userdata.device_id}', msg)
    


def set_config(userdata: AcUserData, cmd: AcCommandConfig, timestamp_ns = None):
    assert cmd.type == AcCommandType.config

    success = True

    intervals: list[tuple[int, int]] = []
    for item in cmd.value:
        if item.startHour > item.endHour or item.startHour == item.endHour:
            success = False
            break
        if item.startHour < 0 or item.endHour > 23:
            success = False
            break
        if item.temperature < userdata.config.minTemperature or item.temperature > userdata.config.maxTemperature:
            success = False
            break
        if item.mode not in userdata.config.supportedModes:
            success = False
            break

        for interval in intervals:
            i1: tuple[int, int] = interval
            i2: tuple[int, int] = (item.startHour, item.endHour)

            if i1[0] < i2[1] and i2[0] < i1[1]:
                success = False
                break
            if i1[0] == i2[1] and i2[0] == i1[1]:
                success = False
                break
        if not success:
            break

        intervals.append((item.startHour, item.endHour))

    if success:
        userdata.config.config = cmd.value
        cfg.save_config(userdata.device_id, userdata.config)

    fields = {
        "user_id": cmd.userId,
        "config": json.dumps([asdict(c) for c in cmd.value], separators=(',', ':')),
        "uuid": cmd.uuid,
    }
    tags = {
        "type": "config",   
        "status": success
    }
    if timestamp_ns is None:
        msg = make_msg(userdata.device_id, MessageType.AC_COMMAND, fields=fields, tags=tags)
        print(cmd.userId, cmd.value)
        userdata.client.publish(f'ac_w/{userdata.device_id}', msg)
    else:
        msg = make_msg_at(userdata.device_id, MessageType.AC_COMMAND, fields=fields, tags=tags, when_nanoseconds=timestamp_ns)
        userdata.client.publish(f'ac_w/{userdata.device_id}', msg)


def _simulator_loop_BULK_DATA(client: mqtt.Client, userdata: AcUserData):
    now = datetime.now()
    ns_IN_s = 1000000000 # Nanoseconds in a microsecond
    virtual_now = now - timedelta(days=30*3)
    three_months_ago = (now - timedelta(days=30*3)).timestamp()
    timestamp = int(virtual_now.timestamp() * ns_IN_s)
    i = 0

    while True:
        command = random.choice(['turn_on', 'temperature', 'mode', 'config'])
        user_id = random.choice([-1, -1, -1, 1, 2, 3, 4, 5]) # NOTE: Access control is not considered.

        if command == 'turn_on':       
            is_on_or_off = random.choice([True, False])
            set_is_turned_on(userdata, AcCommandIsOn(user_id, AcCommandType.is_on, is_on_or_off, str(uuid4())), timestamp)
            pass
        elif command == 'temperature':
            temperature = random.uniform(10, 30)
            set_temperature(userdata, AcCommandTemperature(user_id, AcCommandType.temperature, temperature, str(uuid4())), timestamp)
            pass
        elif command == 'mode':
            mode = random.choice([AcMode.auto, AcMode.cool, AcMode.fan, AcMode.heat])
            set_mode(userdata, AcCommandMode(user_id, AcCommandType.mode, mode, str(uuid4())), timestamp)
        elif command == 'config':
            config = [] # Randomizing a JSON with a tight schema? Get out of here.
            set_config(userdata, AcCommandConfig(user_id, AcCommandType.config, config, str(uuid4())), timestamp)


        # Next tick
        # Unlike a read, a command does not occur every 10 (or however many) seconds.
        # We will assume that every AC is on average used once per day. During the
        # cold season (last three months to Feb), this assumption may suffice.
        # That means that commands on average are 12 hours apart. I'll round it
        # to 5 hours. If this were summer, we would amp up the usage tenfold.
        virtual_now = virtual_now + timedelta(seconds=5 * 3600)
        timestamp = int(virtual_now.timestamp() * ns_IN_s)
        i += 1
        if i % 6 == 0:
            print('ac\t', virtual_now.timestamp(), datetime.now().timestamp(), f'{int((virtual_now.timestamp() - three_months_ago) / (datetime.now().timestamp() - three_months_ago) * 100)}%')

            if virtual_now.timestamp() >= datetime.now().timestamp():
                return

def _simulator_loop(client: mqtt.Client, userdata: AcUserData):
    if BULK_DATA:
        _simulator_loop_BULK_DATA(client, userdata)
    else:
        sleep_duration = 10
        actual_temperature = int((userdata.config.minTemperature + userdata.config.maxTemperature) / 2)

        current_day = -1
        acknowledged_configs = []
        is_on_currently = False # We need this so that the AC turns off exactly once after a custom regime item ends.

        while True:
            # If a day changes, reset acknowledged_configs.
            today = datetime.now().day
            if today != current_day:
                acknowledged_configs.clear()
                current_day = today

            # Search through custom config, apply config if needed.
            # If the config was already applied, ignore it.
            if len(userdata.config.config) > 0:
                hour = datetime.now().hour
                foundAny = False
                foundAnyAcknowledged = False

                for conf in userdata.config.config:
                    # If this config is the current one, apply its mode and temperature.
                    # This device will not acknowledge this config anymore.
                    if hour >= conf.startHour and hour < conf.endHour:
                        foundAny = True
                        # If acknowledged once, ignore. That way the user can send commands
                        # even if the device is running in custom mode.
                        if conf in acknowledged_configs:  
                            break

                        foundAnyAcknowledged = True

                        set_mode(userdata, AcCommandMode(-1, AcCommandType.mode, conf.mode, str(uuid4())))
                        set_temperature(userdata, AcCommandTemperature(-1, AcCommandType.temperature, conf.temperature, str(uuid4())))
                        acknowledged_configs.append(conf)
                        break

                if foundAny:
                    is_on_currently = True

                # If a config was found in the previous loop, turn on the AC.
                # Otherwise, turn it off.
                if foundAny and not userdata.state.is_on and foundAnyAcknowledged:
                    set_is_turned_on(userdata, AcCommandIsOn(-1, AcCommandType.is_on, True, str(uuid4())))    
                elif not foundAny and userdata.state.is_on and is_on_currently:
                    set_is_turned_on(userdata, AcCommandIsOn(-1, AcCommandType.is_on, False, str(uuid4())))
                    is_on_currently = False

            # While it is running
            if userdata.state.is_on:
                if userdata.state.mode == AcMode.cool:
                    if actual_temperature > userdata.state.temperature:
                        actual_temperature -= 0.2
                elif userdata.state.mode == AcMode.heat:
                    if actual_temperature < userdata.state.temperature:
                        actual_temperature += 0.2
                elif userdata.state.mode == AcMode.auto:
                    if actual_temperature > userdata.state.temperature:
                        actual_temperature -= 0.2              
                    if actual_temperature < userdata.state.temperature:
                        actual_temperature += 0.2
            time.sleep(sleep_duration)


'''
Main
'''

def run(device_id: int, property_id: int, is_bulk: bool):
    global BULK_DATA
    BULK_DATA = is_bulk

    config, success = cfg.load_config(device_id)
    if not success:
        config = generate_config()
        cfg.save_config(device_id, config)
    else:
        config = AcConfig(**config)
        config.config = [AcCustomModeItem(**d) for d in config.config]
    state = AcState(config.minTemperature, False, config.supportedModes[0])

    userdata = AcUserData(device_id, property_id, config, None, state)
    client = _make_and_connect_client(userdata)

    ping_thread = threading.Thread(target=lambda: _ping_loop(client, device_id, property_id), daemon=True)
    ping_thread.start()

    simulation_thread = threading.Thread(target = lambda: _simulator_loop(client, userdata), daemon=True)
    simulation_thread.start()

    try:
        client.loop_forever()
    except KeyboardInterrupt:
        client.disconnect()