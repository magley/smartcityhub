from datetime import datetime, timedelta
import math
import time
from uuid import uuid4
import paho.mqtt.client as mqtt
import json
from properties import properties
import threading
from common.message import *
from dataclasses import asdict, dataclass
import random
import sim.config as cfg

BULK_DATA = False # Set to True to quickly generate data from the last 3 months instead of real-time.

# Plates ending with 000 will be whitelisted (first three)
PLATES = [
    'NS-387-000', 'NS-593-000', 'NS-033-000', 'NS-594-496', 'NS-485-321', 'NS-403-485', 'NS-495-544',
]


@dataclass
class GateConfig():
    plates: list[str]


class SensorEvent(str, enum.Enum):
    ARRIVED = 'arrived'
    LEFT = 'left'
    PASSED = 'passed'


class Direction(str, enum.Enum):
    ENTRY = 'entry'
    EXIT = 'exit'


@dataclass
class AtGate:
    plate: str
    dir: Direction


class GateMode(str, enum.Enum):
    PRIVATE = 'private'
    PUBLIC = 'public'


class GateOpen(str, enum.Enum):
    OPEN = 'open'
    CLOSED = 'closed'
    OPENING = 'opening'
    CLOSING = 'closing'


class GateOverride(str, enum.Enum):
    NO = 'no'
    YES = 'yes'


@dataclass
class GateState:
    open_state: GateOpen
    mode: GateMode
    override: GateOverride


@dataclass
class GateUserData():
    device_id: int
    property_id: int
    client: mqtt.Client # Initially None, upon connection to InfluxDB it will be set to the client object
    state: GateState
    whitelisted: list[str]
    open_close_len_in_s: float
    started_open_close: datetime | None


class GateCommandType(str, enum.Enum):
    open_state = 'open_state'
    mode = 'mode'
    override = 'override'
    sensor_exit = 'sensor_exit'
    sensor_entry = 'sensor_entry'


@dataclass
class GateCommand:
    userId: int
    type: GateCommandType
    value: any
    uuid: str


@dataclass
class GateCommandOpenState:
    userId: int
    type: GateCommandType
    value: GateOpen
    uuid: str


@dataclass
class GateCommandOverride:
    userId: int
    type: GateCommandType
    value: GateOverride
    uuid: str


@dataclass
class GateCommandMode:
    userId: int
    type: GateCommandType
    value: GateMode
    uuid: str


@dataclass
class GateCommandSensor:
    userId: int
    type: GateCommandType
    value: list  # [plates<str>, direction<Direction>, event<SensorEvent>]
    uuid: str


'''
Config
'''


def generate_config() -> GateConfig:
    cfg = GateConfig([PLATES[0], PLATES[1], PLATES[2]])
    print(f"[Gate::generate_config()]: {cfg}")
    return cfg


'''
MQTT Callbacks and helper functions. 
'''

def _on_connect(client: mqtt.Client, userdata: GateUserData, flags, result_code):
    client.subscribe(f"gate/cmd/{userdata.device_id}")
    print("Subbed to", f"gate/cmd/{userdata.device_id}")


def _on_receive(client: mqtt.Client, userdata: any, msg: mqtt.MQTTMessage):
    cmd: GateCommand = GateCommand(**json.loads(msg.payload))
    cmd.type = GateCommandType(cmd.type) # Force it to be GateCommandType, not string.

    if cmd.type == GateCommandType.open_state:
        set_gate_state(userdata, GateCommandOpenState(**cmd.__dict__))
    elif cmd.type == GateCommandType.mode:
        set_mode(userdata, GateCommandMode(**cmd.__dict__))
    elif cmd.type == GateCommandType.override:
        set_override(userdata, GateCommandOverride(**cmd.__dict__))
    else:
        # Ignoring GateCommandType.sensor
        print(f'Unknown command {cmd.type}, ignoring.')


def _on_send(client: mqtt.Client, userdata: any, mid: any):
    pass


def _on_disconnect(client: mqtt.Client, userdata: any, on_disconnect):
    pass


def _make_and_connect_client(userdata: GateUserData) -> mqtt.Client:
    client = mqtt.Client()
    userdata.client = client
    client.on_connect = _on_connect
    client.on_disconnect = _on_disconnect
    client.on_message = _on_receive
    client.on_publish = _on_send
    client.user_data_set(userdata)
    client.username_pw_set(properties['username'], properties['password'])
    client.connect(properties['host'], port=properties['port'])
    return client


'''
Ping logic
'''

def _ping_loop(client: mqtt.Client, device_id: int, property_id: int):
    sleep_duration = properties['ping_freq_seconds']
    topic = f'ping/{property_id}'
    i = 0
    if BULK_DATA:
        now = datetime.now()
        ns_IN_s = 1000000000 # Nanoseconds in a microsecond
        virtual_now = now - timedelta(days=30*3)
        three_months_ago = (now - timedelta(days=30*3)).timestamp()
        timestamp = int(virtual_now.timestamp() * ns_IN_s)

        while True:
            msg = make_msg_at(device_id, MessageType.PING, {'ignore': 0}, {}, timestamp)
            client.publish(topic=topic, payload=msg)

            # Next tick
            virtual_now = virtual_now + timedelta(seconds=sleep_duration)
            timestamp = int(virtual_now.timestamp() * ns_IN_s)

            i += 1
            if i % 6 == 0:
                print('ping\t', virtual_now.timestamp(), datetime.now().timestamp(), f'{int((virtual_now.timestamp() - three_months_ago) / (datetime.now().timestamp() - three_months_ago) * 100)}%')

                if virtual_now.timestamp() >= datetime.now().timestamp():
                    return
    else:
        while True:
            msg = make_msg(device_id, MessageType.PING, {'ignore': 0}, {})
            print(topic, msg)
            client.publish(topic=topic, payload=msg)
            time.sleep(sleep_duration)


'''
Simulator
'''


def set_gate_state(userdata: GateUserData, cmd: GateCommandOpenState, timestamp_ns = None):
    if type(cmd.value) == str:
        cmd.value = GateOpen(cmd.value)
    assert cmd.type == GateCommandType.open_state and type(cmd.value) is GateOpen

    userdata.state.open_state = cmd.value
    if cmd.value == GateOpen.OPENING or cmd.value == GateOpen.CLOSING:
        userdata.started_open_close = datetime.now()
    else:
        userdata.started_open_close = None
    
    fields = {
        "user_id": cmd.userId,
        "uuid": cmd.uuid,
    }
    tags = {
        "type": "open_state",
        "open_state": cmd.value.value,
        "status": True
    }

    if timestamp_ns is None:
        msg = make_msg(userdata.device_id, MessageType.GATE_COMMAND, fields=fields, tags=tags)
        print(cmd.userId, cmd.value.value)
        userdata.client.publish(f'gate_w/{userdata.device_id}', msg)
    else:
        msg = make_msg_at(userdata.device_id, MessageType.GATE_COMMAND, fields=fields, tags=tags, when_nanoseconds=timestamp_ns)
        userdata.client.publish(f'gate_w/{userdata.device_id}', msg)


def set_mode(userdata: GateUserData, cmd: GateCommandMode):
    if type(cmd.value) == str:
        cmd.value = GateMode(cmd.value)
    assert cmd.type == GateCommandType.mode and type(cmd.value) is GateMode

    userdata.state.mode = cmd.value

    fields = {
        "user_id": cmd.userId,
        "uuid": cmd.uuid,
    }
    tags = {
        "type": "mode",
        "mode": cmd.value.value,
        "status": True
    }
    msg = make_msg(userdata.device_id, MessageType.GATE_COMMAND, fields=fields, tags=tags)
    print(cmd.userId, cmd.value.value)
    userdata.client.publish(f'gate_w/{userdata.device_id}', msg)


def set_override(userdata: GateUserData, cmd: GateCommandOverride):
    if type(cmd.value) == str:
        cmd.value = GateOverride(cmd.value)
    assert cmd.type == GateCommandType.override and type(cmd.value) is GateOverride

    userdata.state.override = cmd.value

    fields = {
        "user_id": cmd.userId,
        "uuid": cmd.uuid,
    }
    tags = {
        "type": "override",
        "override": cmd.value.value,
        "status": True
    }
    msg = make_msg(userdata.device_id, MessageType.GATE_COMMAND, fields=fields, tags=tags)
    print(cmd.userId, cmd.value.value)
    userdata.client.publish(f'gate_w/{userdata.device_id}', msg)


def set_sensor(userdata: GateUserData, cmd: GateCommandSensor):
    if type(cmd.value[1]) is str:
        cmd.value[1] = Direction(cmd.value[1])
    if type(cmd.value[2]) is str:
        cmd.value[2] = SensorEvent(cmd.value[2])
    assert (cmd.type == GateCommandType.sensor_exit or cmd.type == GateCommandType.sensor_entry) and type(cmd.value) is list
    assert type(cmd.value[0]) == str and type(cmd.value[1]) == Direction and type(cmd.value[2]) == SensorEvent

    # Sensor state is part of simulator, not userdata

    fields = {
        "user_id": cmd.userId,
        "uuid": cmd.uuid,
    }
    tags = {
        "type": cmd.type.value,
        "plate": cmd.value[0],
        "direction": cmd.value[1].value,
        "event": cmd.value[2].value,
        "status": True
    }
    msg = make_msg(userdata.device_id, MessageType.GATE_COMMAND, fields=fields, tags=tags)
    print(cmd.userId, cmd.value)
    userdata.client.publish(f'gate_w/{userdata.device_id}', msg)


def set_sensor_sim(userdata: GateUserData, at_gate: AtGate, event: SensorEvent):
    if at_gate.dir == Direction.EXIT:
        set_sensor(userdata, GateCommandSensor(-1, GateCommandType.sensor_exit, [at_gate.plate, at_gate.dir, event], str(uuid4())))
    else:
        set_sensor(userdata, GateCommandSensor(-1, GateCommandType.sensor_entry, [at_gate.plate, at_gate.dir, event], str(uuid4())))


def set_gate_state_sim(userdata: GateUserData, open: GateOpen, timestamp = None):
    set_gate_state(userdata, GateCommandOpenState(-1, GateCommandType.open_state, open, str(uuid4())), timestamp)


def generate(plate_to_exclude: AtGate | None, direction: Direction):
    while True:
        plate = PLATES[random.randint(0, len(PLATES) - 1)]
        if not plate_to_exclude:
            break
        if plate != plate_to_exclude.plate:
            break
    return AtGate(plate, direction)


def _simulator_loop(client: mqtt.Client, userdata: GateUserData):
    if BULK_DATA:
        _simulator_loop_BULK_DATA(client, userdata)
    else:
        _simulator_loop_regular(client, userdata)


def _simulator_loop_BULK_DATA(client: mqtt.Client, userdata: GateUserData):
    now = datetime.now()
    ns_IN_s = 1000000000 # Nanoseconds in a microsecond
    virtual_now = now - timedelta(days=30*3)
    three_months_ago = (now - timedelta(days=30*3)).timestamp()
    timestamp = int(virtual_now.timestamp() * ns_IN_s)
    i = 0

    while True:
        command = random.choice(['open', 'closed'])

        if command == 'open':
            set_gate_state_sim(userdata, GateOpen.OPEN, timestamp)
        else:
            set_gate_state_sim(userdata, GateOpen.CLOSED, timestamp)

        # Next tick
        # Assuming a gate is opened/closed 4 times a day = every 6 hours
        virtual_now = virtual_now + timedelta(seconds=6 * 3600)
        timestamp = int(virtual_now.timestamp() * ns_IN_s)
        i += 1
        if i % 6 == 0:
            print('gate\t', virtual_now.timestamp(), datetime.now().timestamp(), f'{int((virtual_now.timestamp() - three_months_ago) / (datetime.now().timestamp() - three_months_ago) * 100)}%')

            if virtual_now.timestamp() >= datetime.now().timestamp():
                return



def _simulator_loop_regular(client: mqtt.Client, userdata: GateUserData):
    prev_to_enter: AtGate | None = None
    to_enter: AtGate | None = None

    prev_to_exit: AtGate | None = None
    to_exit: AtGate | None = None

    should_generate_to_enter = lambda: random.random() > 0.6
    should_generate_to_exit = lambda: random.random() > 0.9

    to_enter_should_leave = lambda: random.random() > 0.6
    to_exit_should_leave = lambda: random.random() > 0.6

    while True:
        # --------------------------
        # Update gate state - complete gate transition from opening to open / closing to closed
        if userdata.started_open_close:
            if (datetime.now() - userdata.started_open_close).total_seconds() > userdata.open_close_len_in_s:
                if userdata.state.open_state == GateOpen.OPENING:
                    set_gate_state_sim(userdata, GateOpen.OPEN)
                elif userdata.state.open_state == GateOpen.CLOSING:
                    set_gate_state_sim(userdata, GateOpen.CLOSED)
        # --------------------------

        # --------------------------
        # Only update car at gate while gate is not open/opening (for sake of more predictable simulation)
        if userdata.state.open_state != GateOpen.OPENING and userdata.state.open_state != GateOpen.OPEN:
            # Chance for car at gate to leave
            if to_enter:
                to_enter = None if to_enter_should_leave() else to_enter
            # Generate new car if none at gate
            if not to_enter:
                to_enter = generate(to_exit, Direction.ENTRY) if should_generate_to_enter() else to_enter
            
            if to_exit:
                to_exit = None if to_exit_should_leave() else to_exit
            if not to_exit:
                to_exit = generate(to_enter, Direction.EXIT) if should_generate_to_exit() else to_exit
        # --------------------------

        # --------------------------  
        # Car detection for when car arrives at/leaves gate
        if to_enter:
            if prev_to_enter != to_enter:
                if prev_to_enter:
                    set_sensor_sim(userdata, prev_to_enter, SensorEvent.LEFT)
                set_sensor_sim(userdata, to_enter, SensorEvent.ARRIVED)
                prev_to_enter = to_enter
        elif prev_to_enter:
            set_sensor_sim(userdata, prev_to_enter, SensorEvent.LEFT)
            prev_to_enter = None

        if to_exit:
            if prev_to_exit != to_exit:
                if prev_to_exit:
                    set_sensor_sim(userdata, prev_to_exit, SensorEvent.LEFT)
                set_sensor_sim(userdata, to_exit, SensorEvent.ARRIVED)
                prev_to_exit = to_exit
        elif prev_to_exit:
            set_sensor_sim(userdata, prev_to_exit, SensorEvent.LEFT)
            prev_to_exit = None
        # --------------------------

        # --------------------------
        # Car passing through gate logic
        if to_enter:
            if userdata.state.open_state == GateOpen.OPEN: 
                # Gate is open, just pass through
                set_sensor_sim(userdata, to_enter, SensorEvent.PASSED)
                to_enter = None
                prev_to_enter = None
            elif userdata.state.open_state == GateOpen.CLOSED or userdata.state.open_state == GateOpen.CLOSING:
                if userdata.state.override == GateOverride.NO:
                    if userdata.state.mode == GateMode.PUBLIC:
                        set_gate_state_sim(userdata, GateOpen.OPENING)
                    elif userdata.state.mode == GateMode.PRIVATE and to_enter.plate in userdata.whitelisted:
                        set_gate_state_sim(userdata, GateOpen.OPENING)
        
        # to_exit logic is slightly different
        if to_exit:
            if userdata.state.open_state == GateOpen.OPEN: 
                # Gate is open, just pass through
                set_sensor_sim(userdata, to_exit, SensorEvent.PASSED)
                to_exit = None
                prev_to_exit = None
            elif userdata.state.open_state == GateOpen.CLOSED or userdata.state.open_state == GateOpen.CLOSING:
                if userdata.state.override == GateOverride.NO:
                    # Don't check userdata.state.mode, just start opening gate
                    set_gate_state_sim(userdata, GateOpen.OPENING)
        
        if not to_enter and not to_exit and userdata.state.open_state == GateOpen.OPEN:
            if userdata.state.override == GateOverride.NO:
                set_gate_state_sim(userdata, GateOpen.CLOSING)
        # --------------------------

        time.sleep(5)


'''
Main
'''

def run(device_id: int, property_id: int, is_bulk: bool):
    global BULK_DATA
    BULK_DATA = is_bulk

    config, success = cfg.load_config(device_id)
    if not success:
        config = generate_config()
        cfg.save_config(device_id, config)
    else:
        config = GateConfig(**config)
    state = GateState(GateOpen.CLOSED, GateMode.PRIVATE, GateOverride.NO)
    
    # Should be longer than telegraf batch interval (10s) for sake of avoiding spinner soft-lock on front.
    # Note that because of the simulator's sleep this value won't be exact.
    # For example, a 5s simulator sleep with 11s open_close_len_in_s will cause the wait to be 15s
    open_close_len_in_s = 11
    userdata = GateUserData(device_id, property_id, None, state, config.plates, open_close_len_in_s, None)
    client = _make_and_connect_client(userdata)

    ping_thread = threading.Thread(target=lambda: _ping_loop(client, device_id, property_id), daemon=True)
    ping_thread.start()

    simulation_thread = threading.Thread(target = lambda: _simulator_loop(client, userdata), daemon=True)
    simulation_thread.start()

    try:
        client.loop_forever()
    except KeyboardInterrupt:
        client.disconnect()
