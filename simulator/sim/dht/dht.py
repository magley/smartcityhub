from datetime import datetime, timedelta
import math
import time
import paho.mqtt.client as mqtt
import json
from properties import properties
import threading
from common.message import *
from random import random


BULK_DATA = False # Set to True to quickly generate data from the last 3 months instead of real-time.


if __name__ == "__main__":
    print("This module cannot be run manually! Use Root main.py.")
    exit(1)


'''
MQTT Callbacks and helper functions. 
'''

def _on_connect(client: mqtt.Client, userdata: any, flags, result_code):
    pass


def _on_receive(client: mqtt.Client, userdata: any, msg: mqtt.MQTTMessage):
    pass


def _on_send(client: mqtt.Client, userdata: any, mid: any):
    pass


def _on_disconnect(client: mqtt.Client, userdata: any, on_disconnect):
    pass


def _make_and_connect_client() -> mqtt.Client:
    client = mqtt.Client()
    client.on_connect = _on_connect
    client.on_disconnect = _on_disconnect
    client.on_message = _on_receive
    client.on_publish = _on_send
    client.username_pw_set(properties['username'], properties['password'])
    client.connect(properties['host'], port=properties['port'])
    return client

'''
Ping logic
'''

def _ping_loop(client: mqtt.Client, device_id: int, property_id: int):
    sleep_duration = properties['ping_freq_seconds']
    topic = f'ping/{property_id}'
    i = 0
    if BULK_DATA:
        now = datetime.now()
        ns_IN_s = 1000000000 # Nanoseconds in a microsecond
        three_months_ago = (now - timedelta(days=30*3)).timestamp()
        virtual_now = now - timedelta(days=30*3)
        timestamp = int(virtual_now.timestamp() * ns_IN_s)

        while True:
            msg = make_msg_at(device_id, MessageType.PING, {'ignore': 0}, {}, timestamp)
            client.publish(topic=topic, payload=msg)

            # Next tick
            virtual_now = virtual_now + timedelta(seconds=sleep_duration)
            timestamp = int(virtual_now.timestamp() * ns_IN_s)

            i += 1
            if i % 6 == 0:
                print('ping\t', virtual_now.timestamp(), datetime.now().timestamp(), f'{int((virtual_now.timestamp() - three_months_ago) / (datetime.now().timestamp() - three_months_ago) * 100)}%')

                if virtual_now.timestamp() >= datetime.now().timestamp():
                    return
    else:
        while True:
            msg = make_msg(device_id, MessageType.PING, {'ignore': 0}, {})
            print(topic, msg)
            client.publish(topic=topic, payload=msg)
            time.sleep(sleep_duration)

'''
Simulator
'''

temp_h = {
    0: 13,
    1: 14,
    2: 14,
    3: 13,
    4: 12,
    5: 13,
    6: 14.2,
    7: 15,
    8: 16.5,
    9: 16.7,
    10: 20,
    11: 20.5,
    12: 21,
    13: 22,
    14: 22,
    15: 21.8,
    16: 21,
    17: 20.6,
    18: 19.2,
    19: 18.8,
    20: 17,
    21: 16.8,
    22: 15.5,
    23: 15,
}

min_base = min(temp_h.values())
max_base = max(temp_h.values())

# https://www.climatestotravel.com/climate/serbia
temp_range_m = {
    1: (-2.6, 4.5),
    2: (-1.4, 7.4),
    3: (2.2, 12.8),
    4: (6.9, 18.4),
    5: (11.5, 23),
    6: (15.2, 26.8),
    7: (16.6, 29),
    8: (16.7, 29.3),
    9: (12.3, 24),
    10: (7.5, 18.4),
    11: (3.2, 11.9),
    12: (-1.2, 5.5),
}

# https://www.researchgate.net/figure/Average-monthly-precipitation-and-humidity-throughout-the-year_fig2_352239998
humidity_m = {
    1: 68,
    2: 68,
    3: 65,
    4: 50,
    5: 53,
    6: 69,
    7: 78,
    8: 80,
    9: 73,
    10: 65,
    11: 72,
    12: 71,
}


def _get_temperature(t: datetime) -> int:
    now = t
    hour = now.hour
    day = now.day
    month = now.month

    # Base temperature
    t = temp_h[hour]

    # Translate to expected monthly min-max range
    min_temp, max_temp = temp_range_m[month]
    k = (max_temp - min_temp) / (max_base - min_base)
    t = (t - min_base) * k + min_temp

    # Precipitation
    prec_base = abs(math.sin((month - 4) * math.pi) * 100 - 10) # in mm
    raining = (prec_base > 300) and (day % 3 == 0)
    if raining:
        t -= 1.2

    # Noise
    noise = random() * 0.5 - 0.26
    t += noise

    return t


def _get_humidity(t: datetime) -> int:
    now = t
    day = now.day
    month = now.month

    # Base humidity
    h = humidity_m[month]

    # Precipitation
    perc_base = abs(math.sin((month - 4) * math.pi) * 100 - 10) # in mm
    raining = (perc_base > 300) and (day % 3 == 0)
    if raining:
        h += 2

    # Noise
    noise = random() * 2 - 1
    h += noise

    return h


def _simulator_loop(client: mqtt.Client, device_id: int, property_id: int):
    topic = f'dht/{device_id}/r'
    if BULK_DATA:
        now = datetime.now()
        ns_IN_s = 1000000000 # Nanoseconds in a microsecond
        three_months_ago = (now - timedelta(days=30*3)).timestamp()
        virtual_now = now - timedelta(days=30*3)
        timestamp = int(virtual_now.timestamp() * ns_IN_s)
        i = 0

        while True:
            payload = {
                'humidity': _get_humidity(virtual_now),
                'temperature': _get_temperature(virtual_now)
            }
            msg = make_msg_at(device_id, MessageType.DHT_READ, fields=payload, tags={}, when_nanoseconds=timestamp)
            client.publish(topic=topic, payload=msg)

            # Next tick
            virtual_now = virtual_now + timedelta(seconds=10)
            timestamp = int(virtual_now.timestamp() * ns_IN_s)

            i += 1
            if i % 6 == 0:
                print('dht\t', virtual_now.timestamp(), datetime.now().timestamp(), f'{int((virtual_now.timestamp() - three_months_ago) / (datetime.now().timestamp() - three_months_ago) * 100)}%')

                if virtual_now.timestamp() >= datetime.now().timestamp():
                    return
    else:
        sleep_duration = 10
        topic = f'dht/{device_id}/r'
        while True:

            payload = {
                'humidity': _get_humidity(datetime.now()),
                'temperature': _get_temperature(datetime.now())
            }

            msg = make_msg(device_id, MessageType.DHT_READ, fields=payload, tags={})
            print(msg)
            client.publish(topic=topic, payload=msg)
            time.sleep(sleep_duration)

'''
Main
'''

def run(device_id: int, property_id: int, is_bulk: bool):
    global BULK_DATA
    BULK_DATA = is_bulk

    client = _make_and_connect_client()

    ping_thread = threading.Thread(target=lambda: _ping_loop(client, device_id, property_id), daemon=True)
    ping_thread.start()

    simulation_thread = threading.Thread(target = lambda: _simulator_loop(client, device_id, property_id), daemon=True)
    simulation_thread.start()

    try:
        client.loop_forever()
    except KeyboardInterrupt:
        client.disconnect()