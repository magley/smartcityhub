from datetime import datetime, timedelta
import math
import time
from uuid import uuid4
import paho.mqtt.client as mqtt
import json
from properties import properties
import threading
from common.message import *
from dataclasses import asdict, dataclass


BULK_DATA = False # Set to True to quickly generate data from the last 3 months instead of real-time.

if __name__ == "__main__":
    print("This module cannot be run manually! Use Root main.py.")
    exit(1)


class LampCommandType(str, enum.Enum):
    is_on = 'is_on',
    mode = 'mode',


class LampMode(str, enum.Enum):
    auto = 'Auto',
    manual = 'Manual',


@dataclass
class LampState():
    is_on: bool
    mode: LampMode
    threshold: float


@dataclass
class LampCommand():
    userId: int
    type: LampCommandType
    value: any
    uuid: str


@dataclass
class LampCommandIsOn():
    userId: int
    type: LampCommandType
    value: bool
    uuid: str


@dataclass
class LampCommandMode():
    userId: int
    type: LampCommandType
    value: list
    uuid: str


@dataclass
class LampUserData():
    device_id: int
    property_id: int
    client: mqtt.Client # Initially None, upon connection to InfluxDB it will be set to the client object
    state: LampState


'''
MQTT Callbacks and helper functions. 
'''

def _on_connect(client: mqtt.Client, userdata: LampUserData, flags, result_code):
    client.subscribe(f"lamp/cmd/{userdata.device_id}")
    print("Subbed to", f"lamp/cmd/{userdata.device_id}")


def _on_receive(client: mqtt.Client, userdata: any, msg: mqtt.MQTTMessage):
    cmd: LampCommand = LampCommand(**json.loads(msg.payload))
    cmd.type = LampCommandType(cmd.type) # Force it to be LampCommandType, not string.

    if cmd.type == LampCommandType.is_on:
        set_is_turned_on(userdata, LampCommandIsOn(**cmd.__dict__))
    elif cmd.type == LampCommandType.mode:
        set_mode(userdata, LampCommandMode(**cmd.__dict__))
    else:
        print(f'Unknown command {cmd.type}, ignoring.')


def _on_send(client: mqtt.Client, userdata: any, mid: any):
    pass


def _on_disconnect(client: mqtt.Client, userdata: any, on_disconnect):
    pass


def _make_and_connect_client(userdata: LampUserData) -> mqtt.Client:
    client = mqtt.Client()
    userdata.client = client
    client.on_connect = _on_connect
    client.on_disconnect = _on_disconnect
    client.on_message = _on_receive
    client.on_publish = _on_send
    client.user_data_set(userdata)
    client.username_pw_set(properties['username'], properties['password'])
    client.connect(properties['host'], port=properties['port'])
    return client


'''
Ping logic
'''

def _ping_loop(client: mqtt.Client, device_id: int, property_id: int):
    sleep_duration = properties['ping_freq_seconds']
    topic = f'ping/{property_id}'
    i = 0
    if BULK_DATA:
        now = datetime.now()
        ns_IN_s = 1000000000 # Nanoseconds in a microsecond
        three_months_ago = (now - timedelta(days=30*3)).timestamp()
        virtual_now = now - timedelta(days=30*3)
        timestamp = int(virtual_now.timestamp() * ns_IN_s)

        while True:
            msg = make_msg_at(device_id, MessageType.PING, {'ignore': 0}, {}, timestamp)
            client.publish(topic=topic, payload=msg)

            # Next tick
            virtual_now = virtual_now + timedelta(seconds=sleep_duration)
            timestamp = int(virtual_now.timestamp() * ns_IN_s)

            i += 1
            if i % 6 == 0:
                print('ping\t', virtual_now.timestamp(), datetime.now().timestamp(), f'{int((virtual_now.timestamp() - three_months_ago) / (datetime.now().timestamp() - three_months_ago) * 100)}%')

                if virtual_now.timestamp() >= datetime.now().timestamp():
                    return
    else:
        while True:
            msg = make_msg(device_id, MessageType.PING, {'ignore': 0}, {})
            print(topic, msg)
            client.publish(topic=topic, payload=msg)
            time.sleep(sleep_duration)


'''
Simulator
'''

def set_is_turned_on(userdata: LampUserData, cmd: LampCommandIsOn, timestamp_ns = None):
    assert cmd.type == LampCommandType.is_on and type(cmd.value) is bool

    userdata.state.is_on = cmd.value

    fields = {
        "user_id": cmd.userId,
        "uuid": cmd.uuid,
    }
    tags = {
        "type": "is_on",
        "is_on": cmd.value,
        "status": True
    }
    if timestamp_ns is None:
        msg = make_msg(userdata.device_id, MessageType.LAMP_COMMAND, fields=fields, tags=tags)
        print(cmd.userId, cmd.value)
        userdata.client.publish(f'lamp_w/{userdata.device_id}', msg)
    else:
        msg = make_msg_at(userdata.device_id, MessageType.LAMP_COMMAND, fields=fields, tags=tags, when_nanoseconds=timestamp_ns)
        userdata.client.publish(f'lamp_w/{userdata.device_id}', msg)



def set_mode(userdata: LampUserData, cmd: LampCommandMode, timestamp_ns = None):
    if type(cmd.value[0]) == str:
        cmd.value[0] = LampMode(cmd.value[0])
    assert cmd.type == LampCommandType.mode and type(cmd.value) is list
    

    success = True
    if cmd.value[1] < 0 or cmd.value[1] > 1000:
        success = False

    if success:
        userdata.state.mode = LampMode(cmd.value[0])
        userdata.state.threshold = cmd.value[1]
    
    fields = {
        "user_id": cmd.userId,
        "uuid": cmd.uuid,
        "threshold": cmd.value[1],
    }
    tags = {
        "type": "mode",
        "mode": cmd.value[0].value,
        "status": success
    }

    if timestamp_ns is None:
        msg = make_msg(userdata.device_id, MessageType.LAMP_COMMAND, fields=fields, tags=tags)
        print(cmd.userId, cmd.value)
        userdata.client.publish(f'lamp_w/{userdata.device_id}', msg)
    else:
        msg = make_msg_at(userdata.device_id, MessageType.LAMP_COMMAND, fields=fields, tags=tags, when_nanoseconds=timestamp_ns)
        userdata.client.publish(f'lamp_w/{userdata.device_id}', msg)


lux_h = {
    0: 1,
    1: 2,
    2: 5,
    3: 10,
    4: 30,
    5: 80,
    6: 400,
    7: 450,
    8: 500,
    9: 700,
    10: 800,
    11: 900,
    12: 1000,
    13: 950,
    14: 900,
    15: 850,
    16: 800,
    17: 750,
    18: 700,
    19: 600,
    20: 400,
    21: 200,
    22: 50,
    23: 10,
}

import random
def _get_illuminance(t: datetime):
    now = t
    hour = now.hour
    min = now.minute
    cur_lux = lux_h[hour]
    next_lux = lux_h[(hour + 1) % 24]
    diff = next_lux - cur_lux
    diff_inc = diff / 60
    lux = cur_lux + diff_inc * min
    lux += lux * random.random() * 0.1
    return lux


def _simulator_loop(client: mqtt.Client, userdata: LampUserData):
    if BULK_DATA:
        threading.Thread(target = lambda: _simulator_loop_BULK_DATA_reads(client, userdata), daemon=True).start()
        threading.Thread(target = lambda: _simulator_loop_BULK_DATA_commands(client, userdata), daemon=True).start()
    else:
        threading.Thread(target = lambda: _simulator_loop_regular(client, userdata), daemon=True).start()


def _simulator_loop_BULK_DATA_reads(client: mqtt.Client, userdata: LampUserData):
    topic = f'lamp_m/{userdata.device_id}'
    now = datetime.now()
    ns_IN_s = 1000000000 # Nanoseconds in a microsecond
    three_months_ago = (now - timedelta(days=30*3)).timestamp()
    virtual_now = now - timedelta(days=30*3)
    timestamp = int(virtual_now.timestamp() * ns_IN_s)
    i = 0

    while True:
        payload = {
            'illuminance': _get_illuminance(virtual_now),
        }
        msg = make_msg_at(userdata.device_id, MessageType.LAMP_READ_M, fields=payload, tags={}, when_nanoseconds=timestamp)
        client.publish(topic=topic, payload=msg)

        # Next tick
        virtual_now = virtual_now + timedelta(seconds=10)
        timestamp = int(virtual_now.timestamp() * ns_IN_s)

        i += 1
        if i % 6 == 0:
            print('lampR\t', virtual_now.timestamp(), datetime.now().timestamp(), f'{int((virtual_now.timestamp() - three_months_ago) / (datetime.now().timestamp() - three_months_ago) * 100)}%')

        if i // 6 >= 129600:
            return



def _simulator_loop_BULK_DATA_commands(client: mqtt.Client, userdata: LampUserData):
    now = datetime.now()
    ns_IN_s = 1000000000 # Nanoseconds in a microsecond
    virtual_now = now - timedelta(days=30*3)
    three_months_ago = (now - timedelta(days=30*3)).timestamp()
    timestamp = int(virtual_now.timestamp() * ns_IN_s)
    i = 0

    while True:
        command = random.choice(['is_on', 'mode'])
        user_id = random.choice([-1, -1, -1, 1, 2, 3, 4, 5]) # NOTE: Access control is not considered.

        if command == 'is_on':       
            is_on_or_off = random.choice([True, False])
            set_is_turned_on(userdata, LampCommandIsOn(user_id, LampCommandType.is_on, is_on_or_off, str(uuid4())), timestamp)
        elif command == 'mode':
            mode = random.choice([LampMode.auto, LampMode.manual])
            set_mode(userdata, LampCommandMode(user_id, LampCommandType.mode, [mode, random.uniform(0, 50)], str(uuid4())), timestamp)

        # Next tick
        # Assuming a command is triggered once every 12 hours (day/night on/off)
        virtual_now = virtual_now + timedelta(seconds=12 * 3600)
        timestamp = int(virtual_now.timestamp() * ns_IN_s)
        i += 1
        if i % 6 == 0:
            print('lampW\t', virtual_now.timestamp(), datetime.now().timestamp(), f'{int((virtual_now.timestamp() - three_months_ago) / (datetime.now().timestamp() - three_months_ago) * 100)}%')

            if virtual_now.timestamp() >= datetime.now().timestamp():
                return


def _simulator_loop_regular(client: mqtt.Client, userdata: LampUserData):
    sleep_duration = 5
    topic = f'lamp_m/{userdata.device_id}'
    while True:
        # Read current illuminance
        illuminance = _get_illuminance(datetime.now())
        payload = {
            'illuminance': illuminance,
        }

        msg = make_msg(userdata.device_id, MessageType.LAMP_READ_M, fields=payload, tags={})
        client.publish(topic=topic, payload=msg)

        # Update simulator
        if userdata.state.mode == LampMode.auto:
            if illuminance >= userdata.state.threshold and userdata.state.is_on:
                set_is_turned_on(userdata, LampCommandIsOn(-1, LampCommandType.is_on, False, str(uuid4())))
            if illuminance < userdata.state.threshold and not userdata.state.is_on:
                set_is_turned_on(userdata, LampCommandIsOn(-1, LampCommandType.is_on, True, str(uuid4())))

        time.sleep(sleep_duration)

'''
Main
'''

def run(device_id: int, property_id: int, is_bulk: bool):
    global BULK_DATA
    BULK_DATA = is_bulk

    state = LampState(False, LampMode.manual, 5.0)
    
    userdata = LampUserData(device_id, property_id, None, state)
    client = _make_and_connect_client(userdata)

    ping_thread = threading.Thread(target=lambda: _ping_loop(client, device_id, property_id), daemon=True)
    ping_thread.start()

    _simulator_loop(client, userdata)

    try:
        client.loop_forever()
    except KeyboardInterrupt:
        client.disconnect()
