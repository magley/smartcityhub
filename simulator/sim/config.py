
import json
from dataclasses import dataclass, asdict

def load_config(device_id: int) -> tuple[dict, bool]:
    """
    ### Returns

    On success, the JSON config as a python `dict`, and `True`.

    On failure, `None` and `False`.
    """

    try:
        with open(f"data/{device_id}.cfg") as f:
            return json.loads(f.read()), True
    except OSError:
        return None, False


def save_config(device_id: int, config: object):
    with open(f"data/{device_id}.cfg", "w") as f:
        f.write(json.dumps(asdict(config)))


def save_config_dict(device_id: int, config: dict):
    with open(f"data/{device_id}.cfg", "w") as f:
        f.write(json.dumps(config))