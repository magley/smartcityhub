import time
import paho.mqtt.client as mqtt
import json
from properties import properties
import threading
from common.message import *
from uuid import uuid4
import datetime
from datetime import timedelta
import random


BULK_DATA = False # Set to True to quickly generate data from the last 3 months instead of real-time.


'''
SprinklersTrigger: {
    time: time
    days: int[] # "Mon(0)/Tue(1)/Wed(2)/Thu(3)/Fri(4)/Sat(5)/Sun(6)"
    isOn: bool
    triggeredToday: bool
}

SprinklersTriggerReceived: {
    hour: int
    minute: int
    days: int[] # "Mon(0)/Tue(1)/Wed(2)/Thu(3)/Fri(4)/Sat(5)/Sun(6)"
    isOn: bool
}

SprinklersState: {
    isOn: bool
    triggers: SprinklersTrigger[]
}

SprinklersUserdata: {
    device_id: int
    property_id: int
    client: mqtt.Client # Initially None, upon connection to InfluxDB it will be set to the client object
    state: SprinklersState
}

SprinklersCommandType: (
    isOn,
    setTriggers,
)

SprinklersCmdIsOn: {
    userId: int
    type: SprinklersCommandType
    value: bool
    uuid: str
}

SprinklersCmdSetTriggers: {
    userId: int
    type: SprinklersCommandType
    value: SprinklersTriggerReceived[]
    uuid: str
}
'''


if __name__ == "__main__":
    print("This module cannot be run manually! Use Root main.py.")
    exit(1)


'''
MQTT
'''

def _on_connect(client: mqtt.Client, userdata: dict, flags, result_code):
    client.subscribe(f"sprinklers/cmd/{userdata['device_id']}")
    print("Subbed to", f"sprinklers/cmd/{userdata['device_id']}")


def _on_receive(client: mqtt.Client, userdata: dict, msg: mqtt.MQTTMessage):
    msg_dict = json.loads(msg.payload)
    if msg_dict['type'] == 'isOn':
        set_is_on(userdata, msg_dict)
    elif msg_dict['type'] == 'setTriggers':
        set_triggers(userdata, msg_dict)
    else:
        print(f'Unknown command {msg_dict["type"]} in {msg_dict}, ignoring.')


def _on_send(client: mqtt.Client, userdata: dict, mid: any):
    pass


def _on_disconnect(client: mqtt.Client, userdata: dict, on_disconnect):
    pass


def _make_and_connect_client(userdata: dict) -> mqtt.Client:
    client = mqtt.Client()
    userdata['client'] = client
    client.on_connect = _on_connect
    client.on_disconnect = _on_disconnect
    client.on_message = _on_receive
    client.on_pub_cmd = _on_send
    client.user_data_set(userdata)
    client.username_pw_set(properties['username'], properties['password'])
    client.connect(properties['host'], port=properties['port'])
    return client


'''
Ping logic
'''

def _ping_loop(client: mqtt.Client, device_id: int, property_id: int):
    sleep_duration = properties['ping_freq_seconds']
    topic = f'ping/{property_id}'
    i = 0
    if BULK_DATA:
        now = datetime.datetime.now()
        ns_IN_s = 1000000000 # Nanoseconds in a microsecond
        three_months_ago = (now - timedelta(days=30*3)).timestamp()
        virtual_now = now - timedelta(days=30*3)
        timestamp = int(virtual_now.timestamp() * ns_IN_s)

        while True:
            msg = make_msg_at(device_id, MessageType.PING, {'ignore': 0}, {}, timestamp)
            client.publish(topic=topic, payload=msg)

            # Next tick
            virtual_now = virtual_now + timedelta(seconds=sleep_duration)
            timestamp = int(virtual_now.timestamp() * ns_IN_s)

            i += 1
            if i % 6 == 0:
                print('ping\t', virtual_now.timestamp(), datetime.datetime.now().timestamp(), f'{int((virtual_now.timestamp() - three_months_ago) / (datetime.datetime.now().timestamp() - three_months_ago) * 100)}%')

                if virtual_now.timestamp() >= datetime.datetime.now().timestamp():
                    return
    else:
        while True:
            msg = make_msg(device_id, MessageType.PING, {'ignore': 0}, {})
            print(topic, msg)
            client.publish(topic=topic, payload=msg)
            time.sleep(sleep_duration)


'''
Simulator
'''

def _validate_triggers(triggers: list):
    '''
    Make sure there aren't overlapping triggers
    '''
    for idx, trg in enumerate(triggers):
        for trg2 in triggers[idx+1:]:
            if trg['hour'] != trg2['hour'] or trg['minute'] != trg2['minute']:
                continue
            for trg_day in trg['days']:
                if trg_day in trg2['days']:
                    return False
    return True


def set_triggers(userdata: dict, cmd: dict):
    triggers = cmd['value']
    cmd_uuid = cmd['uuid']
    cmd_user_id = cmd['userId']
    success = _validate_triggers(triggers)

    if success:
        n_triggers = []
        for trg in triggers:
            trg_time = datetime.time(hour=trg['hour'], minute=trg['minute'])
            n_trigger = {
                "time": trg_time,
                "days": trg["days"],
                "isOn": trg["isOn"],
                "triggeredToday": False,
            }
            
            # See if there are any duplicates that can replace n_trigger to preserve previous triggeredToday
            for trg2 in userdata['state']['triggers']:
                if trg['days'] == trg2['days'] and trg_time == trg2['time'] and trg['isOn'] == trg['isOn']:
                    n_trigger = trg2

            n_triggers.append(n_trigger)

        userdata['state']['triggers'] = n_triggers


    fields = {
        "user_id": cmd_user_id,
        "uuid": cmd_uuid,
        "triggers": json.dumps(triggers),
    }
    tags = {
        "type": "setTriggers",
        "status": success
    }
    msg = make_msg(userdata['device_id'], MessageType.SPRINKLERS_COMMAND, fields=fields, tags=tags)
    print(cmd_user_id, triggers, success)
    userdata['client'].publish(f'sprinklers_w/{userdata["device_id"]}', msg)


def set_is_on(userdata: dict, cmd: dict, timestamp_ns = None):
    is_on = bool(cmd['value'])
    cmd_uuid = cmd['uuid']
    cmd_user_id = cmd['userId']
    success = True

    if success:
        userdata['state']['isOn'] = is_on

    fields = {
        "user_id": cmd_user_id,
        "uuid": cmd_uuid,
    }
    tags = {
        "type": "isOn",
        "is_on": is_on,
        "status": success
    }
    if timestamp_ns is None:
        msg = make_msg(userdata['device_id'], MessageType.SPRINKLERS_COMMAND, fields=fields, tags=tags)
        print(cmd_user_id, is_on, success)
        userdata['client'].publish(f'sprinklers_w/{userdata["device_id"]}', msg)
    else:
        msg = make_msg_at(userdata['device_id'], MessageType.SPRINKLERS_COMMAND, fields=fields, tags=tags, when_nanoseconds=timestamp_ns)
        userdata['client'].publish(f'sprinklers_w/{userdata["device_id"]}', msg)


def set_is_on_sim(userdata: dict, is_on: bool, timestamp = None):
    cmd = {
        "userId": -1,
        "type": "isOn",
        "value": is_on,
        "uuid": str(uuid4())
    }
    set_is_on(userdata, cmd, timestamp)


def _simulator_loop(client: mqtt.Client, userdata: dict):
    if BULK_DATA:
        _simulator_loop_BULK_DATA(client, userdata)
    else:
        _simulator_loop_regular(client, userdata)


def _simulator_loop_BULK_DATA(client: mqtt.Client, userdata: dict):
    now = datetime.datetime.now()
    ns_IN_s = 1000000000 # Nanoseconds in a microsecond
    virtual_now = now - timedelta(days=30*3)
    three_months_ago = (now - timedelta(days=30*3)).timestamp()
    timestamp = int(virtual_now.timestamp() * ns_IN_s)
    i = 0

    while True:
        command = random.choice(['on', 'off'])

        if command == 'open':
            set_is_on_sim(userdata, True, timestamp)
        else:
            set_is_on_sim(userdata, False, timestamp)

        # Next tick
        # Assuming sprinklers are activated twice a day = every 12 hours
        virtual_now = virtual_now + timedelta(seconds=12 * 3600)
        timestamp = int(virtual_now.timestamp() * ns_IN_s)
        i += 1
        if i % 6 == 0:
            print('spri\t', virtual_now.timestamp(), datetime.datetime.now().timestamp(), f'{int((virtual_now.timestamp() - three_months_ago) / (datetime.datetime.now().timestamp() - three_months_ago) * 100)}%')

            if virtual_now.timestamp() >= datetime.datetime.now().timestamp():
                return


def _simulator_loop_regular(client: mqtt.Client, userdata: dict):
    sleep_duration = 2
    day_today = datetime.datetime.now().weekday()
    
    while True:
        now = datetime.datetime.now()
        day = now.weekday()

        # Restart all triggers if day has changed since simulator started
        if day != day_today:
            day_today = day
            for trg in userdata['state']['triggers']:
                trg['triggeredToday'] = False

        time_ = datetime.time(hour=now.hour, minute=now.minute)

        for trg in userdata['state']['triggers']:
            for trg_day in trg['days']:
                if trg_day == day and trg['time'] == time_:
                    if not trg['triggeredToday'] and userdata['state']['isOn'] != trg['isOn']:
                        set_is_on_sim(userdata, trg['isOn'])
                    trg['triggeredToday'] = True

        time.sleep(sleep_duration)


'''
Main
'''

def run(device_id: int, property_id: int, is_bulk: bool):
    global BULK_DATA
    BULK_DATA = is_bulk

    state = {
        'isOn': False,
        'triggers': []
    }
    userdata = {
        'device_id': device_id,
        'property_id': property_id,
        'client': None,
        'state': state,
    }

    client = _make_and_connect_client(userdata)

    ping_thread = threading.Thread(target=lambda: _ping_loop(client, device_id, property_id), daemon=True)
    ping_thread.start()

    simulation_thread = threading.Thread(target = lambda: _simulator_loop(client, userdata), daemon=True)
    simulation_thread.start()

    try:
        client.loop_forever()
    except KeyboardInterrupt:
        client.disconnect()
