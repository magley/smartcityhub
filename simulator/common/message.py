
import dataclasses
import enum
import json
from influx_line_protocol import Metric
import time


class MessageType(str, enum.Enum):
    '''
    The enum value must match measurement name in influxdb.
    '''

    PING = 'ping',
    DHT_READ = 'dht',
    AC_COMMAND = 'ac_cmd',
    AC_READ = 'ac',
    LAMP_READ_M = 'lamp_m',
    LAMP_COMMAND = 'lamp_cmd',
    GATE_COMMAND = 'gate_cmd',
    WASH_MACHINE_COMMAND = "wash_cmd"
    SPRINKLERS_COMMAND = "sprinklers_cmd",


def _make_msg(device_id: int, type: MessageType, vals: dict = {}, tags: dict = {}) -> dict:
    return {
        "device_id": device_id,
        "type": type,
        "vals": vals,
        "tags": tags,
    }


def make_msg(device_id: int, type: MessageType, fields: dict, tags: dict) -> str:
    msg = _make_msg(device_id, type, fields, tags)

    metric = Metric(type)
    metric.with_timestamp(time.time_ns())
    metric.add_tag('device_id', device_id)

    for k, v in msg['tags'].items():
        metric.add_tag(k, v)
    for k, v in msg['vals'].items():
        metric.add_value(k, v)

    return str(metric)


def make_msg_at(device_id: int, type: MessageType, fields: dict, tags: dict, when_nanoseconds: int) -> str:
    msg = _make_msg(device_id, type, fields, tags)

    metric = Metric(type)
    metric.with_timestamp(when_nanoseconds)
    metric.add_tag('device_id', device_id)

    for k, v in msg['tags'].items():
        metric.add_tag(k, v)
    for k, v in msg['vals'].items():
        metric.add_value(k, v)

    return str(metric)