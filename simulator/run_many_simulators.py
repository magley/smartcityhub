import os
import subprocess
import threading
import time

device_id_start = 100_537 # Inclusive
device_id_end = 100_837 # Inclusive
device_type = "wash"

for i in range(device_id_start, device_id_end+1):
    property_id = 1
    device_id = i

    command = f'python main.py -p {property_id} -d {device_id} -t {device_type}'

    def f():
        os.system(f"start /wait cmd /c \"{command}\"")
    t = threading.Thread(target=f)
    t.start()

    print(i - device_id_start)