import argparse
import sim.dht.dht as dht
import sim.ac.ac as ac
import sim.lamp.lamp as lamp
import sim.gate.gate as gate
import sim.wash.washmachine as wash
import sim.sprinklers.sprinklers as sprinklers

parser = argparse.ArgumentParser()
parser.add_argument('-property', '-p', type=int, help='Property ID', required=True)
parser.add_argument('-device', '-d', type=int, help='Device ID', required=True)
parser.add_argument('-type', '-t', type=str, help='Type of device to simulate', choices=['dht', 'ac', 'wash', 'sprinklers', 'lamp', 'gate', 'solar', 'battery', 'charger'], required=True)
parser.add_argument('-bulk', type=bool, action=argparse.BooleanOptionalAction, default=False, help='Generate data in bulk for the previous 3 months?')
args = parser.parse_args()

DEVICE_ID = args.device
PROPERTY_ID = args.property
DEVICE_TYPE = args.type
is_bulk = args.bulk

if args.type == 'dht':
    dht.run(DEVICE_ID, PROPERTY_ID, is_bulk)
elif args.type == 'ac':
    ac.run(DEVICE_ID, PROPERTY_ID, is_bulk)
elif args.type == 'lamp':
    lamp.run(DEVICE_ID, PROPERTY_ID, is_bulk)
elif args.type == 'gate':
    gate.run(DEVICE_ID, PROPERTY_ID, is_bulk)
elif args.type == 'wash':
    wash.run(DEVICE_ID, PROPERTY_ID, is_bulk)
elif args.type == 'sprinklers':
    sprinklers.run(DEVICE_ID, PROPERTY_ID, is_bulk)
else:
    print(f'Simulator for device type {DEVICE_TYPE} not implemented yet.')
    exit(1)