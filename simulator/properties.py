import toml

properties: dict = {}
with open("properties.toml") as f:
    properties = toml.loads(f.read()) 