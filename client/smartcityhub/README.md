Front-end application powered by vue-js.

# How to run

1) Navigate to `/smartcityhub`
2) Install all node modules (`npm i`)
3) Run: `npm run dev`

# How to deploy

1) Navigate to `/smartcityhub`
2) Install all node modules (`npm i`)
3) Run `npm run build`
4) It will generate the static files in `D:/nvt-data/dist` (see vite.config.ts).
5) You can run the deployed files with `npm run preview` or use nginx (`localhost:8300`).

### Development notes

- `src/assets/*.png` had to be moved to `public/*.png`. It's a magic folder (not really).
- `assets` is used for when you import the files directly in your source code. Href-s go in `public`.
- using `new Type(...)` in a `<template>` for custom defined types does not work in deployed mode. You get an error: `Class constructor PropertyDTOProp cannot be invoked without 'new'`. Solution: convert them a priori in `<script>`. 

# If using VS Code

Install the following extensions:

* TypeScript Vue Plugin (Volar)
* Vue Language Features (Volar)

You may also have to disable the native typescript/javascript extension. See [here](https://vuejs.org/guide/typescript/overview.html#volar-takeover-mode).