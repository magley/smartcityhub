import { AuthService } from "@/api/auth.api";
import axios, { type InternalAxiosRequestConfig } from "axios";

// Shared axios instance for communicating with the server program.
export const axiosInstance = axios.create({
    baseURL: "http://localhost:8300/api"
});

export const axiosPhoto = axios.create({
    baseURL: "http://localhost:8300/photo"
})

axiosInstance.interceptors.request.use(
    (value: InternalAxiosRequestConfig<any>) => {
        value.headers.Authorization = `Bearer ${AuthService.getJWTString()}`;
        return value;
    }
);

axiosPhoto.interceptors.request.use(
    (value: InternalAxiosRequestConfig<any>) => {
        value.headers.Authorization = `Bearer ${AuthService.getJWTString()}`;
        return value;
    }
);

export function getAvatar(userEmail: string): string {
    return `http://localhost:8300/avatar/${userEmail}.jpg`
}