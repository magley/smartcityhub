import { AuthService } from "@/api/auth.api";

/*
    This is a wrapper around plain WebSockets, sort of emulating Stomp's API
    by introducing the concept of subscription.

    Because the websocket server uses a specific protocol for messaging, this
    "library" is very specialized.

    All it does is it maps `SockNVTSub`, which is embedded in every message sent
    by the WebSocket, to a callback function. 

    In Typescript, Map<K, V> doesn't work with non-primitive keys, because it
    ALWAYS compares them by reference. So the internal map has to convert 
    `SockNVTSub` into a string (JSON.stringify is dangerous because of order).

    Usage:

    let ws = SockNVT("ws://...");
    ws.onConnect = () => {
        ws.Subscribe(userId, topic, id1, id2, (res) => {
            // do stuff with res.
        });
    }
    ws.connect();

    There is no error handling.
*/

interface SockNVTSub {
    user: number,
    type: string,
    id1: number,
    id2: number
};

function SockNVTSub_hash(sub: SockNVTSub): string {
    return `${sub.user}${sub.type}${sub.id1}${sub.id2}`;
}

interface SockNVTResponseFlat {
    user: number,
    type: string,
    id1: number,
    id2: number,
    payload: any
};

export interface SockNVTResponse {
    sub: SockNVTSub
    payload: any
};

export interface SockNVTSubCallback {
    (ev: SockNVTResponse): void
};

function isSockNVTResponse(object: any): object is SockNVTResponseFlat {
    const keys = Object.keys(object);
    return keys.includes('user') 
        && keys.includes('type') 
        && keys.includes('id1') 
        && keys.includes('id2')
        && keys.includes('payload');
};

export class SockNVT {
    url: string
    ws!: WebSocket;
    subs: Map<string, SockNVTSubCallback | undefined>;

    constructor(url: string) {
        this.url = url;
        this.subs = new Map<string, SockNVTSubCallback | undefined>();
    }

    onConnect(): void {
    }

    onClose(): void {
    }

    connect() {
        this.ws = new WebSocket(this.url);
        this.ws.onopen = () => { this.onConnect() };
        this.ws.onmessage = (ev) => { this.onGetMessage(ev) };
        this.ws.onclose = () => { this.onClose() };     
    }

    close(): void {
        this.ws.close();
    }

    subscribe(userId: number, type: string, id1: number, id2: number, callback: SockNVTSubCallback) {
        const subBody: SockNVTSub = {
            user: +userId,
            type: type,
            id1: +id1,
            id2: +id2
        };

        this.ws.send(JSON.stringify(subBody));
        this.subs.set(SockNVTSub_hash(subBody), callback);  
    }

    private onGetMessage(ev: MessageEvent<any>) {
        const json = JSON.parse(ev.data);
        
        if (!isSockNVTResponse(json)) {
            console.error("Websocket message does not adhere to `SockNVTResponse` schema:");
            console.error(json);
            return;
        }

        const msg_ = json as SockNVTResponseFlat;
        const msg: SockNVTResponse = {
            sub: {
                id1: msg_.id1,
                id2: msg_.id2,
                type: msg_.type,
                user: msg_.user
            },
            payload: msg_.payload
        }

        const callback = this.subs.get(SockNVTSub_hash(msg.sub));
        if (callback == undefined) {
            return;
        }
        callback(msg);
    }
}