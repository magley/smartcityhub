import { AuthService } from '@/api/auth.api';

import { userStore } from '@/store/userStore';
import { useToast } from 'primevue/usetoast';
import { createRouter, createWebHistory } from 'vue-router';

//==================------------------- Shared -----------------================
import HomeView from '@/views/shared/HomeView.vue';
import NotFoundView from '@/views/shared/NotFoundView.vue';
import ChangePasswordView from '@/views/shared/ChangePasswordView.vue';
//==================------------------- Guest ------------------================
import LoginView from '@/views/guest/LoginView.vue';
import RegisterView from '@/views/guest/RegisterView.vue';
import VerifyAccountView from '@/views/guest/VerifyAccountView.vue';
//==================------------------- User -------------------================
import PropertyDetailsView from '@/views/user/PropertyDetailsView.vue';
import AddDeviceView from '@/views/user/AddDeviceView.vue';
import AddPropertyView from '@/views/user/AddPropertyView.vue';
import DeviceView from '@/views/user/DeviceView.vue';
//==================------------------- Admin ------------------================
import PropertyRequestView from '@/views/admin/PropertyRequestView.vue';
//==================----------------- Superadmin ---------------================
import AdminListView from '@/views/superadmin/AdminListView.vue';
//==================-----------------............---------------================

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'Home',
            component: HomeView
        },
        {
            path: "/login",
            name: "Login",
            component: LoginView
        },
        {
            path: "/logout",
            name: "Logout",
            component: LoginView // stub
        },
        {
            path: "/register",
            name: "Register",
            component: RegisterView
        },
        {
            path: "/verify",
            name: "Verify",
            component: VerifyAccountView
        },
        {
            path: "/passwordchange",
            name: "Change Password",
            component: ChangePasswordView
        },
        {
            path: "/:catchAll(.*)",
            name: "404",
            component: NotFoundView
        },
        {
            /**
             * Inside a regular Vue component, if you wanted to redirect the user
             * the 404 page based on a backend response, you would go:
             *      router.push("/404"); // Assuming no route matches /404.
             * But when using next(), like in router.beforeEach(), you route by
             * name. Mapping by name to '404' would try go to "/:catchAll(.*)", 
             * which is just "/" and would effectively do nothing. 
             * That's why "Not Found" exists and routes to '/404'.
             */
            path: "/404",
            name: "Not Found",
            component: NotFoundView
        },

        // Client

        {
            path: "/property/:propertyId",
            name: "Property Page",
            component: PropertyDetailsView
        },
        {
            path: "/property/request",
            name: "Request Property",
            component: AddPropertyView
        },
        {
            path: "/device/add/:type/:propertyId",
            name: "Add Device",
            component: AddDeviceView
        },
        {
            path: "/device/:id",
            name: "Device",
            component: DeviceView
        },

        // Admin
        {
            path: "/property-approve/:propertyId",
            name: "Approve Property",
            component: PropertyRequestView,
        },

        // Superadmin

        {
            path: "/superadmin/admins",
            name: "Admins",
            component: AdminListView
        },
    ]
});

router.beforeEach((to, from, next) => {
    const unauthNames = ['Login', 'Register', 'Verify'];
    const clientNames = ['Property Page', 'Add Device'];
    const adminNames = ['Admins'];

    // Ugly. Better solution: hardcode the role inside route so every page
    // that requires the client would be /client/etc. and then you just compare
    // the route "prefix" with the JWT's role.
    const isProperRoleForName = (name: string): boolean => {
        if (AuthService.isLoggedIn() == false)
            return false;
        if (AuthService.getRole() == 'CLIENT' && adminNames.find(n => n == name) != undefined) {
            return false;
        }
        if (AuthService.getRole() == 'ADMIN' && clientNames.find(n => n == name) != undefined) {
            return false;
        }
        if (AuthService.getRole() == 'SUPER_ADMIN' && clientNames.find(n => n == name) != undefined) {
            return false;
        }
        return true;
    }

    if (!AuthService.isLoggedIn()) {
        if (unauthNames.find(n => n == to.name) == undefined && to.name != "404") {
            next({
                name: "Login",
                query: { 'redirect': 1 },
                force: true
            });
        } else {
            next();
        }
    } else {
        if (to.name == 'Logout') {
            AuthService.removeJWT();
            next({
                name: "Login",
                force: true
            });
            userStore.refresh();
        }
        else if (to.name == 'Login') {
            // On successful login, redirect to home page based on user role.
            // If logged-in user tries to open the login page, he gets redirected.
            const role = AuthService.getRole();
            const roleMap: { [key: string]: string } = {
                'SUPER_ADMIN': 'Home',
                'ADMIN': 'Home',
                'CLIENT': 'Home',
            };

            next({
                name: roleMap[role]
            });
        }
        else {
            if (isProperRoleForName(to.name as string)) {
                next();
            } else {
                next({ name: "Not Found" });
            }
        }
    }
});


router.afterEach((to, from) => {
    const toast = useToast();

    toast.removeGroup('bc');
    if (AuthService.shouldNagForPasswordChange() && to.name != "Change Password") {
        toast.add({
            severity: 'warn',
            summary: 'Update your password',
            detail: 'Please change your password to help protect your system.',
            group: 'bc',
        });
    }
});

export default router