import type { DeviceCommandHistoryMetaDTO, InfluxPaginator } from "../device.api"
import { axiosInstance } from "@/util/http";
import type { AxiosResponse } from "axios";
import type { DeviceCmdHistory_Base, DeviceCmdHistory_Request } from "./device-common.api";

export interface SprinklersStartDTO {
    id: number,
    uuid: string,
    start: boolean
}

export interface SprinklersTrigger {
    hour: number,
    minute: number,
    days: number[],
    isOn: boolean,
}

export interface SprinklersTriggersDTO {
    id: number,
    uuid: string,
    triggers: SprinklersTrigger[],
}

export enum SprinklersCommandType {
    isOn = "isOn",
    setTriggers = "setTriggers",
}

export interface SprinklersCmdHistoryItemDTO extends DeviceCmdHistory_Base {
    type: SprinklersCommandType,
    oneOf: {
        is_on: boolean | undefined,
        triggers: string | undefined,
    }
}

export interface SprinklersCmdHistoryDTO {
    totalItems: number,
    pagination: InfluxPaginator,
    list: SprinklersCmdHistoryItemDTO[],
}


export class Sprinklers_Util {
    static toHumanText(cmd: SprinklersCmdHistoryItemDTO): string {
        switch (cmd.type) {
            case SprinklersCommandType.isOn: return `Turned sprinklers ${cmd.oneOf.is_on ? "on" : "off"}`;
            case SprinklersCommandType.setTriggers: return `New triggers ${cmd.oneOf.triggers}`;
        }
    }
}

export class Sprinklers_Service {
    static async setOnOff(dto: SprinklersStartDTO): Promise<AxiosResponse<void>> {
        return await axiosInstance.put(`/device/sprinklers/start`, dto);
    }

    static async setTriggers(dto: SprinklersTriggersDTO): Promise<AxiosResponse<void>> {
        return await axiosInstance.put(`/device/sprinklers/triggers`, dto);
    }

    static async getHistory(dto: DeviceCmdHistory_Request): Promise<AxiosResponse<SprinklersCmdHistoryDTO>> {
        return await axiosInstance.post(`/device/sprinklers/command-history`, dto);
    }
}