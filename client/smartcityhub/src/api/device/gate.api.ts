
import type { DeviceCommandHistoryMetaDTO, DeviceCommandUserMeta, DeviceDTO, DeviceRegisterDTO, InfluxPaginator } from "../device.api"
import { axiosInstance } from "@/util/http";
import type { AxiosResponse } from "axios";
import type { DeviceCmdHistory_Base, DeviceCmdHistory_Request } from "./device-common.api";

export interface GateRegisterDTO {
    base: DeviceRegisterDTO,
    plates: Array<string>,
}

export interface GateDTO {
    base: DeviceDTO,
    plates: Array<string>,
}

export interface GateStartDTO {
    id: number,
    uuid: string,
    open: GateOpen,
}


export interface GateModeDTO {
    id: number,
    uuid: string,
    mode: GateMode,
}

export interface GateOverrideDTO {
    id: number,
    uuid: string,
    override: GateOverride,
}

export enum GateOpen {
    OPEN = 'open', OPENING = 'opening', CLOSING = 'closing', CLOSED = 'closed'
}

export enum GateMode {
    PRIVATE = "private", PUBLIC = "public"
}

export enum GateOverride {
    NO = "no", YES = "yes"
}

export enum GateDirection {
    ENTRY = "entry", EXIT = "exit"
}

export enum GateSensorEvent {
    ARRIVED = "arrived", LEFT = "left", PASSED = "passed"
}

export enum GateCommandType {
    open_state = "open_state",
    mode = "mode",
    override = "override",
    sensor_exit = "sensor_exit",
    sensor_entry = "sensor_entry",
}

export interface GateCmdHistoryItemDTO extends DeviceCmdHistory_Base {
    type: GateCommandType,
    oneOf: {   
        openState: GateOpen | undefined,
        mode: GateMode | undefined,
        override: GateOverride | undefined,
        plate: string | undefined,
        direction: GateDirection | undefined,
        event: GateSensorEvent | undefined,
    }
}

export interface GateCmdHistoryDTO {
    totalItems: number,
    pagination: InfluxPaginator,
    list: GateCmdHistoryItemDTO[],
}


export class Gate_Util {
    static toHumanText_Override (mode: GateOverride): string {
        switch (mode) {
            case GateOverride.YES: return "Override"
            case GateOverride.NO: return "Automatic"
        }
    }
        
    static toHumanText_OverrideCmd(mode: GateOverride): string {
        switch (mode) {
            case GateOverride.YES: return "Enabled override"
            case GateOverride.NO: return "Disabled override"
        }
    }
    
    static toHumanText_DirectionCmd (dir: GateDirection): string {
        switch (dir) {
            case GateDirection.ENTRY: return "entrance"
            case GateDirection.EXIT: return "exit"
        }
    }
    
    static toHumanText(cmd: GateCmdHistoryItemDTO): string {
        switch (cmd.type) {
            case GateCommandType.open_state: return `Gate is ${cmd.oneOf.openState}`
            case GateCommandType.mode: return `Set mode to ${cmd.oneOf.mode}`
            case GateCommandType.override: return `${Gate_Util.toHumanText_OverrideCmd(cmd.oneOf.override!)}`
            case GateCommandType.sensor_exit: return `${cmd.oneOf.plate} ${cmd.oneOf.event} ${Gate_Util.toHumanText_DirectionCmd(cmd.oneOf.direction!)}`
            case GateCommandType.sensor_entry: return `${cmd.oneOf.plate} ${cmd.oneOf.event} ${Gate_Util.toHumanText_DirectionCmd(cmd.oneOf.direction!)}`
        }
    }  
}

export class Gate_Service { 
    static async register(dto: GateRegisterDTO): Promise<AxiosResponse<void>> {
        return await axiosInstance.post(`/device/gate`, dto);
    }

    static async get(id: number): Promise<AxiosResponse<GateDTO>> {
        return await axiosInstance.get(`/device/gate/${id}`);
    }

    static async setOnOff(dto: GateStartDTO): Promise<AxiosResponse<void>> {
        return await axiosInstance.put(`/device/gate/start`, dto);
    }

    static async setMode(dto: GateModeDTO): Promise<AxiosResponse<void>> {
        return await axiosInstance.put(`/device/gate/mode`, dto);
    }

    static async setOverride(dto: GateOverrideDTO): Promise<AxiosResponse<void>> {
        return await axiosInstance.put(`/device/gate/override`, dto);
    }
    
    static async getHistory(dto: DeviceCmdHistory_Request): Promise<AxiosResponse<GateCmdHistoryDTO>> {
        return await axiosInstance.post(`/device/gate/command-history`, dto);
    }
}