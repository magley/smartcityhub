import type { InfluxPaginator } from "../device.api";

export interface DeviceCmdHistory_Base {
    timestamp: Date,
    user: {
        id: number,
        name: string,
        lastName: string,
    }
    status: boolean,
    uuid: string,
}

export interface DeviceCmdHistory_Request {
    id: number,
    users: Array<string>,
    pagination: InfluxPaginator,
    minDate: Date,
    maxDate: Date,
}