import type { DeviceCommandHistoryMetaDTO, DeviceCommandUserMeta, DeviceDTO, DeviceRegisterDTO, InfluxPaginator } from "../device.api"
import { axiosInstance } from "@/util/http";
import type { AxiosResponse } from "axios";
import type { DeviceCmdHistory_Base, DeviceCmdHistory_Request } from "./device-common.api";

export enum ACMode {
    heat = "heat",
    cool = "cool",
    auto = "auto",
    fan = "fan",
}

export enum ACCommandType {
    temperature = "temperature",
    is_on = "is_on",
    mode = "mode",
    config = "config"
}

export interface ACDTO {
    base: DeviceDTO,
    minTemperature: number,
    maxTemperature: number, 
    supportedModes: Array<ACMode>,
}

export interface ACRegisterDTO {
    base: DeviceRegisterDTO,
    supportedModes: Array<ACMode>,
    minTemperature: number,
    maxTemperature: number,
}

export interface ACStartDTO {
    id: number,
    uuid: string,
    start: boolean
}

export interface ACModeDTO {
    id: number,
    uuid: string,
    mode: ACMode
}

export interface ACConfigItemDTO  {
    startHour: number,
    endHour: number,
    mode: ACMode,
    temperature: number
}

export interface ACTemperatureDTO {
    id: number,
    uuid: string,
    temperature: number
}

export interface ACConfigDTO {
    id: number,
    customMode: Array<ACConfigItemDTO>,
    uuid: string
}

export interface ACCmdHistoryItemDTO extends DeviceCmdHistory_Base {
    type: ACCommandType,
    oneOf: {
        isOn: boolean | undefined,
        mode: ACMode | undefined,
        temperature: number | undefined,
        config: string | undefined,
    }
}

export interface ACCmdHistoryDTO {
    totalItems: number,
    pagination: InfluxPaginator,
    list: ACCmdHistoryItemDTO[],
}

export class AC_Util {
    static toHumanText(cmd: ACCmdHistoryItemDTO): string {
        switch (cmd.type) {
            case ACCommandType.is_on: return `has turned the AC ${cmd.oneOf.isOn ? "on" : "off"}`;
            case ACCommandType.config: return `has set a custom regime`;
            case ACCommandType.temperature: return `has set the temperature to ${cmd.oneOf.temperature}°C`;
            case ACCommandType.mode: return `has set the mode to ${cmd.oneOf.mode}`;
        }
        return "?";
    }
}

export class AC_Service {
    static async register(dto: ACRegisterDTO): Promise<AxiosResponse<void>> {
        return await axiosInstance.post(`/device/ac`, dto);
    }

    static async get(id: number): Promise<AxiosResponse<ACDTO>> {
        return await axiosInstance.get(`/device/ac/${id}`);
    }
    
    static async setStart(dto: ACStartDTO): Promise<AxiosResponse<void>> {
        return await axiosInstance.put(`/device/ac/start`, dto);
    }

    static async setTemperature(dto: ACTemperatureDTO): Promise<AxiosResponse<void>> {
        return await axiosInstance.put(`/device/ac/temperature`, dto);
    }

    static async setMode(dto: ACModeDTO): Promise<AxiosResponse<void>> {
        return await axiosInstance.put(`/device/ac/mode`, dto);
    }

    static async setConfig(dto: ACConfigDTO): Promise<AxiosResponse<void>> {
        return await axiosInstance.put(`/device/ac/config`, dto);      
    }

    static async getHistory(dto: DeviceCmdHistory_Request): Promise<AxiosResponse<ACCmdHistoryDTO>> {
        return await axiosInstance.post(`/device/ac/history`, dto);
    }
}