import type { DeviceCommandHistoryMetaDTO, DeviceCommandUserMeta, DeviceDTO, DeviceRegisterDTO, InfluxPaginator } from "../device.api"
import { axiosInstance } from "@/util/http";
import type { AxiosResponse } from "axios";
import type { DeviceCmdHistory_Base, DeviceCmdHistory_Request } from "./device-common.api";

export enum WashMachineMode {
    off = "off",
    cotton_95 = "cotton_95",
    cotton_60 = "cotton_60",
    cotton_40 = "cotton_40",
    synthetic_60 = "synthetic_60",
    synthetic_40 = "synthetic_40",
    wool_40 = "wool_40",
}

export enum WashMachineCommandType {
    mode = "mode",
    schedule = "schedule",
    removeSchedule = "removeSchedule"
}

export interface WashMachineDTO {
    base: DeviceDTO,
    supportedModes: Array<WashMachineMode>,
}

export interface WashMachineRegisterDTO {
    base: DeviceRegisterDTO,
    supportedModes: Array<WashMachineMode>,
}

export interface WashMachineCmdModeDTO {
    id: number,
    uuid: string,
    mode: WashMachineMode,
}

export interface WashMachineCmdScheduleDTO {
    id: number,
    uuid: string,
    mode: WashMachineMode,
    time: Date,
}

export interface WashMachineOneOf_ScheduledDTO {
    timestamp: number,
    mode: WashMachineMode
}

export interface WashMachineCmdHistoryItemDTO extends DeviceCmdHistory_Base {
    type: WashMachineCommandType,
    oneOf: {
        mode: WashMachineMode | undefined,
        scheduled: WashMachineOneOf_ScheduledDTO | undefined
    }
}

export interface WashMachineCmdHistoryDTO {
    totalItems: number,
    pagination: InfluxPaginator,
    list: WashMachineCmdHistoryItemDTO[],
}

export class WashMachine_Util {
    static toHumanText(cmd: WashMachineCmdHistoryItemDTO): string {
        switch (cmd.type) {
            case WashMachineCommandType.mode: {
                if (cmd.oneOf.mode == WashMachineMode.off)
                    return "has turned the washing machine off";
                else 
                    return `has started the washing of '${WashMachine_Util.toHumanTextMode(cmd.oneOf.mode!)}'`;
            }
            case WashMachineCommandType.schedule: return `has scheduled '${WashMachine_Util.toHumanTextMode(cmd.oneOf.scheduled!.mode)}' for ${new Date(cmd.oneOf.scheduled!.timestamp * 1000).toLocaleString()}`;
            case WashMachineCommandType.removeSchedule : return `has removed the scheduling of '${WashMachine_Util.toHumanTextMode(cmd.oneOf.scheduled!.mode)}' for ${new Date(cmd.oneOf.scheduled!.timestamp * 1000).toLocaleString()}`;
        }
        return "?";
    }

    static isWool(mode: WashMachineMode): boolean {
        return mode == WashMachineMode.wool_40;
    }

    static isCotton(mode: WashMachineMode): boolean {
        return mode == WashMachineMode.cotton_40 || mode == WashMachineMode.cotton_60 || mode == WashMachineMode.cotton_95;
    }

    static isSynthetic(mode: WashMachineMode): boolean {
        return mode == WashMachineMode.synthetic_40 || mode == WashMachineMode.synthetic_60;
    }

    static toHumanTextMode(mode: WashMachineMode): string {
        if (mode == WashMachineMode.off) {
            return "Off";
        }

        let parts = WashMachineMode[mode].split("_");
        function capitalizeFirstLetter(s: string) {
            return s.charAt(0).toUpperCase() + s.slice(1);
        }

        parts[0] = capitalizeFirstLetter(parts[0]);
        parts[1] = `${parts[1]}°C`;

        return `${parts[0]} ${parts[1]}`;
    }
}

export class WashMachine_Service {
    static async register(dto: WashMachineRegisterDTO): Promise<AxiosResponse<void>> {
        return await axiosInstance.post(`/device/wash`, dto);
    }

    static async get(id: number): Promise<AxiosResponse<WashMachineDTO>> {
        return await axiosInstance.get(`/device/wash/${id}`);
    }
    
    static async setMode(dto: WashMachineCmdModeDTO): Promise<AxiosResponse<void>> {
        return await axiosInstance.put(`/device/wash/mode`, dto);
    }

    static async schedule(dto: WashMachineCmdScheduleDTO): Promise<AxiosResponse<void>> {
        return await axiosInstance.put(`/device/wash/schedule`, dto);
    }

    static async removeSchedule(dto: WashMachineCmdScheduleDTO): Promise<AxiosResponse<void>> {
        return await axiosInstance.put(`/device/wash/remove-schedule`, dto);
    }

    static async getHistory(dto: DeviceCmdHistory_Request): Promise<AxiosResponse<WashMachineCmdHistoryDTO>> {
        return await axiosInstance.post(`/device/wash/history`, dto);
    }

    static async getRelevantScheduledTasks(id: number, daysBack: number): Promise<AxiosResponse<Array<WashMachineCmdScheduleDTO>>> {
        return await axiosInstance.get(`/device/wash/scheduled/${id}/${daysBack}`);
    }
}