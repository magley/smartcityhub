import type { DeviceCommandHistoryMetaDTO, DeviceCommandUserMeta, DeviceDTO, DeviceHistoricalDTO, DeviceRegisterDTO, InfluxPaginator } from "../device.api"
import { axiosInstance } from "@/util/http";
import type { AxiosResponse } from "axios";
import type { DeviceCmdHistory_Base, DeviceCmdHistory_Request } from "./device-common.api";

export interface LampReadingDTO {
    timestamp: Date,
    illuminance: number,
}

export interface LampStartDTO {
    id: number,
    uuid: string,
    start: boolean
}

export interface LampModeDTO {
    id: number,
    uuid: string,
    mode: LampMode,
    threshold: number,
}

export enum LampMode {
    MANUAL = "Manual", AUTO = "Auto",
}

export enum LampCommandType {
    is_on = "is_on",
    mode = "mode",
}

export interface LampCmdHistoryItemDTO extends DeviceCmdHistory_Base {
    type: LampCommandType,
    oneOf: {
        isOn: boolean | undefined,
        mode: LampMode | undefined,
        threshold: number | undefined,
    }
}

export interface LampCmdHistoryDTO {
    totalItems: number,
    pagination: InfluxPaginator,
    list: LampCmdHistoryItemDTO[],
}


export class Lamp_Util {
    static toHumanText(cmd: LampCmdHistoryItemDTO): string {
        switch (cmd.type) {
            case LampCommandType.is_on: return `Turned lamp ${cmd.oneOf.isOn ? "on" : "off"}`;
            case LampCommandType.mode: {
                switch (cmd.oneOf.mode!) {
                    case LampMode.AUTO: return `Set mode to ${cmd.oneOf.mode} with threshold ${cmd.oneOf.threshold}`;
                    case LampMode.MANUAL: return `Set mode to ${cmd.oneOf.mode}`;
                }
            }
        }
    }
}

export class Lamp_Service {
    static async setOnOff(dto: LampStartDTO): Promise<AxiosResponse<void>> {
        return await axiosInstance.put(`/device/lamp/start`, dto);
    }

    static async setMode(dto: LampModeDTO): Promise<AxiosResponse<void>> {
        return await axiosInstance.put(`/device/lamp/mode`, dto);
    }

    static async getHistory(dto: DeviceCmdHistory_Request): Promise<AxiosResponse<LampCmdHistoryDTO>> {
        return await axiosInstance.post(`/device/lamp/command-history`, dto);
    }

    static async getMeasurements(dto: DeviceHistoricalDTO): Promise<AxiosResponse<Array<LampReadingDTO>>> {
        return await axiosInstance.put(`/device/lamp/measurement-history`, dto);
    }
}