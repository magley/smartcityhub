import { axiosInstance } from "@/util/http";
import type { AxiosResponse } from "axios";

export interface LoginDTO {
    username: string,
    password: string
}

export class SessionService {
    static async Login(credentials: LoginDTO): Promise<AxiosResponse<string>> {
        return await axiosInstance.post(`/session/login`, credentials);
    }
}