import { axiosInstance } from "@/util/http";
import { type AxiosResponse } from "axios";
import type { CollectionResultDTO } from "./common.api";

export interface CreateUserDTO {
    email: string,
    password: string,
    name: string,
    lastName: string, 
}

export interface UserDTO {
    email: string,
    password: string,
    name: string,
    lastName: string,
    roles: Array<string>
};

export interface UserInfoDTO {
    id: number,
    email: string,
    name: string,
    lastName: string,
    role: string
}

export interface ClientRegisterDTO {
    email: string,
    password: string,
    name: string,
    lastName: string,
    avatarB64: string,
}

export interface ChangePasswordDTO {
    oldPassword: string,
    newPassword: string
}

export class UserService {
    static async Get(id: number): Promise<AxiosResponse<UserDTO>> {
        return await axiosInstance.get(`/user/${id}`);
    }

    static async GetAll(): Promise<AxiosResponse<Array<UserInfoDTO>>> {
        return await axiosInstance.get(`/user/all`);
    }

    static async getAllClients(): Promise<AxiosResponse<Array<UserInfoDTO>>> {
        return await axiosInstance.get(`/user/all/clients`);
    }

    static async searchClients(query: string): Promise<AxiosResponse<Array<UserInfoDTO>>> {
        return await axiosInstance.get(`/user/all/clients/search?search=${query}`);
    }

    static async GetAllAdmins(): Promise<AxiosResponse<Array<UserInfoDTO>>> {
        return await axiosInstance.get(`/user/all/admins`);
    }

    static async getAllAdmins(page: number, count: number, sortColumn: string | undefined, sortOrder: number | undefined): Promise<AxiosResponse<CollectionResultDTO<UserInfoDTO>>> {
        if (sortColumn && sortOrder) {
            return await axiosInstance.get(`/user/all/admins/p?page=${page}&size=${count}&sort=${sortColumn},${sortOrder > 0 ? 'ASC' : 'DESC'}`);
        } else {
            return await axiosInstance.get(`/user/all/admins/p?page=${page}&size=${count}`);
        }    
    }

    static async Remove(id: number): Promise<AxiosResponse<void>> {
        return await axiosInstance.delete(`/user/${id}`);
    }

    static async RegisterClient(user: ClientRegisterDTO): Promise<AxiosResponse<void>> {
        return await axiosInstance.post(`/user/client`, user);
    }

    static async RegisterAdmin(user: CreateUserDTO): Promise<AxiosResponse<void>> {
        return await axiosInstance.post(`/user/admin`, user);
    }

    static async ChangePassword(dto: ChangePasswordDTO): Promise<AxiosResponse<void>> {
        return await axiosInstance.post(`/user/changepass`, dto);
    }
};