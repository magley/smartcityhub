import { axiosInstance } from "@/util/http";
import type { AxiosResponse } from "axios";

export interface CountryWithCitiesDTO {
    id: number,
    name: string,
    cities: CityDTO[],
}

export interface CityDTO {
    id: number,
    name: string,
}

export class CountryService {
    static async GetAll(): Promise<AxiosResponse<CountryWithCitiesDTO[]>> {
        return await axiosInstance.get(`/country/all`);
    }
}