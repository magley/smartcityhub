import { axiosInstance } from "@/util/http";
import type { AxiosResponse } from "axios";
import type { CollectionResultDTO } from "./common.api";

export interface NewPermissionDTO {
    userId: number,
    entityId: number,
    entityType: PermissionType
}


export interface RevokePermissionDTO {
    userId: number,
    entityId: number,
    entityType: PermissionType
}


export interface PermissionDTO {
    entityId: number,
    userId: number,
    userName: string,
    userLastName: string,
    userEmail: string,
}

export enum PermissionType {
    property = "property",
    device = "device"
}

export interface ShareDTO {
    isShared: boolean,
    sharedByName: string,
    sharedByLastName: string,
}

export interface PropertyPartialPermissionDTO {
    partialPermission: boolean,
}

export class Permission_Service {
    static async getPropertyPremissions(id: number, page: number, count: number): Promise<AxiosResponse<CollectionResultDTO<PermissionDTO>>> {
        return await axiosInstance.get(`/permission/property/${id}/p?page=${page}&size=${count}`);
    }

    static async getDevicePremissions(id: number, page: number, count: number): Promise<AxiosResponse<CollectionResultDTO<PermissionDTO>>> {
        return await axiosInstance.get(`/permission/device/${id}/p?page=${page}&size=${count}`);
    }

    static async newPropertyPermission(dto: NewPermissionDTO): Promise<AxiosResponse<void>> {
        return await axiosInstance.post(`/permission/property`, dto);
    }

    static async newDevicePermission(dto: NewPermissionDTO): Promise<AxiosResponse<void>> {
        return await axiosInstance.post(`/permission/device`, dto);
    }

    static async revokePropertyPermission(dto: RevokePermissionDTO): Promise<AxiosResponse<void>> {
        return await axiosInstance.put(`/permission/property/revoke`, dto);
    }

    static async revokeDevicePermission(dto: RevokePermissionDTO): Promise<AxiosResponse<void>> {
        return await axiosInstance.put(`/permission/device/revoke`, dto);
    }

    static async getIfPropertyPartialPermission(id: number): Promise<AxiosResponse<PropertyPartialPermissionDTO>> {
        return await axiosInstance.get(`/permission/property/partial/${id}`);
    }
}