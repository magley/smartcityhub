export interface ErrorDTO {
    code: string,
    message: string
}

export interface CollectionResultDTO<T> {
    totalCount: number,
    items: Array<T>
}