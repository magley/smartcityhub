import { axiosInstance } from "@/util/http";
import type { AxiosResponse } from "axios";

export class ValidateService {
    static async Validate(token: string): Promise<AxiosResponse<void>> {
        return await axiosInstance.get(`/verification/${token}`);
    }
}