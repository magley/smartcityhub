import type { DeviceDTO } from '@/api/device.api';
import type { UserDTO } from '@/api/user.api';
import { axiosInstance, axiosPhoto } from "@/util/http";
import type { AxiosResponse } from "axios";
import type { ShareDTO } from './permission.api';
import type { CollectionResultDTO } from './common.api';


export interface PropertyDTO {
    id: number,
    type: PropertyType,
    address: string,
    cityName: string,
    countryName: string,
    size: number,
    floors: number,
    location: GeoCoordinate,
    status: Status,
    share: ShareDTO,
}

// NOTE: This is done because Vue's component props don't accept interfaces as type annotations
export class PropertyDTOProp implements PropertyDTO {
    id: number;
    type: PropertyType;
    address: string;
    cityName: string;
    countryName: string;
    size: number;
    floors: number;
    location: GeoCoordinate;
    status: Status;
    share: ShareDTO;

    //id: number, type: PropertyType, address: string, cityName: string, countryName: string, size: number, floors: number, location: GeoCoordinate, status: Status
    constructor(i: PropertyDTO) {
        this.id = i.id;
        this.type = i.type;
        this.address = i.address;
        this.cityName = i.cityName;
        this.countryName = i.countryName;
        this.size = i.size;
        this.floors = i.floors;
        this.location = i.location;
        this.status = i.status;
        this.share = i.share;
    }
}

export class PropertyDTOWithOwnerProp implements PropertyWithOwnerDTO {
    property: PropertyDTO;
    owner: UserDTO;

    constructor(property: PropertyDTO, owner: UserDTO) {
        this.property = property;
        this.owner = owner;
    }
}

export interface PropertyWithOwnerDTO {
    property: PropertyDTO,
    owner: UserDTO,
}

export enum State {
    PENDING = "PENDING", APPROVED = "APPROVED", DENIED = "DENIED"
}

export interface Status {
    state: State,
    denyReason?: string,
}

export interface PropertyWithDevicesDTO {
    property: PropertyDTO,
    devices: Array<DeviceDTO>
}

export interface GeoCoordinate {
    latitude: number,
    longitude: number,
}

export enum PropertyType {
    House = "House", Apartment = "Apartment"
}

export interface CreatePropertyRequestDTO {
    type: PropertyType,
    address: string,
    cityId: number,
    size: number,
    floors: number,
    location: GeoCoordinate,
    imageB64: string,
}

export class PropertyService {
    // static async GetPropertiesOwnedByUser(userId: number): Promise<AxiosResponse<Array<PropertyWithOwnerDTO>>> {
    //     return await axiosInstance.get(`/property/user/${userId}`);
    // }

    static async getPropertiesIHaveAccessTo(): Promise<AxiosResponse<Array<PropertyDTO>>> {
        return await axiosInstance.get(`/property/user/own`);
    }

    static async GetPropertyWithDevices(propertyId: number, page: number, pageSize: number): Promise<AxiosResponse<PropertyWithDevicesDTO>> {
        return await axiosInstance.get(`/property/${propertyId}/devices?page=${page}&size=${pageSize}`);
    }

    static async CreatePropertyRequest(request: CreatePropertyRequestDTO): Promise<AxiosResponse<void>> {
        return await axiosInstance.post(`/property/request`, request);
    }

    static async Get(id: number): Promise<AxiosResponse<PropertyDTO>> {
        return await axiosInstance.get(`/property/${id}/basic`);
    }

    static async GetAllPending(): Promise<AxiosResponse<PropertyWithOwnerDTO[]>> {
        return await axiosInstance.get(`/property/pending`);
    }

    static async getPendingPaginated(page: number, count: number, sortColumn: string | undefined, sortOrder: number | undefined): Promise<AxiosResponse<CollectionResultDTO<PropertyWithOwnerDTO>>> {
        if (sortColumn && sortOrder) {
            console.log(page, count, sortColumn, sortOrder);
            return await axiosInstance.get(`/property/pending/p?page=${page}&size=${count}&sort=${sortColumn},${sortOrder > 0 ? 'ASC' : 'DESC'}`);
        } else {
            return await axiosInstance.get(`/property/pending/p?page=${page}&size=${count}`);
        }    
    }

    static async GetPending(id: number): Promise<AxiosResponse<PropertyWithOwnerDTO>> {
        return await axiosInstance.get(`/property/pending/${id}`);
    }

    static async Approve(id: number): Promise<AxiosResponse<void>> {
        return await axiosInstance.put(`/property/admin/approve/${id}`);
    }

    static async Deny(id: number, reason: string): Promise<AxiosResponse<void>> {
        return await axiosInstance.put(`/property/admin/deny/${id}`, { "reason": reason });
    }

    static async GetApproved(id: number): Promise<AxiosResponse<PropertyDTO>> {
        return await axiosInstance.get(`/property/user/approved/${id}`);
    }

    static async GetPropertyImageBlob(id: number): Promise<AxiosResponse<Blob>> {
        return await axiosPhoto.get(`/property_${id}.jpg`, { responseType: 'blob' });
    }

    static async getDirectlyOwnedApproved(page: number, count: number): Promise<AxiosResponse<PropertyDTO[]>> {
        return await axiosInstance.get(`/property/user/owned/approved/paged?page=${page}&size=${count}`);
    }

    static async getDirectlyOwnedPending(page: number, count: number): Promise<AxiosResponse<PropertyDTO[]>> {
        return await axiosInstance.get(`/property/user/owned/pending/paged?page=${page}&size=${count}`);
    }


    static async getAllSharedWithMe(page: number, count: number): Promise<AxiosResponse<PropertyDTO[]>> {
        return await axiosInstance.get(`/property/user/shared?page=${page}&size=${count}`);
    }
}