import { axiosInstance, axiosPhoto } from "@/util/http";
import type { AxiosResponse } from "axios";
import {v4 as uuidv4} from 'uuid';
import type { PropertyType } from "./property.api";

export enum DevicePowerSupply {
    Battery, Network
};

export enum DeviceType {
    ac = 'ac',
    dht = 'dht',
    wash = 'wash',
    lamp = 'lamp',
    gate = 'gate',
    sprinkler = 'sprinkler',
    battery = 'battery',
    solar = 'solar',
    charger = 'charger',
};

export const deviceTypeToHumanText = (type: DeviceType | undefined): string => {
    switch (type) {
        case DeviceType.ac: return "Air conditioner";
        case DeviceType.dht: return "Ambience sensor";
        case DeviceType.wash: return "Wash machine";
        case DeviceType.lamp: return "Lamp";
        case DeviceType.gate: return "Gate";
        case DeviceType.sprinkler: return "Sprinkler";
        case DeviceType.battery: return "House battery";
        case DeviceType.solar: return "Solar panels";
        case DeviceType.charger: return "Vehicle charger";
        default: return "Device";
    }
}

export interface DeviceDTO {
    id: number,
    propertyId: number,
    online: boolean,
    powerSupply: DevicePowerSupply,
    powerDrainage: number,
    type: DeviceType
};

export interface DeviceRegisterDTO {
    propertyId: number,
    type: DeviceType,
    imageB64: string,
    powerSupply: DevicePowerSupply,
    powerDrainage: number,
};

export interface DeviceMetaDTO {
    id: number,
    propertyId: number,
    propertyType: PropertyType,
    type: DeviceType,
    owned: boolean,
};

export interface DeviceHistoricalDTO {
    id: number,
    mode: string,
    date1?: Date,
    date2?: Date,
}

export type DHTHistoricalDTO = DeviceHistoricalDTO

export interface DHTReadingDTO {
    timestamp: Date,
    temperature: number,
    humidity: number
};

export interface DeviceCommandUserMeta {
    id: number,
    name: string,
    lastName: string,
}
export interface DeviceCommandHistoryMetaDTO {
    id: number,
    users: DeviceCommandUserMeta[],
}

export interface InfluxPaginator {
    pageSize: number,
    pageCount: number,
    sortColumn: string,
    sortDesc: boolean
}

export interface DeviceCommandHistoryRequestDTO {
    id: number,
    users: Array<string>,
    pagination: InfluxPaginator,
    minDate: Date,
    maxDate: Date,
}

export type GateCommandHistoryRequestDTO = DeviceCommandHistoryRequestDTO

interface DeviceCommandHistoryDTO<Item> {
    list: Array<Item>,
    totalItems: number,
    pagination: InfluxPaginator,
}

export interface DeviceAvailabilityReport {
    start: Date,
    end: Date,
    durationInS: number,
    percentage: number,
}

export enum AvailabilityDistanceUnit {
    days = "DAYS",
    hours = "HOURS",
}

export interface DeviceAvailabilityReportDTO {
    unit: AvailabilityDistanceUnit
    reports: DeviceAvailabilityReport[]
}

export class DeviceService {
    static async RegisterDevice(dto: DeviceRegisterDTO): Promise<AxiosResponse<void>> {
        return await axiosInstance.post(`/device`, dto);
    }

    static async GetDeviceMeta(id: number): Promise<AxiosResponse<DeviceMetaDTO>> {
        return await axiosInstance.get(`/device/meta/${id}`);
    }

    static async GetDHTHIstorical(dto: DHTHistoricalDTO): Promise<AxiosResponse<Array<DHTReadingDTO>>> {
        return await axiosInstance.put(`/device/dht/history`, dto);
    }

    static async GetDeviceAvailabilityReport(dto: DeviceHistoricalDTO): Promise<AxiosResponse<DeviceAvailabilityReportDTO>> {
        return await axiosInstance.put(`/device/availability-report`, dto);
    }

    static async getDeviceImageBlob(id: number): Promise<AxiosResponse<Blob>> {
        return await axiosPhoto.get(`/device_${id}.jpg`, { responseType: 'blob' });
    }
}