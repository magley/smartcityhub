interface MapItem {
    colorClass: string,
    imageFile: string,
    label: string,
    canBePoweredByBattery: boolean
};

export const map = new Map<string, MapItem>([
    ['ac', {
        colorClass: 'bg-orange',
        imageFile: 'device_ac.png',
        label: 'ac',
        canBePoweredByBattery: false,
    }],
    ['dht', {
        colorClass: 'bg-orange',
        imageFile: 'device_dht_sensor.png',
        label: 'dht',
        canBePoweredByBattery: true,
    }],
    ['wash', {
        colorClass: 'bg-orange',
        imageFile: 'device_wash_machine.png',
        label: 'wash',
        canBePoweredByBattery: false,
    }],
    ['lamp', {
        colorClass: 'bg-blue',
        imageFile: 'device_lamp.png',
        label: 'lamp',
        canBePoweredByBattery: true,
    }],
    ['gate', {
        colorClass: 'bg-blue',
        imageFile: 'device_gate.png',
        label: 'gate',
        canBePoweredByBattery: false,
    }],
    ['sprinkler', {
        colorClass: 'bg-blue',
        imageFile: 'device_sprinkler.png',
        label: 'sprinkler',
        canBePoweredByBattery: true,
    }],
    ['battery', {
        colorClass: 'bg-red',
        imageFile: 'device_battery.png',
        label: 'battery',
        canBePoweredByBattery: false,
    }],
    ['solar', {
        colorClass: 'bg-red',
        imageFile: 'device_solar_panel.png',
        label: 'solar',
        canBePoweredByBattery: false,
    }],
    ['charger', {
        colorClass: 'bg-red',
        imageFile: 'device_vehicle_charger.png',
        label: 'charger',
        canBePoweredByBattery: false,
    }],
]);