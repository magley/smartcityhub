import { AuthService } from "@/api/auth.api";
import { reactive } from "vue";

export const userStore = reactive({
    role: AuthService.getRole(),
    refresh() {
        this.role = AuthService.getRole();
    },
    hasRole(r: string): boolean {
        return this.role == r;
    },
    signedOut(): boolean {
        return this.role == "";
    }
});