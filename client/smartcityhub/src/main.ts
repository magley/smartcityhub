import { createApp } from 'vue'
import { createPinia } from 'pinia'

import router from './router/router';
import App from './App.vue'

import PrimeVue from 'primevue/config';
import 'primeicons/primeicons.css'
import "primevue/resources/themes/viva-light/theme.css";
import "/node_modules/primeflex/primeflex.css";
import ToastService from 'primevue/toastservice';
import ConfirmationService from 'primevue/confirmationservice';
import DialogService from 'primevue/dialogservice';
import Tooltip from 'primevue/tooltip';
import Ripple from 'primevue/ripple';

const app = createApp(App)
app.use(createPinia())
app.use(PrimeVue, { ripple: true });
app.use(router);
app.use(ToastService);
app.use(ConfirmationService);
app.use(DialogService);
app.directive('tooltip', Tooltip);
app.directive('ripple', Ripple);

app.mount('#app');