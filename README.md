![Logo](client/smartcityhub/public/favicon.png)

# SmartCityHub

SmartCityHub is a centralized IoT management service.

## Team members

- SV 20/2020 - Team member 2
- SV 25/2020 - Team member 1

## Technologies

- Spring Boot (_Kotlin_)
- Vue (_Typescript_)
- Gorilla web toolkit (_Golang_)
- MariaDB
- Nginx
- Mosquitto
- InfluxDB
- Telegraf
- Elasticsearch
- Redis

## Instructions

1) Start mosquitto (see `mosquitto/README.md`)
2) Start nginx (see `nginx/README.md`)
3) Start influxdb (see `influxdb/README.md`)
4) Start telegraf (see `telegraf/README.md`)
5) Start redis (see `socketserver/README.md`)
6) Start websocket server (see `socketserver/README.md`)
7) Start elasticsearch (see `elasticsearch/README.md`)
8) Start frontend app (see `client/smartcityhub/README.md`)
9) Start backend app (see `server/smartcityhub/README.md`)

## The stack

![The tech stack for SmartCityHub](docs/SmartCityHub_Stack.png)
