Setting up
---

1. Download [telegraf](https://dl.influxdata.com/telegraf/releases/telegraf-1.28.5_windows_amd64.zip)
2. Unzip anywhere
3. Set environment variable: `set INFLUX_TOKEN=[your influxdb token]`
4. Run `./telegraf.exe --config "./telegraf.conf"`


Instead of setting the env variable, locating to telegraf.exe and then passing in the conf file -- write a batch script:

```powershell
@echo off
setlocal 
  set "INFLUX_TOKEN=[your influxdb token]"
  cd /d "[folder where telegraf.exe is]"
  telegraf.exe --config "[folder where telegraf.conf is (in this repo)]"
  set INFLUX_TOKEN=
endlocal
```

This will also make `INFLUX_TOKEN` temporary to that process, so it doesn't leak into your env variables.