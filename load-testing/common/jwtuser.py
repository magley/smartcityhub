from locust import HttpUser

class JwtUser(object):
    """
        Wrapper for `HttpUser` that injects the JWT in request headers.
    """
    def __init__(self, httpUser: HttpUser, jwt: str):
        self.jwt = jwt
        self.httpUser = httpUser
        self.headers = { 'Authorization': f"Bearer {jwt}" }

    def get(self, url: str):
        return self.httpUser.client.get(url, headers=self.headers)
    
    def post(self, url: str, body):
        return self.httpUser.client.post(url, json=body, headers=self.headers)
    
    def put(self, url: str, body):
        return self.httpUser.client.put(url, json=body, headers=self.headers)
    
    def delete(self, url: str):
        return self.httpUser.client.delete(url, headers=self.headers)