class Result(object):
    """
        Kind of like Rust's Result. 
    """
    
    def __init__(self, value, error):
        self.value = value
        self.error = error
        pass

    def ok(value):
        return Result(value, False)
    
    def err(msg):
        return Result(msg, True)
    
    def unwrap(self):
        if self.error:
            raise Exception(self.value)
        else:
            return self.value
        
    def expect(self, msg: str):
        if self.error:
            raise Exception(msg)
        else:
            return self.value