from locust import HttpUser
from common.result import Result

def login(httpUser: HttpUser, username: str, password: str):
    """
        Utility function for logging in.
    """

    body = {
        "username": username,
        "password": password
    }
    resp = httpUser.client.post("session/login", json=body)

    if resp.status_code == 200:
        return Result.ok(resp.content.decode("utf-8"))
    else:
        return Result.err(resp.content.decode("utf-8"))