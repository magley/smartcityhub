import uuid
from locust import HttpUser, task, between
import common.auth as auth
from common.jwtuser import JwtUser
import random
from urllib3 import PoolManager
import datetime
import json

class User(HttpUser):
    wait_time = between(0.25, 1.0)
    cli = None
    self_id = None
    pool_manager = PoolManager(maxsize=5000, block=True)

    def generate_custom_config(self, id):
        '''
            ```kotlin
            data class ACCmdCustomModeDTO (
                val id: Long,
                val customMode: List<ACCustomModeItemDTO>,
                val uuid: String
            )

            data class ACCustomModeItemDTO (
                val startHour: Int,
                val endHour: Int,
                val mode: ACMode,
                val temperature: Double
            )
            ```      
        '''

        number_of_items = random.randint(0, 5)
        customMode = []
        uuid_v = str(uuid.uuid4())

        t = 2
        for _ in range(number_of_items):
            customMode.append({
                "startHour": t,
                "endHour": t+1,
                "mode": random.choice(['cool', 'heat', 'auto', 'fan']),
                "temperature": random.uniform(11, 33),
            })

            t += 2

        return {
            "id": id,
            "customMode": customMode,
            "uuid": uuid_v,
        }

    @task
    def get_ac_history_no_users(self):
        # Sign in.
        jwt = auth.login(self, "client", "123").unwrap()
        self.cli = JwtUser(self, jwt)
        approved_props = self.cli.get("property/user/owned/approved").json()

        device_id = random.randint(1009, 1309)

        body = self.generate_custom_config(device_id)

        self.cli.put("device/ac/config", body)