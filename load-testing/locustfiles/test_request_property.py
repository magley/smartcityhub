import uuid
from locust import HttpUser, task, between
import common.auth as auth
from common.jwtuser import JwtUser
import random
from urllib3 import PoolManager
import datetime
import json

property_types = ['House', 'Apartment']

class User(HttpUser):
    wait_time = between(1, 2)
    cli = None
    self_id = None
    pool_manager = PoolManager(maxsize=5000, block=True)

    def on_start(self):
        usernames = ["client1@gmail.com",
                     "client2@gmail.com",
                     "client3@gmail.com",
                     "client4@gmail.com",
                     "client5@gmail.com",
                     "client6@gmail.com",
                     "client7@gmail.com",
                     "client8@gmail.com",
                     "client9@gmail.com" ]
        self.self_id = random.randint(1, 9)
        username = usernames[self.self_id - 1]

        # Sign in.
        jwt = auth.login(self, username, f"123456_{self.self_id}").unwrap()
        self.cli = JwtUser(self, jwt)


    @task
    def request_property(self):
        type = random.choice(property_types)
        body = {
            'type': type,
            'address': 'random address',
            'cityId': 1,
            'size': 10.5,
            'floors': 1,
            'location': { 'latitude': 0.0, 'longitude': 0.0 },
            'imageB64': 'iVBORw0KGgoAAAANSUhEUgAAAAgAAAAIAQMAAAD+wSzIAAAABlBMVEX///+/v7+jQ3Y5AAAADklEQVQI12P4AIX8EAgALgAD/aNpbtEAAAAASUVORK5CYII'
        }

        self.cli.post("property/request", body)