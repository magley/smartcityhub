from locust import HttpUser, task, between
import common.auth as auth
from common.jwtuser import JwtUser
import random

class User(HttpUser):
    wait_time = between(2, 5)

    def register_ac(cli: JwtUser, propertyId: int):
        body = {
            "base": {
                "propertyId": propertyId,
                "type": "ac",
                "powerSupply": "Battery",
                "powerDrainage": 123,
                "imageB64": "iVBORw0KGgoAAAANSUhEUgAAAAgAAAAIAQMAAAD+wSzIAAAABlBMVEX///+/v7+jQ3Y5AAAADklEQVQI12P4AIX8EAgALgAD/aNpbtEAAAAASUVORK5CYII"
            },
            "supportedModes": [
                "heat", "fan"
            ],
            "minTemperature": 14,
            "maxTemperature": 30,
        }
        resp = cli.post("device/ac", body)
        if not resp.ok:
            raise Exception(resp.content)

    @task
    def register_device_property1(self):
        usernames = [ "client", "client2", "client3", "client4", "client5", "client6", "client7", "client8", "client9" ] 
        username = random.choice(usernames)

        # Sign in.
        jwt = auth.login(self, username, "123").unwrap()
        cli = JwtUser(self, jwt)

        # Get all approved properties
        approved_props = cli.get("property/user/owned/approved").json()
        property = random.choice(approved_props)

        # Register device for one of the properties
        User.register_ac(cli, property['id'])