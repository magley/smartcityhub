from locust import HttpUser, task, between
import common.auth as auth
from common.jwtuser import JwtUser
import random
from urllib3 import PoolManager
import datetime
import json

class User(HttpUser):
    wait_time = between(0.25, 1.0)
    cli = None
    self_id = None
    pool_manager = PoolManager(maxsize=5000, block=True)

    def on_start(self):
        jwt = auth.login(self, "admin", "admin").unwrap()
        self.cli = JwtUser(self, jwt)

    @task(4)
    def add_ok_admin(self):
        self.cli.get("user/all/admins")

        body = {
            "email": f"notsuperadmin_{random.randint(0, 1000000000)}@gmail.com",
            "password": "admin",
            "name": "Adam",
            "lastName": "Mini",
        }

        self.cli.post("user/admin", body)

    @task
    def add_bad_admin(self):
        self.cli.get("user/all/admins")

        body = {
            "email": "admin", # Already taken
            "password": "admin",
            "name": "Adam",
            "lastName": "Mini",
        }

        self.cli.post("user/admin", body)