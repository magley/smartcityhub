from locust import HttpUser, task, between
import common.auth as auth
from common.jwtuser import JwtUser
import random
from urllib3 import PoolManager
import datetime
import json

class User(HttpUser):
    wait_time = between(0.25, 1.0)
    cli = None
    self_id = None
    pool_manager = PoolManager(maxsize=5000, block=True)

    @task
    def get_dht_history_6h(self):
        # Sign in.
        jwt = auth.login(self, "client", "123").unwrap()
        self.cli = JwtUser(self, jwt)

        # Select random property. This doesn't affect the device being picked,
        # because there's no easy way to controln (at a large scale) which device
        # is DHT. For testing purposes with many devices, it's okay to pick at
        # random, the bottleneck _will_ be InfluxDB.
        approved_props = self.cli.get("property/user/owned/approved").json()

        # Select device. 
        device_id = random.randint(1, 1000)

        body = {
            "id": device_id,
            "mode": "mode6h",
        }

        self.cli.put("device/dht/history", body)

    @task
    def get_dht_history_12h(self):
        jwt = auth.login(self, "client", "123").unwrap()
        self.cli = JwtUser(self, jwt)
        approved_props = self.cli.get("property/user/owned/approved").json()
        device_id = random.randint(1, 1000)

        body = {
            "id": device_id,
            "mode": "mode12h",
        }

        self.cli.put("device/dht/history", body)

    @task
    def get_dht_history_24h(self):
        jwt = auth.login(self, "client", "123").unwrap()
        self.cli = JwtUser(self, jwt)
        approved_props = self.cli.get("property/user/owned/approved").json()
        device_id = random.randint(1, 1000)

        body = {
            "id": device_id,
            "mode": "mode24h",
        }

        self.cli.put("device/dht/history", body)

    @task
    def get_dht_history_1w(self):
        jwt = auth.login(self, "client", "123").unwrap()
        self.cli = JwtUser(self, jwt)
        approved_props = self.cli.get("property/user/owned/approved").json()
        device_id = random.randint(1, 1000)

        body = {
            "id": device_id,
            "mode": "mode1w",
        }

        self.cli.put("device/dht/history", body)

    @task
    def get_dht_history_1m(self):
        jwt = auth.login(self, "client", "123").unwrap()
        self.cli = JwtUser(self, jwt)
        approved_props = self.cli.get("property/user/owned/approved").json()
        device_id = random.randint(1, 1000)

        body = {
            "id": device_id,
            "mode": "mode1m",
        }

        self.cli.put("device/dht/history", body)

    @task
    def get_dht_history_custom(self):
        jwt = auth.login(self, "client", "123").unwrap()
        self.cli = JwtUser(self, jwt)
        approved_props = self.cli.get("property/user/owned/approved").json()
        device_id = random.randint(1, 1000)

        date1 = datetime.datetime.now()
        date2 = date1 - datetime.timedelta(days=28) # Can't be more than 1mo, this is a safe number.

        body = {
            "id": device_id,
            "mode": "modeCustom",
            "date1": date1.isoformat(sep='T'),
            "date2": date2.isoformat(sep='T'),
        }

        print(body)

        self.cli.put("device/dht/history", body)