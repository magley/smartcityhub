from locust import HttpUser, task, between
import common.auth as auth
from common.jwtuser import JwtUser
import random
from urllib3 import PoolManager
import datetime
import json

class User(HttpUser):
    wait_time = between(0.25, 1.0)
    cli = None
    self_id = None
    pool_manager = PoolManager(maxsize=5000, block=True)

    def on_start(self):
        jwt = auth.login(self, "admin", "a1b06842-ef89-4").unwrap()
        self.cli = JwtUser(self, jwt)

    @task
    def approve_property(self):
        id = random.randint(1, 10000)

        self.cli.put(f"property/admin/approve/{id}", {})