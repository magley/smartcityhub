from locust import HttpUser, task, between
import common.auth as auth
from common.jwtuser import JwtUser
import random

class HelloWorldUser(HttpUser):
    wait_time = between(0.5, 2.0)

    @task
    def login(self):
        usernames = [
            "client", "client2", "client3", "client4", "client5",
            "client6", "client7", "client8", "client9"
        ]

        jwt = auth.login(self, random.choice(usernames), "123").unwrap()
        cli = JwtUser(self, jwt)