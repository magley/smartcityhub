import uuid
from locust import HttpUser, task, between
import common.auth as auth
from common.jwtuser import JwtUser
import random
from urllib3 import PoolManager
import datetime
import json

class User(HttpUser):
    wait_time = between(2, 5)
    cli = None
    self_id = None
    pool_manager = PoolManager(maxsize=5000, block=True)

    def on_start(self):
        usernames = ["client1@gmail.com",
                     "client2@gmail.com",
                     "client3@gmail.com",
                     "client4@gmail.com",
                     "client5@gmail.com",
                     "client6@gmail.com",
                     "client7@gmail.com",
                     "client8@gmail.com",
                     "client9@gmail.com" ]
        self.self_id = random.randint(1, 9)
        username = usernames[self.self_id - 1]

        # Sign in.
        jwt = auth.login(self, username, f"123456_{self.self_id}").unwrap()
        self.cli = JwtUser(self, jwt)


    @task
    def change_password(self):
        body = {
            'oldPassword': f"123456_{self.self_id}",
            'newPassword': f"123456_{self.self_id}",
        }

        self.cli.post("user/changepass", body)