import uuid
from locust import HttpUser, task, between
import common.auth as auth
from common.jwtuser import JwtUser
import random
from urllib3 import PoolManager
import datetime
import json

class User(HttpUser):
    wait_time = between(2, 5)
    cli = None
    self_id = None
    pool_manager = PoolManager(maxsize=5000, block=True)

    def generate_trigger(self):
        return {
            'hour': random.randint(0, 23),
            'minute': random.randint(0, 59),
            'days': [d for d in range(random.randint(0, 6))],
            'isOn': random.choice([True, False])
        }

    @task
    def open(self):
        # Sign in.
        jwt = auth.login(self, "client", "123").unwrap()
        self.cli = JwtUser(self, jwt)
        approved_props = self.cli.get("property/user/owned/approved").json()

        device_id = random.randint(606, 906)

        # Generate up to 5 triggers
        triggers = [self.generate_trigger() for _ in range(random.randint(0, 5))]

        body = {
            'id': device_id,
            'uuid': str(uuid.uuid4()),
            'triggers': triggers,
        }

        self.cli.put("device/sprinklers/triggers", body)