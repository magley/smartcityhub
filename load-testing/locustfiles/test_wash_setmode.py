import uuid
from locust import HttpUser, task, between
import common.auth as auth
from common.jwtuser import JwtUser
import random
from urllib3 import PoolManager
import datetime
import json

class User(HttpUser):
    wait_time = between(0.25, 1.0)
    cli = None
    self_id = None
    pool_manager = PoolManager(maxsize=5000, block=True)

    def on_start(self):
        jwt = auth.login(self, "client", "123").unwrap()
        self.cli = JwtUser(self, jwt)
        self.cli.get("property/user/owned/approved").json()

    @task
    def get_ac_history_no_users(self):
        device_id = random.randint(100_537, 100_837)

        body = {
            'id': device_id,
            'uuid': str(uuid.uuid4()),
            'mode': random.choice(['off','cotton_95','cotton_60','cotton_40','synthetic_60','synthetic_40','wool_40'])
        }

        self.cli.put("device/wash/mode", body)