from locust import HttpUser, task, between
import common.auth as auth
from common.jwtuser import JwtUser
import random
from urllib3 import PoolManager
import datetime
import json

class User(HttpUser):
    wait_time = between(5, 10)
    cli = None
    self_id = None
    pool_manager = PoolManager(maxsize=5000, block=True)

    @task
    def get_availability_history_6h(self):
        # Sign in.
        jwt = auth.login(self, "client", "123").unwrap()
        self.cli = JwtUser(self, jwt)

        # Select device. 
        device_id = 1

        body = {
            "id": device_id,
            "mode": "mode6h",
        }

        self.cli.put("device/availability-report", body)

    @task
    def get_availability_history_12h(self):
        jwt = auth.login(self, "client", "123").unwrap()
        self.cli = JwtUser(self, jwt)
        device_id = 1

        body = {
            "id": device_id,
            "mode": "mode12h",
        }

        self.cli.put("device/availability-report", body)

    @task
    def get_availability_history_24h(self):
        jwt = auth.login(self, "client", "123").unwrap()
        self.cli = JwtUser(self, jwt)
        device_id = 1

        body = {
            "id": device_id,
            "mode": "mode24h",
        }

        self.cli.put("device/availability-report", body)

    @task
    def get_availability_history_1w(self):
        jwt = auth.login(self, "client", "123").unwrap()
        device_id = 1

        body = {
            "id": device_id,
            "mode": "mode1w",
        }

        self.cli.put("device/availability-report", body)

    @task
    def get_availability_history_1m(self):
        jwt = auth.login(self, "client", "123").unwrap()
        self.cli = JwtUser(self, jwt)
        device_id = 1

        body = {
            "id": device_id,
            "mode": "mode1m",
        }

        self.cli.put("device/availability-report", body)

    @task
    def get_availability_history_custom(self):
        jwt = auth.login(self, "client", "123").unwrap()
        self.cli = JwtUser(self, jwt)
        device_id = 1

        date1 = datetime.datetime.now()
        date2 = date1 - datetime.timedelta(days=28) # Can't be more than 1mo, this is a safe number.

        body = {
            "id": device_id,
            "mode": "modeCustom",
            "date1": date1.isoformat(sep='T'),
            "date2": date2.isoformat(sep='T'),
        }

        print(body)

        self.cli.put("device/availability-report", body)