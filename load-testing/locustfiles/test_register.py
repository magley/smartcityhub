from locust import HttpUser, task, between
from urllib3 import PoolManager
import common.auth as auth
from common.jwtuser import JwtUser
import random

class HelloWorldUser(HttpUser):
    wait_time = between(2, 5)
    pool_manager = PoolManager(maxsize=5000, block=True)

    def random_string(self):
        alphabet = "qwertyuiopasdfghjklzxcvbnm1234567890"
        return ''.join([random.choice(alphabet) for _ in range(10)])

    @task
    def register(self):
        payload = {
            "email": self.random_string() + "@gmail.com",
            "name": self.random_string() + "@gmail.com",
            "lastName": self.random_string() + "@gmail.com",
            "password": self.random_string(),
            "avatarB64": "iVBORw0KGgoAAAANSUhEUgAAAAgAAAAIAQMAAAD+wSzIAAAABlBMVEX///+/v7+jQ3Y5AAAADklEQVQI12P4AIX8EAgALgAD/aNpbtEAAAAASUVORK5CYII",
        }

        self.client.post("user/client", json=payload)