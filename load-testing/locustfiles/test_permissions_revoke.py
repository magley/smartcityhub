from locust import HttpUser, task, between
import common.auth as auth
from common.jwtuser import JwtUser
import random
from urllib3 import PoolManager

class User(HttpUser):
    wait_time = between(0.25, 1.0)
    cli = None
    self_id = None
    pool_manager = PoolManager(maxsize=5000, block=True)

    def on_start(self):
        usernames = [ "client", "client2", "client3", "client4", "client5", "client6", "client7", "client8", "client9" ] 
        self.self_id = random.randint(1, 9)
        username = usernames[self.self_id - 1]

        # Sign in.
        jwt = auth.login(self, username, "123").unwrap()
        self.cli = JwtUser(self, jwt)

    @task
    def revoke_permission_for_property(self):
        for _ in range(100):
            # Select random property.
            approved_props = self.cli.get("property/user/owned/approved").json()
            property = random.choice(approved_props)

            # Get all permissions for that property.
            permissions = self.cli.get(f"permission/property/{property['id']}").json()

            if len(permissions) == 0:
                continue
            
            # Select random permission.
            p = random.choice(permissions)

            # Revoke that permission.
            user_id = random.randint(1, 9)
            if user_id not in [p['userId'] for p in permissions] and user_id != self.self_id:
                result = self.cli.put("permission/property/revoke", {
                    "userId": p['userId'],
                    "entityId": p['entityId'],
                    "entityType": "property"
                })

                if not result.ok:
                    print(result)
                    print(result.content)
                break
         
    @task
    def revoke_permission_for_device(self):
        for _ in range(100):
            # Select random property.
            approved_props = self.cli.get("property/user/owned/approved").json()
            property = random.choice(approved_props)

            # Get all devices for that property.
            property_with_devices = self.cli.get(f"property/{property['id']}").json()

            # Select random device.
            if len(property_with_devices['devices']) == 0:
                continue
                
            device = random.choice(property_with_devices['devices'])

            # Get all permissions for that device.
            permissions = self.cli.get(f"permission/device/{device['id']}").json()

            if len(permissions) == 0:
                continue
            
            # Select random permission.
            p = random.choice(permissions)

            # Revoke that permission.
            user_id = random.randint(1, 9)
            if user_id not in [p['userId'] for p in permissions] and user_id != self.self_id:
                result = self.cli.put("permission/device/revoke", {
                    "userId": p['userId'],
                    "entityId": p['entityId'],
                    "entityType": "device"
                })

                if not result.ok:
                    print(result)
                    print(result.content)
                break