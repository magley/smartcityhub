import uuid
from locust import HttpUser, task, between
import common.auth as auth
from common.jwtuser import JwtUser
import random
from urllib3 import PoolManager
import datetime
import json

class User(HttpUser):
    wait_time = between(0.25, 1.0)
    cli = None
    self_id = None
    pool_manager = PoolManager(maxsize=5000, block=True)

    @task
    def get_ac_history_no_users(self):
        # Sign in.
        jwt = auth.login(self, "client", "123").unwrap()
        self.cli = JwtUser(self, jwt)
        approved_props = self.cli.get("property/user/owned/approved").json()

        device_id = random.randint(1009, 1309)

        body = {
            'id': device_id,
            'uuid': str(uuid.uuid4()),
            'mode': random.choice(['cool', 'heat', 'auto', 'fan'])
        }

        self.cli.put("device/ac/mode", body)