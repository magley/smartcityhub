from locust import HttpUser, task, between
import common.auth as auth
from common.jwtuser import JwtUser
import random
from urllib3 import PoolManager
import datetime
import json

class User(HttpUser):
    wait_time = between(0.25, 1.0)
    cli = None
    self_id = None
    pool_manager = PoolManager(maxsize=5000, block=True)

    @task
    def get_gate_history_no_users(self):
        # Sign in.
        jwt = auth.login(self, "client", "123").unwrap()
        self.cli = JwtUser(self, jwt)
        approved_props = self.cli.get("property/user/owned/approved").json()

        device_id = 2

        max_date = datetime.datetime.now()
        min_date = max_date - datetime.timedelta(days=random.randint(1, 28))

        body = {
            "id": device_id,
            "users": [],
            "pagination": {
                "pageSize": 10,
                "pageCount": random.randint(0, 2),
                "sortColumn": "_time"
            },
            "minDate": min_date.isoformat(sep='T'),
            "maxDate": max_date.isoformat(sep='T')
        
        }

        self.cli.post("device/gate/command-history", body)
