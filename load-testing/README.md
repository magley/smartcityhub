# Installation

`pip install -r requirements.txt`

# Writing a test.

Check [the documentation](https://docs.locust.io/en/stable/quickstart.html).
Request URLs are relative to `/api/` i.e. they should be `user/login` etc.

# Running a test module.

`locust -f [filename.py]`
For hostname, use `http://localhost:8080/api/`.