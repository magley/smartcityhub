# Getting started

1) Install [InfluxDB OSS](https://docs.influxdata.com/influxdb/v2/install/?t=Windows)
2) Start influxdb by running `influxd.exe`
3) Go to `localhost:8086` to verify it's running.
4) Add tasks from `/tasks` (see `tasks/README.md`)