Setting up nginx:

1) [Download nginx](http://nginx.org/en/download.html), latest stable version 1.24.0.
2) Extract anywhere and run the .exe (there should be a `nginx.pid` in `/logs` if it's running).
3) Create the following file system hierarchy:

```
D:/
    nvt-data
        images
            avatar
            photo
```

The idea is that we don't want static data on C:/ since it can take up space and
we want it to be portable (between machines, during development). The backend saves
user-uploaded images here and nginx serves those images from there as well.

4) The `nginx.conf` file from this repo must be manually copied to your nginx folder `/conf`. Since this
can get tedious, I recommend creating a batch file like this:

```bat
copy [smartcityhub on your pc]\nginx\nginx.conf [nginx on your pc]\nginx-1.24.0\nginx-1.24.0\conf\nginx.conf
nginx -s reload
```

So that running the batch script copies the configuration and reloads the server.

5) Use `nginx -s quit` to close the server.
