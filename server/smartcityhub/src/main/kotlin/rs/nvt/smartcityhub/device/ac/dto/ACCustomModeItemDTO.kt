package rs.nvt.smartcityhub.device.ac.dto

import rs.nvt.smartcityhub.device.ac.ACMode

/**
 * `ACCustomModeItemDTO` is a single element in the array that defines an AC's custom mode.
 *
 * Each item has a start and end time, primitive mode at which it runs and the temperature to use.
 */
data class ACCustomModeItemDTO (
    val startHour: Int,
    val endHour: Int,
    val mode: ACMode,
    val temperature: Double
)