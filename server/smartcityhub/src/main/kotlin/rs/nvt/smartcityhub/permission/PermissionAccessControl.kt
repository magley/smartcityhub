package rs.nvt.smartcityhub.permission

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import rs.nvt.smartcityhub.device.Device
import rs.nvt.smartcityhub.exception.EntityNotFoundException
import rs.nvt.smartcityhub.property.Property
import rs.nvt.smartcityhub.user.User

@Service
class PermissionAccessControl {
    @Autowired private lateinit var propertyPermissionRepo: IPropertyPermissionRepo
    @Autowired private lateinit var devicePermissionRepo: IDevicePermissionRepo
    @Autowired private lateinit var permissionsService: IPermissionService

    /**
     * Warning: This is a relatively expensive function.
     */
    fun propertyHasPartialPermissions(user: User, property: Property): Boolean {
        try {
            assertPropertyPermissionIncludingPermissionsByDevice(user, property)
        } catch (e: EntityNotFoundException) {
            return false;
        }
        try {
            assertPropertyPermission(user, property)
        } catch (e: EntityNotFoundException) {
            return true;
        }
        return false;
    }
    fun assertPropertyPermissionIncludingPermissionsByDevice(user: User, property: Property) {
        if (property.owner.id == user.id) return
        if (propertyPermissionRepo.findByUserIdAndPropertyId(user.id, property.id) != null) return

        val allDevices = permissionsService.getDevicePermissions(user)
        val devicesOfProperty = allDevices.filter { d -> d.device.property.id == property.id }
        if (devicesOfProperty.isNotEmpty()) return

        throw EntityNotFoundException(Property::class, property.id)
    }
    fun assertPropertyPermission(user: User, property: Property) {
        if (property.owner.id == user.id) return
        if (propertyPermissionRepo.findByUserIdAndPropertyId(user.id, property.id) != null) return

        throw EntityNotFoundException(Property::class, property.id)
    }
    fun assertDevicePermission(user: User, device: Device) {
        if (device.property.owner.id == user.id) return
        if (propertyPermissionRepo.findByUserIdAndPropertyId(user.id, device.property.id) != null) return
        if (devicePermissionRepo.findByUserIdAndDeviceId(user.id, device.id) != null) return
        throw EntityNotFoundException(Device::class, device.id)
    }
}