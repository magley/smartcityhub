package rs.nvt.smartcityhub.device.wash

import com.fasterxml.jackson.databind.ObjectMapper
import com.influxdb.annotations.Column
import com.influxdb.client.InfluxDBClient
import org.eclipse.paho.mqttv5.common.MqttMessage
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import rs.nvt.smartcityhub.config.auth.IAuthenticationFacade
import rs.nvt.smartcityhub.config.mqtt.MqttClientWrapper
import rs.nvt.smartcityhub.device.IDeviceRepo
import rs.nvt.smartcityhub.device.ac.AC
import rs.nvt.smartcityhub.device.ac.ACCommandType
import rs.nvt.smartcityhub.device.ac.ACDTO
import rs.nvt.smartcityhub.device.ac.ACMode
import rs.nvt.smartcityhub.device.ac.dto.ACRegisterDTO
import rs.nvt.smartcityhub.device.ac.dto.cmd_history.ACCmdHistoryItemDTO
import rs.nvt.smartcityhub.device.ac.dto.cmd_history.ACCmdHistoryItemDTO_
import rs.nvt.smartcityhub.device.common.DeviceServiceCommon
import rs.nvt.smartcityhub.device.common.InfluxMapperResult_UserId
import rs.nvt.smartcityhub.device.dto.*
import rs.nvt.smartcityhub.device.sprinklers.SprinklersCmdHistoryItemDTO
import rs.nvt.smartcityhub.device.wash.dto.WashMachineRegisterDTO
import rs.nvt.smartcityhub.device.wash.dto.cmd.WashMachineCmdModeDTO
import rs.nvt.smartcityhub.device.wash.dto.cmd.WashMachineCmdMqttDTO
import rs.nvt.smartcityhub.device.wash.dto.cmd.WashMachineCmdScheduleDTO
import rs.nvt.smartcityhub.device.wash.dto.cmd_history.WashMachineCmdHistoryItemDTO
import rs.nvt.smartcityhub.device.wash.dto.cmd_history.WashMachineCmdHistoryItemDTO_
import rs.nvt.smartcityhub.property.Property
import rs.nvt.smartcityhub.user.IUserService
import rs.nvt.smartcityhub.user.User
import rs.nvt.smartcityhub.util.ImageSaveMode
import rs.nvt.smartcityhub.util.saveBase64ToImage
import java.time.Instant

@Service
class WashMachineService {
    @Autowired lateinit var mqttClient: MqttClientWrapper
    @Autowired lateinit var deviceRepo: IDeviceRepo
    @Autowired lateinit var authFacade: IAuthenticationFacade
    @Autowired lateinit var objectMapper: ObjectMapper
    @Autowired lateinit var cmdService: DeviceServiceCommon
    @Autowired private lateinit var influxDBClient: InfluxDBClient
    @Autowired lateinit var userService: IUserService

    fun registerWashMachine(dto: WashMachineRegisterDTO, property: Property): WashMachine {
        val device = WashMachine(dto.base, property)
        device.supportedModes = dto.supportedModes
        deviceRepo.save(device)
        saveBase64ToImage(dto.base.imageB64, "device_${device.id}", ImageSaveMode.PHOTO)
        return device
    }

    fun setMode(device: WashMachine, mode: WashMachineCmdModeDTO) {
        publishCommand(device, WashMachineCommandType.mode, mode.mode, mode.uuid)
    }

    fun schedule(device: WashMachine, schedule: WashMachineCmdScheduleDTO) {
        publishCommand(device, WashMachineCommandType.schedule, schedule, schedule.uuid)
    }

    fun removeSchedule(device: WashMachine, schedule: WashMachineCmdScheduleDTO) {
        publishCommand(device, WashMachineCommandType.removeSchedule, schedule, schedule.uuid)
    }

    private fun publishCommand(device: WashMachine, type: WashMachineCommandType, value: Any, uuid: String) {
        val userId = authFacade.getUser().id
        val cmd = WashMachineCmdMqttDTO(userId, type, value, uuid)
        val msg = MqttMessage(objectMapper.writeValueAsBytes(cmd))

        mqttClient.publish("wash/cmd/${device.id}", msg)
    }

    fun getHistory(device: WashMachine, dto: DeviceCmdHistoryRequestBodyDTO): DeviceCmdHistoryDTO<WashMachineCmdHistoryItemDTO_> {
        val items = cmdService.getCommandHistory(device, dto, WashMachineCmdHistoryItemDTO::class.java, "wash_w")
        val meta = userService
            .findByRoleAndIdIn(User.Role.CLIENT, items.list.map { u -> u.userId })
            .map{u -> DeviceCmdHistoryUserDataDTO(u.id, u.name, u.lastName)}

        val items2 = items.list.map { o ->
            val u = meta.find { ud -> ud.id == o.userId } ?: DeviceCmdHistoryUserDataDTO(
                DeviceCmdHistoryUserData_Automatic_ID,
                DeviceCmdHistoryUserData_Automatic_Name,
                DeviceCmdHistoryUserData_Automatic_LastName
            )
            WashMachineCmdHistoryItemDTO_(o, u)
        }

        return DeviceCmdHistoryDTO(items2, items.totalItems, items.pagination)
    }

    private fun getRelevantScheduledTasks_(device: WashMachine, daysBack: Long): List<WashMachineCmdScheduleDTO> {
        val query = """
            from(bucket: "nvtBucket")
                |> range(start: -${daysBack}d, stop: -${daysBack - 1}d)
                |> filter(fn: (r) => r._measurement == "wash_w")
                |> drop(columns: ["_start", "_stop", "host", "topic"])
                |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
                |> filter(fn: (r) => r.device_id == "${device.id}" and r.type == "schedule" and r.status == "true")
                |> filter(fn: (r) => r.end_time >= ${Instant.now().epochSecond})
                |> group(columns: ["_measurement"])
                |> drop(columns: ["_measurement", "device_id"])
                |> sort(columns: ["_time"], desc: true)
                |> map(fn: (r) => ({ r with status: bool(v: r.status) }))      
        """

        class InfluxMapperResult_WashMachineCmdScheduled {
            @Column(name = "user_id") var userId: Long = -1
            @Column(name = "uuid") lateinit var uuid: String
            @Column(name = "mode") lateinit var mode: WashMachineMode
            @Column(name = "startTime") var time: Long = 0
        }

        val result = influxDBClient
            .queryApi
            .query(query, InfluxMapperResult_WashMachineCmdScheduled::class.java)
            .map { d ->
                WashMachineCmdScheduleDTO(device.id, d.uuid, d.mode, Instant.ofEpochSecond(d.time))
            }

        return result
    }

    private fun getRelevantRemovedScheduledTasks_(device: WashMachine, daysBack: Long): List<WashMachineCmdScheduleDTO> {
        val query = """
            from(bucket: "nvtBucket")
                |> range(start: -${daysBack}d, stop: -${daysBack - 1}d)
                |> filter(fn: (r) => r._measurement == "wash_w")
                |> drop(columns: ["_start", "_stop", "host", "topic"])
                |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
                |> filter(fn: (r) => r.device_id == "${device.id}" and r.type == "removeSchedule" and r.status == "true")
                |> filter(fn: (r) => r.end_time >= ${Instant.now().epochSecond})
                |> group(columns: ["_measurement"])
                |> drop(columns: ["_measurement", "device_id"])
                |> sort(columns: ["_time"], desc: true)
                |> map(fn: (r) => ({ r with status: bool(v: r.status) }))      
        """

        class InfluxMapperResult_WashMachineCmdScheduled {
            @Column(name = "user_id") var userId: Long = -1
            @Column(name = "uuid") lateinit var uuid: String
            @Column(name = "mode") lateinit var mode: WashMachineMode
            @Column(name = "startTime") var time: Long = 0
        }

        val result = influxDBClient
            .queryApi
            .query(query, InfluxMapperResult_WashMachineCmdScheduled::class.java)
            .map { d ->
                WashMachineCmdScheduleDTO(device.id, d.uuid, d.mode, Instant.ofEpochSecond(d.time))
            }

        return result
    }

    fun getRelevantScheduledTasks(device: WashMachine, daysBack: Long): List<WashMachineCmdScheduleDTO> {
        val tasks = getRelevantScheduledTasks_(device, daysBack)
        val removedTasks = getRelevantRemovedScheduledTasks_(device, daysBack)
        val result = tasks.filter { t -> removedTasks.find { rt -> rt.uuid == t.uuid } == null }

        return result
    }
}