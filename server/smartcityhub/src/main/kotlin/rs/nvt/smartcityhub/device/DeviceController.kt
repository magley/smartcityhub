package rs.nvt.smartcityhub.device

import io.swagger.v3.oas.annotations.Operation
import jakarta.validation.Valid
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import rs.nvt.smartcityhub.config.auth.IAuthenticationFacade
import rs.nvt.smartcityhub.device.dto.DeviceAvailabilityReportDTO
import rs.nvt.smartcityhub.device.dto.DeviceMeasurementHistoryRequestBodyDTO
import rs.nvt.smartcityhub.device.dto.DeviceMetaDTO
import rs.nvt.smartcityhub.device.dto.DeviceRegisterDTO
import rs.nvt.smartcityhub.property.IPropertyService
import rs.nvt.smartcityhub.property.PropertyAccessControl

@RestController
@Validated
@RequestMapping("/api/device")
class DeviceController {
    @Autowired lateinit var deviceService: IDeviceService
    @Autowired lateinit var propertyService: IPropertyService
    @Autowired lateinit var propertyAccessControl: PropertyAccessControl
    @Autowired lateinit var authFacade: IAuthenticationFacade
    @Autowired lateinit var deviceAccessControl: DeviceAccessControl
    @Autowired lateinit var deviceAvailabilityService: DeviceAvailabilityService

    @Operation(summary = "Register a generic device", description = "Use this method unless a device type defines its own register method")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PostMapping()
    fun registerDevice(@Valid @RequestBody dto: DeviceRegisterDTO): ResponseEntity<Void> {
        val property = propertyService.getApproved(dto.propertyId)
        propertyAccessControl.assertOwnership(authFacade.getUser(), property)
        deviceService.registerDevice(dto, property)
        return ResponseEntity(null, HttpStatus.NO_CONTENT)
    }

    @Operation(summary = "Get metadata for this device")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @GetMapping("/meta/{id}")
    fun getDeviceMetadata(@PathVariable id: Long): ResponseEntity<DeviceMetaDTO> {
        val user = authFacade.getUser()
        val device = deviceService.find(id)
        deviceAccessControl.assertOwnership(authFacade.getUser(), device)
        return ResponseEntity(DeviceMetaDTO(device, user), HttpStatus.OK)
    }

    @Operation(summary = "Get report for ")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PutMapping("/availability-report")
    fun getDeviceAvailabilityReport(@RequestBody dto: DeviceMeasurementHistoryRequestBodyDTO): ResponseEntity<DeviceAvailabilityReportDTO> {
        val device = deviceService.find(dto.id)
        deviceAccessControl.assertOwnership(authFacade.getUser(), device)
        val res = deviceAvailabilityService.getDeviceAvailabilityReport(device, dto)
        return ResponseEntity(res, HttpStatus.OK)
    }
}