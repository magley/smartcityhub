package rs.nvt.smartcityhub.device.lamp

import com.influxdb.annotations.Column
import com.influxdb.annotations.Measurement
import rs.nvt.smartcityhub.device.dto.*
import java.time.Instant

/**
 * `LampCmdHistoryItemDTO` is a single command stored in the time-series database.
 */
class LampCmdHistoryItemDTO {
    @Column(name = "_time", timestamp = true) lateinit var timestamp: Instant
    @Column(name = "user_id") var userId: Long = -1
    @Column(name = "type", tag = true) lateinit var type: LampCommandType
    @Column(name = "uuid") lateinit var uuid: String
    @Column(name = "status", tag = true) var status: Boolean = false
    @Column(name = "is_on", tag = true) var isOn: Boolean? = null
    @Column(name = "mode", tag = true) var mode: LampMode? = null
    @Column(name = "threshold") var threshold: Double = 0.0
}

@Measurement(name = "lamp_m")
class LampHistoryItemMeasurementDTO {
    @Column(name= "_measurement", measurement = true) val measurement: String = ""
    @Column(name = "_time", timestamp = true) lateinit var timestamp: Instant
    @Column(name = "device_id", tag = true) var deviceId: String = ""
    @Column(name = "illuminance") var illuminance: Double = 0.0
}
/**
 * `LampMeasurementHistoryItemDTO` is a single item in the response body for a Lamp measurement history request.
 */
data class LampMeasurementHistoryItemDTO(val timestamp: Instant, val illuminance: Double) {
    constructor(measurement: LampHistoryItemMeasurementDTO) : this(measurement.timestamp, measurement.illuminance)
}

typealias LampCmdMqttDTO = DeviceCmdMqttDTO<LampCommandType>

data class LampCmdModeDTO (
    val id: Long,
    val uuid: String,
    val mode: LampMode,
    val threshold: Double,
)

data class LampCmdStartDTO (
    val id: Long,
    val uuid: String,
    val start: Boolean
)