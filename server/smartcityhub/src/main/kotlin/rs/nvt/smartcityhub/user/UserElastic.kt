package rs.nvt.smartcityhub.user

import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import org.springframework.data.elasticsearch.annotations.Document
import org.springframework.data.elasticsearch.annotations.Field
import org.springframework.data.elasticsearch.annotations.FieldType

//
@Document(indexName = "user")
class UserElastic {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0

    @Field(type = FieldType.Text, name = "fullName")
    lateinit var fullName: String

    constructor() {

    }

    constructor(u: User) {
        id = u.id
        fullName = (u.name + " " + u.lastName).trim()
    }
}