package rs.nvt.smartcityhub.device.wash.dto

import rs.nvt.smartcityhub.device.dto.DeviceRegisterDTO
import rs.nvt.smartcityhub.device.wash.WashMachineMode

data class WashMachineRegisterDTO (
    val base: DeviceRegisterDTO,
    val supportedModes: List<WashMachineMode>,
)