package rs.nvt.smartcityhub.device.gate

import jakarta.persistence.ElementCollection
import jakarta.persistence.Entity
import rs.nvt.smartcityhub.device.Device
import rs.nvt.smartcityhub.device.dto.DeviceRegisterDTO
import rs.nvt.smartcityhub.device.DeviceType
import rs.nvt.smartcityhub.property.Property


enum class GateSensorEvent {
    arrived, left, passed
}

enum class GateDirection {
    entry, exit
}

enum class GateOverride {
    yes, no,
}

enum class GateOpen {
    open, opening, closing, closed,
}

enum class GateMode {
    private, public,
}

enum class GateCommandType {
    open_state,
    mode,
    override,
    sensor_exit,
    sensor_entry,
}

@Entity
class Gate : Device {
    constructor() {
    }

    constructor(dto: DeviceRegisterDTO, property: Property) : super(dto, property) {
    }
    override fun getType(): DeviceType = DeviceType.gate

    @ElementCollection(targetClass = String::class)
    lateinit var plates: List<String>
}