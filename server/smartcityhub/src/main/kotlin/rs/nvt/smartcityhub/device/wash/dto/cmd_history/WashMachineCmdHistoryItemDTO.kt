package rs.nvt.smartcityhub.device.wash.dto.cmd_history

import com.influxdb.annotations.Column
import rs.nvt.smartcityhub.device.ac.ACCommandType
import rs.nvt.smartcityhub.device.ac.ACMode
import rs.nvt.smartcityhub.device.wash.WashMachineCommandType
import rs.nvt.smartcityhub.device.wash.WashMachineMode
import java.time.Instant

class WashMachineCmdHistoryItemDTO {
    @Column(name = "_time", timestamp = true) lateinit var timestamp: Instant
    @Column(name = "user_id") var userId: Long = -1
    @Column(name = "type", tag = true) lateinit var type: WashMachineCommandType
    @Column(name = "uuid") lateinit var uuid: String
    @Column(name = "status", tag = true) var status: Boolean = false
    @Column(name = "start_time", tag = true) var startTime: Long? = null
    @Column(name = "mode", tag = true) var mode: WashMachineMode? = null
}