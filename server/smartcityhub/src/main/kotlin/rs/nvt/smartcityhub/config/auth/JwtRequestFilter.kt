package rs.nvt.smartcityhub.config.auth

import jakarta.servlet.FilterChain
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import rs.nvt.smartcityhub.user.IUserService


@Component
class JwtRequestFilter : OncePerRequestFilter() {
    @Autowired
    private lateinit var userService: IUserService
    @Autowired
    private lateinit var jwtTokenUtil: JwtTokenUtil

    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {
        doFilter(request, response)
        chain.doFilter(request, response)
    }

    fun doFilter(request: HttpServletRequest, response: HttpServletResponse) {
        if (!request.requestURL.toString().contains("/api/")) {
            return
        }

        try {
            val jwt = getJwtFromRequest(request) as String
            val authToken = getAuthFromToken(jwt)
            SecurityContextHolder.getContext().authentication = authToken
        } catch (ex: Exception) {
            return
        }
    }

    private fun getJwtFromRequest(request: HttpServletRequest): String? {
        val header: String? = request.getHeader("Authorization")
        return jwtTokenUtil.getJwtFromHeader(header)
    }

    fun getAuthFromToken(jwt: String): UsernamePasswordAuthenticationToken {
        val userDetails: UserDetails = this.getUserDetailsFromJwtToken(jwt)
        return UsernamePasswordAuthenticationToken(userDetails, null, userDetails.authorities)
    }

    private fun getUserDetailsFromJwtToken(jwt: String): UserDetails {
        val username: String = jwtTokenUtil.getUsernameFromJwt(jwt)
        return userService.loadUserByUsername(username)
    }
}