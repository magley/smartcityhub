package rs.nvt.smartcityhub.permission.dto

data class NewPermissionDTO (
    val userId: Long,
    val entityId: Long,
    val entityType: PermissionTypeDTO
)