package rs.nvt.smartcityhub.user

import jakarta.persistence.*
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import rs.nvt.smartcityhub.user.dto.CreateUserDTO
import org.springframework.security.core.userdetails.UserDetails
import rs.nvt.smartcityhub.user.dto.ClientRegisterDTO

@Entity
@Table(name = "users")
@Cache(region = "User", usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
class User : UserDetails {
    enum class Role { SUPER_ADMIN, ADMIN, CLIENT }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0

    @Column(nullable = false, unique = true)
    lateinit var email: String

    @Column(nullable = false)
    lateinit var password_: String

    lateinit var name: String
    lateinit var lastName: String

    @Column(nullable = false)
    @Enumerated
    lateinit var role: Role

    @Column(nullable = false)
    var activated: Boolean = false

    @Column(nullable = false)
    var shouldChangeCurrentPassword: Boolean = false

    constructor()

    constructor(dto: CreateUserDTO) {
        email = dto.email
        password_ = dto.password
        name = dto.name
        lastName = dto.lastName
        role = Role.CLIENT
    }

    constructor(dto: ClientRegisterDTO) {
        email = dto.email
        password_ = dto.password
        name = dto.name
        lastName = dto.lastName
        role = Role.CLIENT
    }

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        fun role2authority(role: Role) = SimpleGrantedAuthority("ROLE_${role}")
        val auth = role2authority(role) as GrantedAuthority
        return listOf(auth).toMutableList()
    }

    override fun getPassword(): String = password_
    override fun getUsername(): String = email
    override fun isAccountNonExpired(): Boolean = true
    override fun isAccountNonLocked(): Boolean = true
    override fun isCredentialsNonExpired(): Boolean = true
    override fun isEnabled(): Boolean = activated
}