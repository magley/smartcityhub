package rs.nvt.smartcityhub.device

import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import rs.nvt.smartcityhub.device.ac.AC
import rs.nvt.smartcityhub.device.dht.DHT
import java.util.Optional


interface IDeviceRepo : JpaRepository<Device, Long> {
    @Query("select d as D_DHT from Device d where type(d) = DHT")
    fun findAllAmbienceSensors(): List<DHT>

    @Query("select d as D_AC from Device d where type(d) = AC")
    fun findAllACUnits(): List<AC>

    @Query("from Device d where d.property.id = :id")
    fun findAllDevicesByPropertyId(@Param("id") id: Long): List<Device>

    fun findByPropertyId(propertyId: Long, pageable: Pageable): List<Device>

    fun findByIdAndPropertyId(id: Long, propertyId: Long): Optional<Device>
}