package rs.nvt.smartcityhub.device.dto

/**
 * `DeviceCmdHistoryMetaDTO` is the metadata describing all the command issued for a device.
 */
class DeviceCmdHistoryMetaDTO (
    val id: Long,
    /**
     * List of all users that have, at some point in time, issued a command to this device.
     */
    val users: List<DeviceCmdHistoryUserDataDTO>,
)