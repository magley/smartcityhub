package rs.nvt.smartcityhub.property.dto

data class DenyReasonDTO(val reason: String)