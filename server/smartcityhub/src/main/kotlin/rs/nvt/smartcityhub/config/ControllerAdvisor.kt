package rs.nvt.smartcityhub.config

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import rs.nvt.smartcityhub.exception.EntityNotFoundException
import rs.nvt.smartcityhub.exception.RequestException
import rs.nvt.smartcityhub.util.ErrorDTO
import java.sql.SQLIntegrityConstraintViolationException


@ControllerAdvice
class ControllerAdvisor {

    private fun httpError(code: HttpStatus, message: String?): ResponseEntity<ErrorDTO> {
        return ResponseEntity(ErrorDTO(code, message ?: "Bad request"), code)
    }

    @ExceptionHandler(RequestException::class)
    fun requestException(e: RequestException): ResponseEntity<ErrorDTO> {
        return httpError(e.code, e.message)
    }

    @ExceptionHandler(SQLIntegrityConstraintViolationException::class)
    fun sqlIntegrity(e: SQLIntegrityConstraintViolationException): ResponseEntity<*> {
        return ResponseEntity(null, HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler(EntityNotFoundException::class)
    fun entityNotFound(e: EntityNotFoundException): ResponseEntity<*> {
        return httpError(HttpStatus.NOT_FOUND, e.message ?: "Resource not found")
    }
}