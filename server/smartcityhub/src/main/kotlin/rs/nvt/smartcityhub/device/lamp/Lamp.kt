package rs.nvt.smartcityhub.device.lamp

import jakarta.persistence.Entity
import rs.nvt.smartcityhub.device.Device
import rs.nvt.smartcityhub.device.dto.DeviceRegisterDTO
import rs.nvt.smartcityhub.device.DeviceType
import rs.nvt.smartcityhub.property.Property


enum class LampMode {
    Manual, Auto,
}

enum class LampCommandType {
    is_on,
    mode,
}

@Entity
class Lamp : Device {
    constructor() {
    }

    constructor(dto: DeviceRegisterDTO, property: Property) : super(dto, property) {
    }
    override fun getType(): DeviceType = DeviceType.lamp
}