package rs.nvt.smartcityhub.permission

import io.swagger.v3.oas.annotations.Operation
import jakarta.validation.Valid
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import rs.nvt.smartcityhub.config.auth.IAuthenticationFacade
import rs.nvt.smartcityhub.device.DeviceAccessControl
import rs.nvt.smartcityhub.device.IDeviceService
import rs.nvt.smartcityhub.permission.dto.NewPermissionDTO
import rs.nvt.smartcityhub.permission.dto.PermissionDTO
import rs.nvt.smartcityhub.permission.dto.PropertyPartialPermissionDTO
import rs.nvt.smartcityhub.permission.dto.RevokePermissionDTO
import rs.nvt.smartcityhub.property.IPropertyService
import rs.nvt.smartcityhub.property.PropertyAccessControl
import rs.nvt.smartcityhub.util.CollectionResultDTO

@RestController
@Validated
@RequestMapping("/api/permission")
class PermissionController {
    @Autowired lateinit var deviceService: IDeviceService
    @Autowired lateinit var propertyService: IPropertyService
    @Autowired lateinit var propertyAccessControl: PropertyAccessControl
    @Autowired lateinit var authFacade: IAuthenticationFacade
    @Autowired lateinit var deviceAccessControl: DeviceAccessControl
    @Autowired lateinit var permissionService: IPermissionService

    @Operation(summary = "[OBSOLETE] Get all permissions for this property")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @GetMapping("/property/{id}")
    fun getPropertyPermissions(@PathVariable id: Long): ResponseEntity<Collection<PermissionDTO>> {
        val property = propertyService.getApproved(id)
        propertyAccessControl.assertOwnership(authFacade.getUser(), property)
        val result = permissionService.getPropertyPermissions(property)
        return ResponseEntity(result, HttpStatus.OK)
    }

    @Operation(summary = "Get permissions for this property")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @GetMapping("/property/{id}/p")
    fun getPropertyPermissionsPaginated(@PathVariable id: Long, p: Pageable): ResponseEntity<CollectionResultDTO<PermissionDTO>> {
        val property = propertyService.getApproved(id)
        propertyAccessControl.assertOwnership(authFacade.getUser(), property)
        val result = permissionService.getPropertyPermissions(property, p)
        val totalCount = permissionService.getPropertyPermissionsCount(property)
        return ResponseEntity(CollectionResultDTO(totalCount, result), HttpStatus.OK)
    }

    @Operation(summary = "[OBSOLETE] Get all permissions for this device")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @GetMapping("/device/{id}")
    fun getDevicePermissions(@PathVariable id: Long): ResponseEntity<Collection<PermissionDTO>> {
        val device = deviceService.find(id)
        deviceAccessControl.assertOwnership(authFacade.getUser(), device)
        val result = permissionService.getDevicePermissions(device)
        return ResponseEntity(result, HttpStatus.OK)
    }

    @Operation(summary = "Get permissions for this device")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @GetMapping("/device/{id}/p")
    fun getDevicePermissionsPaginated(@PathVariable id: Long, p: Pageable): ResponseEntity<CollectionResultDTO<PermissionDTO>> {
        val device = deviceService.find(id)
        deviceAccessControl.assertOwnership(authFacade.getUser(), device)
        val result = permissionService.getDevicePermissions(device, p)
        val totalCount = permissionService.getDevicePermissionsCount(device)
        return ResponseEntity(CollectionResultDTO(totalCount, result), HttpStatus.OK)
    }

    @Operation(summary = "Grant new property permission")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PostMapping("/property")
    fun newPropertyPermission(@RequestBody dto: NewPermissionDTO): ResponseEntity<Void> {
        val property = propertyService.getApproved(dto.entityId)
        propertyAccessControl.assertDirectOwnership(authFacade.getUser(), property)
        permissionService.newPermission(dto)
        return ResponseEntity(null, HttpStatus.NO_CONTENT)
    }

    @Operation(summary = "Grant new device permission")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PostMapping("/device")
    fun newDevicePermission(@RequestBody dto: NewPermissionDTO): ResponseEntity<Void> {
        deviceAccessControl.assertDirectOwnership(authFacade.getUser(), deviceService.find(dto.entityId))
        permissionService.newPermission(dto)
        return ResponseEntity(null, HttpStatus.NO_CONTENT)
    }

    @Operation(summary = "Revoke property permission")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PutMapping("/property/revoke")
    fun revokePropertyPermission(@Valid @RequestBody dto: RevokePermissionDTO): ResponseEntity<Void> {
        val property = propertyService.getApproved(dto.entityId)
        propertyAccessControl.assertDirectOwnership(authFacade.getUser(), property)
        permissionService.revokePermission(dto)
        return ResponseEntity(null, HttpStatus.NO_CONTENT)
    }

    @Operation(summary = "Revoke device permission")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PutMapping("/device/revoke")
    fun revokeDevicePermission(@Valid @RequestBody dto: RevokePermissionDTO): ResponseEntity<Void> {
        deviceAccessControl.assertDirectOwnership(authFacade.getUser(), deviceService.find(dto.entityId))
        permissionService.revokePermission(dto)
        return ResponseEntity(null, HttpStatus.NO_CONTENT)
    }

    @Operation(summary = "Check if I have partial permissions for this property")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @GetMapping("/property/partial/{id}")
    fun getIfPropertyPartialPermission(@PathVariable id: Long): ResponseEntity<PropertyPartialPermissionDTO> {
        val property = propertyService.getApproved(id)
        val isPartial = propertyAccessControl.propertyHasPartialPermissions(authFacade.getUser(), property)
        val result = PropertyPartialPermissionDTO(isPartial)
        return ResponseEntity(result, HttpStatus.OK)
    }
}