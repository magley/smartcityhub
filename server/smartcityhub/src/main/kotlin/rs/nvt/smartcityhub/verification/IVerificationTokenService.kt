package rs.nvt.smartcityhub.verification

import rs.nvt.smartcityhub.user.User

interface IVerificationTokenService {
    fun findFor(user: User): VerificationToken
    fun findByToken(token: String): VerificationToken
    fun verify(token: VerificationToken)
    fun create(user: User): VerificationToken
}