package rs.nvt.smartcityhub.device.wash

import io.swagger.v3.oas.annotations.Operation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import rs.nvt.smartcityhub.config.auth.IAuthenticationFacade
import rs.nvt.smartcityhub.device.DeviceAccessControl
import rs.nvt.smartcityhub.device.DeviceType
import rs.nvt.smartcityhub.device.IDeviceService
import rs.nvt.smartcityhub.device.ac.ACDTO
import rs.nvt.smartcityhub.device.common.DeviceServiceCommon
import rs.nvt.smartcityhub.device.dto.DeviceCmdHistoryDTO
import rs.nvt.smartcityhub.device.dto.DeviceCmdHistoryMetaDTO
import rs.nvt.smartcityhub.device.dto.DeviceCmdHistoryRequestBodyDTO
import rs.nvt.smartcityhub.device.wash.dto.WashMachineDTO
import rs.nvt.smartcityhub.device.wash.dto.WashMachineRegisterDTO
import rs.nvt.smartcityhub.device.wash.dto.cmd.WashMachineCmdModeDTO
import rs.nvt.smartcityhub.device.wash.dto.cmd.WashMachineCmdScheduleDTO
import rs.nvt.smartcityhub.device.wash.dto.cmd_history.WashMachineCmdHistoryItemDTO_
import rs.nvt.smartcityhub.property.IPropertyService
import rs.nvt.smartcityhub.property.PropertyAccessControl

@RestController
@RequestMapping("/api/device/wash")
class WashMachineController {
    @Autowired lateinit var deviceService: IDeviceService
    @Autowired lateinit var propertyService: IPropertyService
    @Autowired lateinit var propertyAccessControl: PropertyAccessControl
    @Autowired lateinit var authFacade: IAuthenticationFacade
    @Autowired lateinit var deviceAccessControl: DeviceAccessControl
    @Autowired lateinit var washMachineService: WashMachineService
    @Autowired lateinit var cmdService: DeviceServiceCommon

    @Operation(summary = "Register a new washing machine")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PostMapping()
    fun register(@RequestBody dto: WashMachineRegisterDTO): ResponseEntity<Void> {
        val property = propertyService.getApproved(dto.base.propertyId)
        propertyAccessControl.assertOwnership(authFacade.getUser(), property)
        washMachineService.registerWashMachine(dto, property)
        return ResponseEntity(null, HttpStatus.NO_CONTENT)
    }

    @Operation(summary = "Get washing machine by ID")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @GetMapping("/{id}")
    fun get(@PathVariable id: Long): ResponseEntity<WashMachineDTO> {
        val device = getDeviceWithAssertion(id)
        val result = WashMachineDTO(device)
        return ResponseEntity(result, HttpStatus.OK)
    }

    @Operation(summary = "Set washing machine mode")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PutMapping("/mode")
    fun setMode(@RequestBody dto: WashMachineCmdModeDTO): ResponseEntity<Void> {
        val device = getDeviceWithAssertion(dto.id)
        washMachineService.setMode(device, dto)
        return ResponseEntity(null, HttpStatus.NO_CONTENT)
    }

    @Operation(summary = "Schedule a washing machine cycle")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PutMapping("/schedule")
    fun schedule(@RequestBody dto: WashMachineCmdScheduleDTO): ResponseEntity<Void> {
        val device = getDeviceWithAssertion(dto.id)
        washMachineService.schedule(device, dto)
        return ResponseEntity(null, HttpStatus.NO_CONTENT)
    }

    @Operation(summary = "Remove a scheduled washing machine cycle")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PutMapping("/remove-schedule")
    fun removeSchedule(@RequestBody dto: WashMachineCmdScheduleDTO): ResponseEntity<Void> {
        val device = getDeviceWithAssertion(dto.id)
        washMachineService.removeSchedule(device, dto)
        return ResponseEntity(null, HttpStatus.NO_CONTENT)
    }

    @Operation(summary = "Get history of actions for this washing machine")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PostMapping("/history")
    fun getHistory(@RequestBody dto: DeviceCmdHistoryRequestBodyDTO): ResponseEntity<DeviceCmdHistoryDTO<WashMachineCmdHistoryItemDTO_>> {
        val device = getDeviceWithAssertion(dto.id)
        val result = washMachineService.getHistory(device, dto)
        return ResponseEntity(result, HttpStatus.OK)
    }

    @Operation(summary = "Get incomplete scheduled cycled scheduled N days ago")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @GetMapping("/scheduled/{id}/{days}")
    fun getRelevantScheduledTasks(@PathVariable id: Long, @PathVariable days: Long): ResponseEntity<Collection<WashMachineCmdScheduleDTO>> {
        val device = getDeviceWithAssertion(id)
        val result = washMachineService.getRelevantScheduledTasks(device, days)
        return ResponseEntity(result, HttpStatus.OK)
    }

    private fun getDeviceWithAssertion(id: Long): WashMachine {
        val device = deviceService.find(id)
        deviceAccessControl.assertOwnership(authFacade.getUser(), device)
        deviceAccessControl.assertType(device, DeviceType.wash)
        return device as WashMachine
    }
}