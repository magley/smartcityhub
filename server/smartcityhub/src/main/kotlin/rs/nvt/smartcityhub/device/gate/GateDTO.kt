package rs.nvt.smartcityhub.device.gate

import com.influxdb.annotations.Column
import rs.nvt.smartcityhub.device.dto.*
import java.time.Instant

data class GateRegisterDTO (
    val base: DeviceRegisterDTO,
    val plates: List<String>,
)

data class GateDTO (val base: DeviceDTO, val plates: List<String>) {
    constructor(device: Gate) : this(DeviceDTO(device), device.plates)
}

/**
 * `GateCmdHistoryItemDTO` is a single command stored in the time-series database.
 */
class GateCmdHistoryItemDTO {
    @Column(name = "_time", timestamp = true) lateinit var timestamp: Instant
    @Column(name = "user_id") var userId: Long = -1
    @Column(name = "type", tag = true) lateinit var type: GateCommandType
    @Column(name = "uuid") lateinit var uuid: String
    @Column(name = "status", tag = true) var status: Boolean = false
    @Column(name = "open_state", tag = true) var openState: GateOpen? = null
    @Column(name = "mode", tag = true) var mode: GateMode? = null
    @Column(name = "override", tag = true) var override: GateOverride? = null
    @Column(name = "plate", tag = true) var plate: String? = null
    @Column(name = "direction", tag = true) var direction: GateDirection? = null
    @Column(name = "event", tag = true) var event: GateSensorEvent? = null
}

typealias GateCmdMqttDTO = DeviceCmdMqttDTO<GateCommandType>

data class GateCmdModeDTO (
    val id: Long,
    val uuid: String,
    val mode: GateMode,
)

data class GateCmdStartDTO (
    val id: Long,
    val uuid: String,
    val open: GateOpen
)

data class GateCmdOverrideDTO (
    val id: Long,
    val uuid: String,
    val override: GateOverride,
)