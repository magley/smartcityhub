package rs.nvt.smartcityhub.city

import jakarta.persistence.*
import rs.nvt.smartcityhub.country.Country

@Entity
@Table(name = "cities")
class City {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0

    @Column(nullable = false)
    lateinit var name: String

    @ManyToOne(optional = false)
    lateinit var country: Country
}