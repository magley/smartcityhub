package rs.nvt.smartcityhub.exception

import org.springframework.http.HttpStatus

class RequestException(val code: HttpStatus, msg: String) : Exception(msg)