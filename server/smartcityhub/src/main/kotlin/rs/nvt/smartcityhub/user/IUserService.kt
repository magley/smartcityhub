package rs.nvt.smartcityhub.user

import org.springframework.data.domain.Pageable
import org.springframework.security.core.userdetails.UserDetailsService
import rs.nvt.smartcityhub.user.dto.ClientRegisterDTO
import rs.nvt.smartcityhub.user.dto.CreateUserDTO
import rs.nvt.smartcityhub.user.dto.PasswordChangeDTO

interface IUserService : UserDetailsService {
    fun findAll(): List<User>
    fun findByRoleAndIdIn(role: User.Role, ids: List<Long>): List<User>
    fun findByRoleAndIdBetween(role: User.Role, idFrom: Long, idTo: Long): List<User>
    // NOTE: This ignores unverified accounts.
    fun findAllClients(): List<User>
    fun findAllAdmins(): List<User>
    fun findAllAdmins(p: Pageable): List<User>
    fun countAllAdmins(): Long
    fun find(userId: Long): User?
    fun findByEmail(email: String): User?
    fun get(id: Long): User
    fun remove(id: Long)
    fun registerAdmin(dto: CreateUserDTO)
    fun registerClient(dto: ClientRegisterDTO)
    fun createSuperAdmin(): String
    fun activateUser(user: User)
    fun shouldCreateSuperAdmin(): Boolean
    fun changePassword(user: User, dto: PasswordChangeDTO)
    fun LOADTEST_createDummyUsers(n: Int)
    abstract fun LOADTEST_createAdmins(i: Int)
    fun addToElasicSearch(user: User)
    fun getClientSearch(searchFullName: String): List<User>
}