package rs.nvt.smartcityhub.device.wash.dto.cmd

import rs.nvt.smartcityhub.device.wash.WashMachineMode
import java.time.Instant

data class WashMachineCmdScheduleDTO(
    val id: Long,
    val uuid: String,
    val mode: WashMachineMode,
    val time: Instant,
)