package rs.nvt.smartcityhub.property

import jakarta.persistence.Embeddable
import jakarta.validation.constraints.Max
import jakarta.validation.constraints.Min
import jakarta.validation.constraints.NotNull

@Embeddable
class GeoCoordinate {
    @NotNull
    @Min(-90)
    @Max(90)
    var latitude: Double = 0.0

    @NotNull
    @Min(-180)
    @Max(180)
    var longitude: Double = 0.0
}