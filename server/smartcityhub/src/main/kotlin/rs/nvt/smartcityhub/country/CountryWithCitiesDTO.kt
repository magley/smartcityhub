package rs.nvt.smartcityhub.country

import rs.nvt.smartcityhub.city.CityDTO

data class CountryWithCitiesDTO(val id: Long, val name: String, val cities: List<CityDTO>) {
    constructor(country: Country): this(country.id, country.name, country.cities.map { c -> CityDTO(c) })
}