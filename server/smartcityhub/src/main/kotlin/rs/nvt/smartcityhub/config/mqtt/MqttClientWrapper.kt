package rs.nvt.smartcityhub.config.mqtt

import jakarta.annotation.PostConstruct
import org.eclipse.paho.mqttv5.client.*
import org.eclipse.paho.mqttv5.client.persist.MemoryPersistence
import org.eclipse.paho.mqttv5.common.MqttException
import org.eclipse.paho.mqttv5.common.MqttMessage
import org.eclipse.paho.mqttv5.common.packet.MqttProperties
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.Marker
import org.slf4j.MarkerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.nio.charset.Charset
import java.util.*
import kotlin.concurrent.thread

const val TOPIC_PING = "p/+/d/ping"
const val TOPIC_DHT_READ = "dht/+/r"
const val CONNECT_RETRY_DELAY_IN_S = 10

@Component
class MqttClientWrapper : MqttCallback {
    @Value("\${mqtt.host}") lateinit var mqttHost: String
    @Value("\${mqtt.port}") lateinit var mqttPort: String
    @Value("\${mqtt.username}") lateinit var mqttUsername: String
    @Value("\${mqtt.password}") lateinit var mqttPassword: String
    val clientUID: String = UUID.randomUUID().toString()
    var logger: Logger = LoggerFactory.getLogger(MqttClientWrapper::class.java)
    var mqttMarker: Marker = MarkerFactory.getMarker("MQTT")
    var connected = false
    lateinit var options: MqttConnectionOptions
    lateinit var client: IMqttClient
    @PostConstruct
    fun postConstruct() {
        options = MqttConnectionOptions()
        options.userName = mqttUsername
        options.password = mqttPassword.encodeToByteArray()
        options.isAutomaticReconnect = true
        options.isCleanStart = false

        client = MqttClient("tcp://${mqttHost}:${mqttPort}", clientUID, MemoryPersistence())
        client.setCallback(this)

        // Thread only used initially for when broker is not running to begin with -- afterward the client knows how to
        // reconnect by itself through the connectComplete() and disconnected() callbacks
        thread(start = true) {
            while (!connected) {
                try {
                    client.connect(options)
                } catch (_: MqttException) {
                    logger.warn(mqttMarker, "Failed initial connect to MQTT broker. Trying again in $CONNECT_RETRY_DELAY_IN_S seconds...")
                    Thread.sleep(CONNECT_RETRY_DELAY_IN_S * 1000L)
                }
            }
        }
    }

    fun publish(topic: String, message: MqttMessage) {
        if (connected) {
            client.publish(topic, message)
        }
    }

    override fun connectComplete(reconnect: Boolean, serverURI: String?) {
        connected = true
        logger.info(mqttMarker, "Connected to ${serverURI}, reconnect=${reconnect}")
    }

    override fun disconnected(disconnectResponse: MqttDisconnectResponse?) {
        connected = false
        logger.info(mqttMarker, "Disconnected with code ${disconnectResponse?.returnCode}, for reason ${disconnectResponse?.reasonString}")
    }

    override fun messageArrived(topic: String?, message: MqttMessage?) {
        logger.info(mqttMarker, "Message sent to null topic; ignoring")
    }

    override fun deliveryComplete(token: IMqttToken?) {
        logger.info(mqttMarker, "Done delivery of message ${token?.requestMessage?.payload?.toString(Charset.defaultCharset())} to ${token?.topics?.get(0).toString()}")
    }

    override fun mqttErrorOccurred(exception: MqttException?) {
        logger.error(mqttMarker, "${exception?.message}")
    }

    override fun authPacketArrived(reasonCode: Int, properties: MqttProperties?) {
        logger.info(mqttMarker, "Auth packet: ${reasonCode}, ${properties?.authenticationData} with method ${properties?.authenticationMethod}")
    }
}