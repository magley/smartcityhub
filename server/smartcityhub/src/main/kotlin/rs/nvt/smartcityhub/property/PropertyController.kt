package rs.nvt.smartcityhub.property


import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import rs.nvt.smartcityhub.city.ICityService
import rs.nvt.smartcityhub.config.auth.IAuthenticationFacade
import rs.nvt.smartcityhub.device.IDeviceService
import rs.nvt.smartcityhub.permission.IPermissionService
import rs.nvt.smartcityhub.property.dto.*
import rs.nvt.smartcityhub.util.CollectionResultDTO
import rs.nvt.smartcityhub.util.ImageSaveMode
import rs.nvt.smartcityhub.util.saveBase64ToImage

@RestController
@RequestMapping("/api/property")
class PropertyController {
    @Autowired lateinit var propertyService: IPropertyService
    @Autowired lateinit var authFacade: IAuthenticationFacade
    @Autowired lateinit var deviceService: IDeviceService
    @Autowired lateinit var propertyAccessControl: PropertyAccessControl
    @Autowired lateinit var cityService: ICityService
    @Autowired lateinit var permissionService: IPermissionService

    @Operation(summary = "Get properties that are shared with me")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @GetMapping("/user/shared")
    fun getAllSharedWithMe(p: Pageable): ResponseEntity<Collection<PropertyDTO>> {
        val user = authFacade.getUser()
        // 2 different lookups with pageable => there will be at most 2 * count elements.
        val shared = permissionService.getPropertyPermissions(user, p).map { p -> p.property }
        val sharedThroughDevice = permissionService.getDevicePermissions(user, p).map { p -> p.device.property }
        val result = shared + sharedThroughDevice
        return ResponseEntity(result.toSet().map { p -> PropertyDTO(p, user) }, HttpStatus.OK)
    }

    @Operation(summary = "Get approved properties I own")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @GetMapping("/user/owned/approved/paged")
    fun getDirectlyOwnedApproved(pageable: Pageable): ResponseEntity<Collection<PropertyDTO>> {
        val user = authFacade.getUser()
        val result = propertyService.findApprovedByOwnerId(user.id, pageable).map { p -> PropertyDTO(p, user) }
        return ResponseEntity(result, HttpStatus.OK)
    }

    @Operation(summary = "Get my properties waiting for approval")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @GetMapping("/user/owned/pending/paged")
    fun getDirectlyOwnedPending(pageable: Pageable): ResponseEntity<Collection<PropertyDTO>> {
        val user = authFacade.getUser()
        val result = propertyService.findPendingByOwnerId(user.id, pageable).map { p -> PropertyDTO(p, user) }
        return ResponseEntity(result, HttpStatus.OK)
    }

    @Operation(summary = "[OBSOLETE] Get all approved properties I own")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @GetMapping("/user/owned/approved")
    fun getAllApproved(): ResponseEntity<Collection<PropertyDTO>> {
        val user = authFacade.getUser()
        val result = propertyService.findAllApprovedByOwnerId(user.id).map { p -> PropertyDTO(p, user) }
        return ResponseEntity(result, HttpStatus.OK)
    }

    @Operation(summary = "Get approved property by ID")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @GetMapping("/user/approved/{propertyId}")
    fun getApprovedProperty(@PathVariable propertyId: Long): ResponseEntity<PropertyDTO> {
        val user = authFacade.getUser()
        val result = PropertyDTO(propertyService.getApproved(propertyId), user)
        return ResponseEntity(result, HttpStatus.OK)
    }

    @Operation(summary = "Get properties I have access to")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @GetMapping("/user/own") // TODO: Misleading name?
    fun getPropertiesIHaveAccessTo(): ResponseEntity<List<PropertyDTO>> {
        val user = authFacade.getUser()
        val owned = propertyService.findAllByOwnerId(user.id)
        val shared = permissionService.getPropertyPermissions(user).map { p -> p.property }
        val sharedThroughDevice = permissionService.getDevicePermissions(user).map { p -> p.device.property }
        val result = owned + shared + sharedThroughDevice
        return ResponseEntity(result.toSet().map { p -> PropertyDTO(p, user) }, HttpStatus.OK)
    }

    @Operation(summary = "Get property by ID")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @GetMapping("{propertyId}/basic")
    fun getProperty(@PathVariable propertyId: Long): ResponseEntity<PropertyDTO> {
        val user = authFacade.getUser()
        val property = propertyService.get(propertyId)
        propertyAccessControl.assertPropertyPermissionIncludingPermissionsByDevice(user, property)

        val propertyDTO = PropertyDTO(property, user)

        return ResponseEntity(
            propertyDTO,
            HttpStatus.OK
        )
    }

    // TODO: It would be great if we got rid of this method altogether. Use getPropertyDevicesPaginated.
    @Operation(summary = "[OBSOLETE] Get all devices of a property")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @GetMapping("{propertyId}")
    fun getPropertyWithDevicesFull(@PathVariable propertyId: Long): ResponseEntity<PropertyWithDevicesDTO> {
        val user = authFacade.getUser()
        val property = propertyService.get(propertyId)
        propertyAccessControl.assertPropertyPermissionIncludingPermissionsByDevice(user, property)
        var devices = deviceService.findDevicesByPropertyId(property.id)

        // Show only devices you have permissions for.
        // If you own the property, that's everything.
        // If you are granted permission for the property, that's everything.
        // Otherwise, filter out devices you don't have permissions for.
        //
        // TODO: This is wasteful because of all the db queries.
        if (propertyAccessControl.propertyHasPartialPermissions(user, property)) {
            val devicesIHavePermissionFor = permissionService.getDevicePermissions(user).map { p -> p.device.id }
            devices = devices.filter { dev -> devicesIHavePermissionFor.contains(dev.id) }
        }

        val propertyWithDevicesDTO = PropertyWithDevicesDTO(property, devices, user)

        return ResponseEntity(
            propertyWithDevicesDTO,
            HttpStatus.OK
        )
    }

    @Operation(summary = "Get devices of a property")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @GetMapping("{propertyId}/devices")
    fun getPropertyDevicesPaginated(@PathVariable propertyId: Long, p: Pageable): ResponseEntity<PropertyWithDevicesDTO> {
        val user = authFacade.getUser()
        val property = propertyService.get(propertyId)
        propertyAccessControl.assertPropertyPermissionIncludingPermissionsByDevice(user, property)
        var devices = deviceService.findDevicesByPropertyId(property.id, p)

        // TODO: This is wasteful because of all the db queries.
        if (propertyAccessControl.propertyHasPartialPermissions(user, property)) {
            val devicesIHavePermissionFor = permissionService.getDevicePermissions(user).map { p -> p.device.id }
            devices = devices.filter { dev -> devicesIHavePermissionFor.contains(dev.id) }
        }

        val propertyWithDevicesDTO = PropertyWithDevicesDTO(property, devices, user)

        return ResponseEntity(
            propertyWithDevicesDTO,
            HttpStatus.OK
        )
    }

    @Operation(summary = "Submit a request for a new property")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PostMapping("/request")
    fun requestProperty(@RequestBody dto: RequestPropertyDTO): ResponseEntity<Object> {
        val owner = authFacade.getUser()
        val city = cityService.findById(dto.cityId)
        val property = propertyService.createProperty(Property(owner, city, dto))
        saveBase64ToImage(dto.imageB64, "property_${property.id}", ImageSaveMode.PHOTO)
        return ResponseEntity(HttpStatus.NO_CONTENT)
    }

    @Operation(summary = "[OBSOLETE] Get all pending properties")
    @PreAuthorize("hasAnyRole('SUPER_ADMIN', 'ADMIN')")
    @GetMapping("/pending")
    fun getAllPending(): List<PropertyWithOwnerDTO> {
        return propertyService.findAllPending().map { pr -> PropertyWithOwnerDTO(pr) }
    }

    @Operation(summary = "Get pending properties")
    @PreAuthorize("hasAnyRole('SUPER_ADMIN', 'ADMIN')")
    @GetMapping("/pending/p")
    fun getPendingPaginated(p: Pageable): ResponseEntity<CollectionResultDTO<PropertyWithOwnerDTO>> {
        val result = propertyService.findAllPending(p).map { pr -> PropertyWithOwnerDTO(pr) }
        val totalCount = propertyService.countAllPending()
        return ResponseEntity(CollectionResultDTO(totalCount, result), HttpStatus.OK)
    }

    @Operation(summary = "Get pending property by ID")
    @PreAuthorize("hasAnyRole('SUPER_ADMIN', 'ADMIN')")
    @GetMapping("/pending/{propertyId}")
    fun getPending(@PathVariable propertyId: Long): ResponseEntity<PropertyWithOwnerDTO> {
        return ResponseEntity(PropertyWithOwnerDTO(propertyService.getPending(propertyId)), HttpStatus.OK)
    }

    @Operation(summary = "Deny request for a property")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    @PutMapping("/admin/deny/{propertyId}")
    fun denyProperty(@PathVariable propertyId: Long, @RequestBody reason: DenyReasonDTO): ResponseEntity<Void> {
        propertyService.deny(propertyId, reason.reason)
        return ResponseEntity.noContent().build()
    }

    @Operation(summary = "Approve request for a property")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    @PutMapping("/admin/approve/{propertyId}")
    fun approveProperty(@PathVariable propertyId: Long): ResponseEntity<Void> {
        propertyService.approve(propertyId)
        return ResponseEntity.noContent().build()
    }
}