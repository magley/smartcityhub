package rs.nvt.smartcityhub.device.ac.dto.cmd

import rs.nvt.smartcityhub.device.ac.ACCommandType
import rs.nvt.smartcityhub.device.dto.DeviceCmdMqttDTO

typealias ACCmdMqttDTO = DeviceCmdMqttDTO<ACCommandType>