package rs.nvt.smartcityhub.device.dto

import java.time.Instant

open class DeviceCmdHistoryBaseDTO {
    lateinit var timestamp: Instant
    lateinit var user: DeviceCmdHistoryUserDataDTO
    var status: Boolean = false
    lateinit var uuid: String
}