package rs.nvt.smartcityhub.device.derived

import jakarta.persistence.Entity
import rs.nvt.smartcityhub.device.Device
import rs.nvt.smartcityhub.device.dto.DeviceRegisterDTO
import rs.nvt.smartcityhub.device.DeviceType
import rs.nvt.smartcityhub.property.Property

@Entity
class Battery : Device {
    constructor() {
    }

    constructor(dto: DeviceRegisterDTO, property: Property) : super(dto, property) {
    }

    override fun getType(): DeviceType = DeviceType.battery
}