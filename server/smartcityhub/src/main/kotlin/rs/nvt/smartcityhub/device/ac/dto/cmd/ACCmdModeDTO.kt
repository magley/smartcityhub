package rs.nvt.smartcityhub.device.ac.dto.cmd

import rs.nvt.smartcityhub.device.ac.ACMode

/**
 * `ACCmdModeDTO` is the HTTP request payload for a "change current mode" command.
 */
data class ACCmdModeDTO (
    val id: Long,
    val uuid: String,
    val mode: ACMode
)