package rs.nvt.smartcityhub.device.dht.dto

import com.influxdb.annotations.Column
import com.influxdb.annotations.Measurement
import java.time.Instant

@Measurement(name = "dht")
class DHTHistoryItemMeasurementDTO {
    @Column(name= "_measurement", measurement = true) val measurement: String = ""
    @Column(name = "_time", timestamp = true) lateinit var timestamp: Instant
    @Column(name = "device_id", tag = true) var deviceId: String = ""
    @Column(name = "temperature") var temperature: Double = 0.0
    @Column(name = "humidity") var humidity: Double = 0.0
}