package rs.nvt.smartcityhub.user

import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface IUserRepo : JpaRepository<User, Long> {
    fun findByRoleAndActivatedTrue(role: User.Role): List<User>
    fun findByRoleAndIdIn(role: User.Role, ids: List<Long>): List<User>
    fun findByRoleAndActivatedTrue(role: User.Role, pageable: Pageable): List<User>
    fun countByRoleAndActivatedTrue(role: User.Role): Long
    fun findByEmail(username: String): Optional<User>
    fun findByRoleAndIdBetween(role: User.Role, from: Long, to: Long): List<User>
}