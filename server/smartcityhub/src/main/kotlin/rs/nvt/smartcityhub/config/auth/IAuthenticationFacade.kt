package rs.nvt.smartcityhub.config.auth

import org.springframework.security.core.Authentication
import rs.nvt.smartcityhub.user.User

interface IAuthenticationFacade {
    fun getAuthentication(): Authentication
    fun getUser(): User
}