package rs.nvt.smartcityhub.permission

import jakarta.persistence.*
import rs.nvt.smartcityhub.country.Country
import rs.nvt.smartcityhub.property.Property
import rs.nvt.smartcityhub.user.User

@Entity
@Table(name = "property_permissions")
class PropertyPermission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0

    @ManyToOne
    lateinit var property: Property

    @ManyToOne
    lateinit var user: User
}