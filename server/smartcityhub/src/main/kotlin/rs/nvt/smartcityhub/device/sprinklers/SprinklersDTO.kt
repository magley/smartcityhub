package rs.nvt.smartcityhub.device.sprinklers

import com.influxdb.annotations.Column
import rs.nvt.smartcityhub.device.dto.*
import java.time.Instant

/**
 * `SprinklersCmdHistoryItemDTO` is a single command stored in the time-series database.
 */
class SprinklersCmdHistoryItemDTO {
    @Column(name = "_time", timestamp = true) lateinit var timestamp: Instant
    @Column(name = "user_id") var userId: Long = -1
    @Column(name = "type", tag = true) lateinit var type: SprinklersCommandType
    @Column(name = "uuid") lateinit var uuid: String
    @Column(name = "status", tag = true) var status: Boolean = false
    @Column(name = "is_on", tag = true) var isOn: Boolean? = null
    @Column(name = "triggers") var triggers: String? = null
}

typealias SprinklersCmdMqttDTO = DeviceCmdMqttDTO<SprinklersCommandType>

data class SprinklersCmdStartDTO (
    val id: Long,
    val uuid: String,
    val start: Boolean
)

data class SprinklersCmdTriggersDTO (
    val id: Long,
    val uuid: String,
    val triggers: List<SprinklersTriggerDTO>
)

data class SprinklersTriggerDTO (
    val hour: Long,
    val minute: Long,
    val days: List<Long>,
    val isOn: Boolean,
)
