package rs.nvt.smartcityhub.device.ac.dto.cmd

/**
 * `ACCmdStartDTO` is the HTTP request payload for a "turn on/off" command.
 */
data class ACCmdStartDTO (
    val id: Long,
    val uuid: String,
    val start: Boolean
)