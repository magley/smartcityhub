package rs.nvt.smartcityhub.property.dto

import rs.nvt.smartcityhub.permission.dto.ShareDTO
import rs.nvt.smartcityhub.property.GeoCoordinate
import rs.nvt.smartcityhub.property.Property
import rs.nvt.smartcityhub.property.PropertyAccessControl
import rs.nvt.smartcityhub.property.Status
import rs.nvt.smartcityhub.user.User

data class PropertyDTO(
    val id: Long,
    val type: Property.Type,
    val address: String,
    val cityName: String,
    val countryName: String,
    val size: Double,
    val floors: Int,
    val location: GeoCoordinate,
    val status: Status,
    var share: ShareDTO
) {
    constructor(p: Property, requestSender: User?) : this(
        p.id,
        p.type,
        p.address,
        p.city.name,
        p.city.country.name,
        p.size,
        p.floors,
        p.location,
        p.status,
        ShareDTO(p.owner.id != requestSender?.id, p.owner.name, p.owner.lastName)
    )
}