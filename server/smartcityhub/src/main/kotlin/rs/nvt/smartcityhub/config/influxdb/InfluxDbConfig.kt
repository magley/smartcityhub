package rs.nvt.smartcityhub.config.influxdb

import com.influxdb.client.InfluxDBClient
import com.influxdb.client.InfluxDBClientFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.io.File

@Configuration
class InfluxDbConfig {
    private final var env: MutableMap<String, String> = HashMap()

    fun getBucket(): String? = env[BUCKET_NVT]

    companion object {
        private const val URL = "URL"
        private const val ORGANIZATION = "ORGANIZATION"
        private const val TOKEN = "TOKEN"
        private const val BUCKET_NVT = "BUCKET_NVT"

        private const val ENV_LOCATION = "./config/influxdb.env"

        const val BUCKET_NVT_VAL = "nvtBucket" // ugh
        const val BUCKET_NVT_DOWNSAMPELD = "nvtDownsampling"
    }

    init {
        val allLines = File(ENV_LOCATION).useLines { it.toList() }
        for (line in allLines) {
            val (key, value) = line.split("=", limit = 2)
            env[key] = value
        }

        assert(env[BUCKET_NVT] == BUCKET_NVT_VAL)
    }

    @Bean
    fun getPingClient(): InfluxDBClient {
        val url = env[URL] as String
        val token = (env[TOKEN] as String).toCharArray()
        val org = env[ORGANIZATION]
        val bucket = env[BUCKET_NVT]

        return InfluxDBClientFactory.create(url, token, org, bucket)
    }
}