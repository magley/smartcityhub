package rs.nvt.smartcityhub.config.auth

import io.swagger.v3.oas.annotations.Operation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/auth")
class AuthController {
    @Autowired
    lateinit var authenticationFacade: IAuthenticationFacade

    @Operation(summary = "[INTERNAL USE] Authenticate user")
    @GetMapping
    fun auth(): ResponseEntity<Void> {
        try {
            authenticationFacade.getUser()
            return ResponseEntity.ok(null)
        }
        catch (e: Exception) {
            e.printStackTrace()
            return ResponseEntity(null, HttpStatus.UNAUTHORIZED)
        }
    }
}