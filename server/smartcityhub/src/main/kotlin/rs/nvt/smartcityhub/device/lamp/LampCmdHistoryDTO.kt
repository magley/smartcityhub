package rs.nvt.smartcityhub.device.lamp

import rs.nvt.smartcityhub.device.dto.DeviceCmdHistoryBaseDTO
import rs.nvt.smartcityhub.device.dto.DeviceCmdHistoryUserDataDTO

data class LampCmdHistoryOneOfDTO (
    var isOn: Boolean?,
    var mode: LampMode?,
    var threshold: Double?,
)

class LampCmdHistoryItemDTO_ : DeviceCmdHistoryBaseDTO {
    var type: LampCommandType
    var oneOf: LampCmdHistoryOneOfDTO

    constructor(dto: LampCmdHistoryItemDTO, u: DeviceCmdHistoryUserDataDTO) {
        timestamp = dto.timestamp
        status = dto.status
        uuid = dto.uuid
        user = u

        type = dto.type
        oneOf = LampCmdHistoryOneOfDTO(dto.isOn, dto.mode, dto.threshold)
    }
}