package rs.nvt.smartcityhub.device.wash.dto.cmd_history

import rs.nvt.smartcityhub.device.ac.ACCommandType
import rs.nvt.smartcityhub.device.ac.ACMode
import rs.nvt.smartcityhub.device.dto.DeviceCmdHistoryBaseDTO
import rs.nvt.smartcityhub.device.dto.DeviceCmdHistoryUserDataDTO
import rs.nvt.smartcityhub.device.wash.WashMachine
import rs.nvt.smartcityhub.device.wash.WashMachineCommandType
import rs.nvt.smartcityhub.device.wash.WashMachineMode

data class WashMachineCmdHistoryOneOf_ScheduledDTO (
    var mode: WashMachineMode?,
    var timestamp: Long?
)

data class WashMachineCmdHistoryOneOfDTO (
    var mode: WashMachineMode?,
    var scheduled: WashMachineCmdHistoryOneOf_ScheduledDTO?,
)

 class WashMachineCmdHistoryItemDTO_ : DeviceCmdHistoryBaseDTO {
    var type: WashMachineCommandType
    var oneOf: WashMachineCmdHistoryOneOfDTO

    constructor(dto: WashMachineCmdHistoryItemDTO, u: DeviceCmdHistoryUserDataDTO) {
        timestamp = dto.timestamp
        status = dto.status
        uuid = dto.uuid
        user = u

        type = dto.type
        oneOf = WashMachineCmdHistoryOneOfDTO(dto.mode, WashMachineCmdHistoryOneOf_ScheduledDTO(dto.mode, dto.startTime))
    }
}