package rs.nvt.smartcityhub.config.auth

import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import rs.nvt.smartcityhub.user.User

@Component
class AuthenticationFacade : IAuthenticationFacade {
    override fun getAuthentication(): Authentication {
        return SecurityContextHolder.getContext().authentication
    }

    override fun getUser(): User {
        return SecurityContextHolder.getContext().authentication.principal as User
    }
}