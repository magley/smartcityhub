package rs.nvt.smartcityhub.country

import io.swagger.v3.oas.annotations.Operation
import jakarta.annotation.security.PermitAll
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/country")
class CountryController {
    @Autowired
    lateinit var countryService: ICountryService

    @Operation(summary = "Get all countries")
    @PermitAll
    @GetMapping("/all")
    fun findAll(): List<CountryWithCitiesDTO> {
        return countryService.findAll().map { c -> CountryWithCitiesDTO(c) }
    }
}