package rs.nvt.smartcityhub.device.dto

/**
 * `DeviceCmdMqttDTO` is the payload sent to the MQTT broker when a command is issued.
 *
 * **This does not go in the time-series database, instead it is processed by the simulator.**
 */
data class DeviceCmdMqttDTO<Type> (
    val userId: Long,
    val type: Type,
    /**
     * Value of the command, depends on `type`.
     */
    val value: Any,
    val uuid: String
)