package rs.nvt.smartcityhub.device.ac

import com.fasterxml.jackson.databind.ObjectMapper
import org.eclipse.paho.mqttv5.common.MqttMessage
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import rs.nvt.smartcityhub.config.auth.IAuthenticationFacade
import rs.nvt.smartcityhub.config.mqtt.MqttClientWrapper
import rs.nvt.smartcityhub.device.IDeviceRepo
import rs.nvt.smartcityhub.device.ac.dto.ACCustomModeItemDTO
import rs.nvt.smartcityhub.device.ac.dto.ACRegisterDTO
import rs.nvt.smartcityhub.device.ac.dto.cmd.ACCmdMqttDTO
import rs.nvt.smartcityhub.device.ac.dto.cmd_history.ACCmdHistoryItemDTO
import rs.nvt.smartcityhub.device.ac.dto.cmd_history.ACCmdHistoryItemDTO_
import rs.nvt.smartcityhub.device.common.DeviceServiceCommon
import rs.nvt.smartcityhub.device.dto.*
import rs.nvt.smartcityhub.property.Property
import rs.nvt.smartcityhub.user.IUserService
import rs.nvt.smartcityhub.user.User
import rs.nvt.smartcityhub.util.ImageSaveMode
import rs.nvt.smartcityhub.util.saveBase64ToImage

@Service
class ACService {
    @Autowired lateinit var mqttClient: MqttClientWrapper
    @Autowired lateinit var deviceRepo: IDeviceRepo
    @Autowired lateinit var authFacade: IAuthenticationFacade
    @Autowired lateinit var objectMapper: ObjectMapper
    @Autowired lateinit var cmdService: DeviceServiceCommon
    @Autowired lateinit var userService: IUserService

    fun registerAC(dto: ACRegisterDTO, property: Property): AC {
        val device = AC(dto.base, property)
        device.minTemperature = dto.minTemperature
        device.maxTemperature = dto.maxTemperature
        device.supportedModes = dto.supportedModes
        deviceRepo.save(device)
        saveBase64ToImage(dto.base.imageB64, "device_${device.id}", ImageSaveMode.PHOTO)
        return device
    }

    fun setConfig(device: AC, config: List<ACCustomModeItemDTO>, uuid: String) {
        publishCommand(device, ACCommandType.config, config, uuid)
    }

    fun start(device: AC, uuid: String) {
        publishCommand(device, ACCommandType.is_on, true, uuid)
    }

    fun stop(device: AC, uuid: String) {
        publishCommand(device, ACCommandType.is_on, false, uuid)
    }

    fun setTemperature(device: AC, temperature: Double, uuid: String) {
        publishCommand(device, ACCommandType.temperature, temperature, uuid)
    }

    fun setMode(device: AC, mode: ACMode, uuid: String) {
        publishCommand(device, ACCommandType.mode, mode, uuid)
    }

    private fun publishCommand(device: AC, type: ACCommandType, value: Any, uuid: String) {
        val userId = authFacade.getUser().id
        val cmd = ACCmdMqttDTO(userId, type, value, uuid)
        val msg = MqttMessage(objectMapper.writeValueAsBytes(cmd))

        mqttClient.publish("ac/cmd/${device.id}", msg)
    }

    fun getHistory(device: AC, dto: DeviceCmdHistoryRequestBodyDTO): DeviceCmdHistoryDTO<ACCmdHistoryItemDTO_> {
        val items = cmdService.getCommandHistory(device, dto, ACCmdHistoryItemDTO::class.java, "ac_w")
        val meta = userService
            .findByRoleAndIdIn(User.Role.CLIENT, items.list.map { u -> u.userId })
            .map{u -> DeviceCmdHistoryUserDataDTO(u.id, u.name, u.lastName)}

        val items2 = items.list.map { o ->
            val u = meta.find { ud -> ud.id == o.userId } ?: DeviceCmdHistoryUserDataDTO(
                DeviceCmdHistoryUserData_Automatic_ID,
                DeviceCmdHistoryUserData_Automatic_Name,
                DeviceCmdHistoryUserData_Automatic_LastName
            )
            ACCmdHistoryItemDTO_(o, u)
        }

        return DeviceCmdHistoryDTO(items2, items.totalItems, items.pagination)
    }
}