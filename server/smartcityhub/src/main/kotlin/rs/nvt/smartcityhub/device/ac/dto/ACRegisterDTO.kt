package rs.nvt.smartcityhub.device.ac.dto

import rs.nvt.smartcityhub.device.ac.ACMode
import rs.nvt.smartcityhub.device.dto.DeviceRegisterDTO

data class ACRegisterDTO (
    val base: DeviceRegisterDTO,
    val supportedModes: List<ACMode>,
    val minTemperature: Double,
    val maxTemperature: Double
)