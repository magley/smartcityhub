package rs.nvt.smartcityhub.device

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import rs.nvt.smartcityhub.config.influxdb.PingMeasurement
import rs.nvt.smartcityhub.device.common.DeviceMeasurementServiceCommon
import rs.nvt.smartcityhub.device.dto.*
import java.time.Duration
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*

@Service
class DeviceAvailabilityService {
    @Autowired lateinit var measurementService: DeviceMeasurementServiceCommon
    private fun getAvailabilityReportIntervals(dto: DeviceMeasurementHistoryRequestBodyDTO): Pair<AvailabilityDistanceUnit, List<AvailabilityReport>> {
        return when (dto.mode) {
            DeviceMeasurementHistoryMode.mode6h -> Pair(AvailabilityDistanceUnit.HOURS, availabilityReportIntervalsHours(6L))
            DeviceMeasurementHistoryMode.mode12h -> Pair(AvailabilityDistanceUnit.HOURS, availabilityReportIntervalsHours(12L))
            DeviceMeasurementHistoryMode.mode24h -> Pair(AvailabilityDistanceUnit.HOURS, availabilityReportIntervalsHours(24L))
            DeviceMeasurementHistoryMode.mode1w -> Pair(AvailabilityDistanceUnit.DAYS, availabilityReportIntervalsDays(7))
            DeviceMeasurementHistoryMode.mode1m -> Pair(AvailabilityDistanceUnit.DAYS, availabilityReportIntervalsDays(30))
            DeviceMeasurementHistoryMode.modeCustom -> {
                val diff = ChronoUnit.DAYS.between(dto.date1!!.toInstant(), dto.date2!!.toInstant())
                if (diff <= 1L) {
                    return Pair(AvailabilityDistanceUnit.HOURS, availabilityReportIntervalsHours(24, dto.date2))
                }
                else {
                    return Pair(AvailabilityDistanceUnit.DAYS, availabilityReportIntervalsDays(diff, dto.date2))
                }
            }
        }
    }

    private fun availabilityReportIntervalsHours(hoursOffset: Long, d: Date? = null): List<AvailabilityReport> {
        val cal = Calendar.getInstance()
        if (d != null) {
            cal.time = d
        }
        cal.set(Calendar.MINUTE, 0)
        cal.set(Calendar.SECOND, 0)
        cal.set(Calendar.MILLISECOND, 0)
        val end = cal.toInstant()
        val reports = mutableListOf<AvailabilityReport>()
        for (offset in hoursOffset downTo 0) {
            val s = end.minus(Duration.ofHours(offset))
            val e = s.plus(Duration.ofHours(1L))
            val rep = AvailabilityReport(s, e, 0, 0.0)
            reports.add(rep)
        }
        return reports
    }

    private fun availabilityReportIntervalsDays(daysOffset: Long, d: Date? = null): List<AvailabilityReport> {
        val cal = Calendar.getInstance()
        if (d != null) {
            cal.time = d
        }
        cal.set(Calendar.HOUR_OF_DAY, 0)
        cal.set(Calendar.MINUTE, 0)
        cal.set(Calendar.SECOND, 0)
        cal.set(Calendar.MILLISECOND, 0)
        val end = cal.toInstant()
        val reports = mutableListOf<AvailabilityReport>()
        for (offset in daysOffset downTo 0) {
            val s = end.minus(Duration.ofDays(offset))
            val e = s.plus(Duration.ofDays(1L))
            val rep = AvailabilityReport(s, e, 0, 0.0)
            reports.add(rep)
        }
        return reports
    }


    data class OnlineInterval(var start: Instant, var end: Instant)
    private fun getOnlineIntervals(res: List<PingMeasurement>): List<OnlineInterval> {
        val diffBeforeOfflineInS = 15L

        if (res.isEmpty()) {
            return emptyList()
        }

        val allIntervals = mutableListOf<OnlineInterval>()
        var interval = OnlineInterval(res[0].time, res[0].time)

        for (idx in 1 until res.size) {
            val newP = res[idx].time
            val diffInS = ChronoUnit.SECONDS.between(interval.end, newP)
            if (diffInS >= diffBeforeOfflineInS) {
                interval.end = interval.end.plus(Duration.ofSeconds(diffBeforeOfflineInS))
                allIntervals.add(interval)
                interval = OnlineInterval(newP, newP)
            }
            else {
                interval.end = newP
            }
        }
        interval.end = interval.end.plus(Duration.ofSeconds(diffBeforeOfflineInS))
        allIntervals.add(interval)

        return allIntervals
    }

    private fun fillOutAvailabilityReport(reports: List<AvailabilityReport>, intervals: List<OnlineInterval>) {
        for (report in reports) {
            for (interval in intervals) {
                if (interval.start.isAfter(report.end) || interval.end.isBefore(report.start)) {
                    continue
                }
                var start = interval.start
                if (start.isBefore(report.start)) {
                    start = report.start
                }
                var end = interval.end
                if (end.isAfter(report.end)) {
                    end = report.end
                }

                report.durationInS += ChronoUnit.SECONDS.between(start, end)
            }
            report.percentage = report.durationInS.toDouble() / ChronoUnit.SECONDS.between(report.start, report.end)
        }
    }

    fun getDeviceAvailabilityReport(device: Device, dto: DeviceMeasurementHistoryRequestBodyDTO): DeviceAvailabilityReportDTO {
        val res = measurementService.getMeasurementHistory(device.id, dto, "ping", PingMeasurement::class.java)

        val reportIntervals = getAvailabilityReportIntervals(dto)
        val intervals = getOnlineIntervals(res)

        fillOutAvailabilityReport(reportIntervals.second, intervals)

        return DeviceAvailabilityReportDTO(reportIntervals.first, reportIntervals.second)
    }
}