package rs.nvt.smartcityhub.device.dto

import java.time.Instant

enum class AvailabilityDistanceUnit {
    DAYS,
    HOURS
}

data class AvailabilityReport(var start: Instant, var end: Instant, var durationInS: Long, var percentage: Double)

data class DeviceAvailabilityReportDTO(val unit: AvailabilityDistanceUnit, val reports: List<AvailabilityReport>)
