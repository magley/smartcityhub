package rs.nvt.smartcityhub.city

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import rs.nvt.smartcityhub.exception.EntityNotFoundException

@Service
class CityService: ICityService {
    @Autowired
    lateinit var cityRepo: ICityRepo

    override fun findById(id: Long): City {
        return cityRepo.findById(id).orElseThrow { EntityNotFoundException(City::class, id) }
    }
}