package rs.nvt.smartcityhub.util

data class InfluxPaginator(
    val pageSize: Long,
    val pageCount: Long,
    val sortColumn: String = "_time",
    val sortDesc: Boolean = true,
) {
    val offset = pageCount * pageSize
}