package rs.nvt.smartcityhub.verification

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import rs.nvt.smartcityhub.user.User
import java.util.*

@Repository
interface IVerificationTokenRepo : JpaRepository<VerificationToken, Long> {
    fun findByUser(user: User): Optional<VerificationToken>
    fun findByToken(token: String): Optional<VerificationToken>
}