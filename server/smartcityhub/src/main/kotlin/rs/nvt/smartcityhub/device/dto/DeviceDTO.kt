package rs.nvt.smartcityhub.device.dto

import rs.nvt.smartcityhub.device.Device
import rs.nvt.smartcityhub.device.DeviceType

data class DeviceDTO(val id: Long, val propertyId: Long, val online: Boolean, val powerSupply: Device.PowerSupply, val powerDrainage: Int = 100, val type: DeviceType) {
    constructor(device: Device) : this(device.id, device.property.id, device.online, device.powerSupply, device.powerDrainage, device.getType())
}