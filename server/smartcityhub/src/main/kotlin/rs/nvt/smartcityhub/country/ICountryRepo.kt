package rs.nvt.smartcityhub.country

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ICountryRepo : JpaRepository<Country, Long>