package rs.nvt.smartcityhub.util

/**
 * Used for pagination. We need the page and the total number of elements.
 */
data class CollectionResultDTO<T> (
    val totalCount: Long,
    val items: List<T>,
)