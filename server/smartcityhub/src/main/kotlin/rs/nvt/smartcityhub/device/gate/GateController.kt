package rs.nvt.smartcityhub.device.gate

import io.swagger.v3.oas.annotations.Operation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import rs.nvt.smartcityhub.config.auth.IAuthenticationFacade
import rs.nvt.smartcityhub.device.DeviceAccessControl
import rs.nvt.smartcityhub.device.DeviceType
import rs.nvt.smartcityhub.device.IDeviceService
import rs.nvt.smartcityhub.device.common.DeviceServiceCommon
import rs.nvt.smartcityhub.device.dto.DeviceCmdHistoryDTO
import rs.nvt.smartcityhub.device.dto.DeviceCmdHistoryMetaDTO
import rs.nvt.smartcityhub.device.dto.DeviceCmdHistoryRequestBodyDTO
import rs.nvt.smartcityhub.property.IPropertyService
import rs.nvt.smartcityhub.property.PropertyAccessControl

@RestController
@RequestMapping("/api/device/gate")
class GateController {
    @Autowired lateinit var gateService: GateService
    @Autowired lateinit var deviceService: IDeviceService
    @Autowired lateinit var propertyService: IPropertyService
    @Autowired lateinit var propertyAccessControl: PropertyAccessControl
    @Autowired lateinit var authFacade: IAuthenticationFacade
    @Autowired lateinit var deviceAccessControl: DeviceAccessControl
    @Autowired lateinit var cmdService: DeviceServiceCommon

    @Operation(summary = "Register a new gate")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PostMapping()
    fun register(@RequestBody dto: GateRegisterDTO): ResponseEntity<Void> {
        val property = propertyService.getApproved(dto.base.propertyId)
        propertyAccessControl.assertOwnership(authFacade.getUser(), property)
        gateService.registerGate(dto, property)
        return ResponseEntity(null, HttpStatus.NO_CONTENT)
    }

    @Operation(summary = "Get gate by ID")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @GetMapping("/{id}")
    fun get(@PathVariable id: Long): ResponseEntity<GateDTO> {
        val device = getDeviceWithAssertion(id)
        val result = GateDTO(device)
        return ResponseEntity(result, HttpStatus.OK)
    }

    @Operation(summary = "Get history of actions for this gate")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PostMapping("/command-history")
    fun getHistory(@RequestBody dto: DeviceCmdHistoryRequestBodyDTO): ResponseEntity<DeviceCmdHistoryDTO<GateCmdHistoryItemDTO_>> {
        val device = getDeviceWithAssertion(dto.id)
        val result = gateService.getHistory(device, dto)
        return ResponseEntity(result, HttpStatus.OK)
    }

    @Operation(summary = "Set mode for this gate")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PutMapping("/mode")
    fun setMode(@RequestBody dto: GateCmdModeDTO): ResponseEntity<Void> {
        val device = getDeviceWithAssertion(dto.id)
        gateService.setMode(device, dto.mode, dto.uuid)
        return ResponseEntity(null, HttpStatus.NO_CONTENT)
    }

    @Operation(summary = "Open or close this gate")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PutMapping("/start")
    fun setStart(@RequestBody dto: GateCmdStartDTO): ResponseEntity<Void> {
        val device = getDeviceWithAssertion(dto.id)
        when (dto.open) {
            GateOpen.opening -> gateService.opening(device, dto.uuid)
            GateOpen.closing -> gateService.closing(device, dto.uuid)
            else -> {} // Should not receive open/closed
        }
        return ResponseEntity(null, HttpStatus.NO_CONTENT)
    }

    @Operation(summary = "Turn override mode for this gate on/off")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PutMapping("/override")
    fun setOverride(@RequestBody dto: GateCmdOverrideDTO): ResponseEntity<Void> {
        val device = getDeviceWithAssertion(dto.id)
        when (dto.override) {
            GateOverride.yes -> gateService.overrideYes(device, dto.uuid)
            GateOverride.no -> gateService.overrideNo(device, dto.uuid)
        }
        return ResponseEntity(null, HttpStatus.NO_CONTENT)
    }

    private fun getDeviceWithAssertion(id: Long): Gate {
        val device = deviceService.find(id)
        deviceAccessControl.assertOwnership(authFacade.getUser(), device)
        deviceAccessControl.assertType(device, DeviceType.gate)
        return device as Gate
    }
}