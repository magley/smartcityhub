package rs.nvt.smartcityhub.property

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import rs.nvt.smartcityhub.exception.EntityNotFoundException
import rs.nvt.smartcityhub.permission.PermissionAccessControl
import rs.nvt.smartcityhub.user.User

@Component
class PropertyAccessControl {
    @Autowired
    lateinit var propertyService: IPropertyService
    @Autowired lateinit var permissionAccessControl: PermissionAccessControl

    fun propertyHasPartialPermissions(user: User, property: Property) = permissionAccessControl.propertyHasPartialPermissions(user, property)
    fun assertPropertyPermissionIncludingPermissionsByDevice(user: User, property: Property) = permissionAccessControl.assertPropertyPermissionIncludingPermissionsByDevice(user, property)

    /**
     * This does not include shared properties.
     */
    fun assertDirectOwnership(user: User, property: Property) {
        if (property.owner.id != user.id) {
            throw EntityNotFoundException(Property::class, property.id)
        }
    }

    /**
     * This includes shared properties. It does not include properties whose devices 'user' has permissions for.
     */
    fun assertOwnership(user: User, property: Property) = permissionAccessControl.assertPropertyPermission(user, property)
}