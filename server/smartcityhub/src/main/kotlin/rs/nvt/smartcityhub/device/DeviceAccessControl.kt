package rs.nvt.smartcityhub.device

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import rs.nvt.smartcityhub.exception.EntityNotFoundException
import rs.nvt.smartcityhub.permission.PermissionAccessControl
import rs.nvt.smartcityhub.property.PropertyAccessControl
import rs.nvt.smartcityhub.user.User

@Component
class DeviceAccessControl {
    @Autowired lateinit var deviceService: IDeviceService
    @Autowired lateinit var propertyAccessControl: PropertyAccessControl
    @Autowired lateinit var permissionAccessControl: PermissionAccessControl

    fun assertOwnership(user: User, device: Device) = permissionAccessControl.assertDevicePermission(user, device)
    fun assertDirectOwnership(user: User, device: Device) = propertyAccessControl.assertOwnership(user, device.property)

    fun assertType(device: Device, type: DeviceType) {
        if (device.getType() != type) {
            throw EntityNotFoundException(DeviceController::class, device.id)
        }
    }
}