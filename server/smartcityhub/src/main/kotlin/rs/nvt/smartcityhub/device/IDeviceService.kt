package rs.nvt.smartcityhub.device

import org.springframework.data.domain.Pageable
import rs.nvt.smartcityhub.device.dto.DeviceAvailabilityReportDTO
import rs.nvt.smartcityhub.device.dto.DeviceMeasurementHistoryRequestBodyDTO
import rs.nvt.smartcityhub.device.dto.DeviceRegisterDTO
import rs.nvt.smartcityhub.property.Property

interface IDeviceService {
    fun findDevicesByPropertyId(id: Long): List<Device>
    fun findDevicesByPropertyId(id: Long, p: Pageable): List<Device>
    fun registerDevice(dto: DeviceRegisterDTO, propertyCached: Property): Device
    fun findAll(): List<Device>
    fun find(id: Long): Device
    fun LOADTEST_generate(property: Property, howMany: Int)
    fun LOADTEST_generate(property: Property, howMany: Int, type: DeviceType)
}