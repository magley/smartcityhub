package rs.nvt.smartcityhub.util

data class Paginator (
    val pageSize: Long,
    val pageCount: Long,
) {
    val offset = pageCount * pageSize
}