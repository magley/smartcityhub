package rs.nvt.smartcityhub.property.dto

import rs.nvt.smartcityhub.property.GeoCoordinate
import rs.nvt.smartcityhub.property.Property
import rs.nvt.smartcityhub.util.validator.Base64MaxSize

data class RequestPropertyDTO(
    val type: Property.Type,
    val address: String,
    val cityId: Long,
    val size: Double,
    val floors: Int,
    val location: GeoCoordinate,
    @Base64MaxSize(maxSizeInBytes = 1024 * 1024 * 500) val imageB64: String
)