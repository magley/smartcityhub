package rs.nvt.smartcityhub.exception

import kotlin.reflect.KClass

class EntityNotFoundException : Exception {
    constructor() : super()
    constructor(type: KClass<*>, id: Long) : super("$type with id $id not found")
    constructor(type: KClass<*>, helpMsg: String = "") : super("$type not found${ if (helpMsg.isNotEmpty()) " ($helpMsg)" else "" }")
}