package rs.nvt.smartcityhub.device.lamp

import io.swagger.v3.oas.annotations.Operation
import jakarta.validation.Valid
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import rs.nvt.smartcityhub.config.auth.IAuthenticationFacade
import rs.nvt.smartcityhub.device.DeviceAccessControl
import rs.nvt.smartcityhub.device.DeviceType
import rs.nvt.smartcityhub.device.IDeviceService
import rs.nvt.smartcityhub.device.common.DeviceServiceCommon
import rs.nvt.smartcityhub.device.dto.DeviceCmdHistoryDTO
import rs.nvt.smartcityhub.device.dto.DeviceCmdHistoryRequestBodyDTO
import rs.nvt.smartcityhub.device.dto.DeviceMeasurementHistoryRequestBodyDTO

@RestController
@RequestMapping("/api/device/lamp")
class LampController {
    @Autowired lateinit var lampService: LampService
    @Autowired lateinit var deviceService: IDeviceService
    @Autowired lateinit var authFacade: IAuthenticationFacade
    @Autowired lateinit var deviceAccessControl: DeviceAccessControl
    @Autowired lateinit var cmdService: DeviceServiceCommon

    @Operation(summary = "Get history of actions for this lamp")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PostMapping("/command-history")
    fun getHistory(@RequestBody dto: DeviceCmdHistoryRequestBodyDTO): ResponseEntity<DeviceCmdHistoryDTO<LampCmdHistoryItemDTO_>> {
        val device = getDeviceWithAssertion(dto.id)
        val result = lampService.getHistory(device, dto)
        return ResponseEntity(result, HttpStatus.OK)
    }

    @Operation(summary = "Get history of insolation readings for this lamp")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PutMapping("/measurement-history")
    fun getMeasurementHistory(@Valid @RequestBody dto: DeviceMeasurementHistoryRequestBodyDTO):  List<LampMeasurementHistoryItemDTO> {
        val device = getDeviceWithAssertion(dto.id)
        return lampService.getMeasurementHistoricalData(device, dto)
    }

    @Operation(summary = "Set mode for this lamp")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PutMapping("/mode")
    fun setMode(@RequestBody dto: LampCmdModeDTO): ResponseEntity<Void> {
        val device = getDeviceWithAssertion(dto.id)
        lampService.setMode(device, dto.mode, dto.threshold, dto.uuid)
        return ResponseEntity(null, HttpStatus.NO_CONTENT)
    }

    @Operation(summary = "Turn the lamp on/off")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PutMapping("/start")
    fun setStart(@RequestBody dto: LampCmdStartDTO): ResponseEntity<Void> {
        val device = getDeviceWithAssertion(dto.id)
        when (dto.start) {
            true -> lampService.start(device, dto.uuid)
            false -> lampService.stop(device, dto.uuid)
        }
        return ResponseEntity(null, HttpStatus.NO_CONTENT)
    }

    private fun getDeviceWithAssertion(id: Long): Lamp {
        val device = deviceService.find(id)
        deviceAccessControl.assertOwnership(authFacade.getUser(), device)
        deviceAccessControl.assertType(device, DeviceType.lamp)
        return device as Lamp
    }
}