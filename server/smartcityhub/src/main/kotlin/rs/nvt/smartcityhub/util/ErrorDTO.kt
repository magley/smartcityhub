package rs.nvt.smartcityhub.util

import org.springframework.http.HttpStatus

data class ErrorDTO (
    val code: HttpStatus,
    val message: String
)