package rs.nvt.smartcityhub.config.auth

data class LoginDTO (val username: String, val password: String)