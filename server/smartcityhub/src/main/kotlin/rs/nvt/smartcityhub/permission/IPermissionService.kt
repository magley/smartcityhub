package rs.nvt.smartcityhub.permission

import org.springframework.data.domain.Pageable
import rs.nvt.smartcityhub.device.Device
import rs.nvt.smartcityhub.permission.dto.NewPermissionDTO
import rs.nvt.smartcityhub.permission.dto.PermissionDTO
import rs.nvt.smartcityhub.permission.dto.RevokePermissionDTO
import rs.nvt.smartcityhub.property.Property
import rs.nvt.smartcityhub.user.User

interface IPermissionService {
    fun newPermission(dto: NewPermissionDTO)
    fun revokePermission(dto: RevokePermissionDTO)
    fun getPropertyPermissions(property: Property): List<PermissionDTO>
    fun getPropertyPermissionsCount(property: Property): Long
    fun getPropertyPermissions(property: Property, p: Pageable): List<PermissionDTO>
    fun getDevicePermissions(device: Device): List<PermissionDTO>
    fun getDevicePermissionsCount(device: Device): Long
    fun getDevicePermissions(device: Device, p: Pageable): List<PermissionDTO>
    fun getPropertyPermissions(user: User): List<PropertyPermission>
    fun getPropertyPermissions(user: User, p: Pageable): List<PropertyPermission>
    fun getDevicePermissions(user: User): List<DevicePermission>
    fun getDevicePermissions(user: User, p: Pageable): List<DevicePermission>
}