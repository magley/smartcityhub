package rs.nvt.smartcityhub.country

import jakarta.persistence.*
import rs.nvt.smartcityhub.city.City

@Entity
@Table(name = "countries")
class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0

    @Column(nullable = false)
    lateinit var name: String

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "country")
    lateinit var cities: List<City>
}