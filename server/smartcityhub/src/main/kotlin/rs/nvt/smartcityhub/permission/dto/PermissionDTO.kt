package rs.nvt.smartcityhub.permission.dto

import rs.nvt.smartcityhub.permission.DevicePermission
import rs.nvt.smartcityhub.permission.PropertyPermission

data class PermissionDTO (
    val entityId: Long,
    val userId: Long,
    val userName: String,
    val userLastName: String,
    val userEmail: String) {

    constructor(permission: DevicePermission) : this(
        permission.device.id,
        permission.user.id,
        permission.user.name,
        permission.user.lastName,
        permission.user.email,
    )

    constructor(permission: PropertyPermission) : this(
        permission.property.id,
        permission.user.id,
        permission.user.name,
        permission.user.lastName,
        permission.user.email
    )
}