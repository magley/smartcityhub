package rs.nvt.smartcityhub.permission

import jakarta.persistence.*
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import rs.nvt.smartcityhub.device.Device
import rs.nvt.smartcityhub.property.Property
import rs.nvt.smartcityhub.user.User

@Entity
@Table(name = "device_permissions")
@Cache(region = "Permission", usage = CacheConcurrencyStrategy.READ_ONLY)
class DevicePermission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0

    @ManyToOne
    lateinit var device: Device

    @ManyToOne
    lateinit var user: User
}