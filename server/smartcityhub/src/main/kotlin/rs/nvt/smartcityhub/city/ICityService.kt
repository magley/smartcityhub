package rs.nvt.smartcityhub.city

interface ICityService {
    fun findById(id: Long): City
}