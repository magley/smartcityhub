package rs.nvt.smartcityhub.config

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import rs.nvt.smartcityhub.device.DeviceType
import rs.nvt.smartcityhub.device.IDeviceService
import rs.nvt.smartcityhub.property.IPropertyService
import rs.nvt.smartcityhub.user.IUserService
import rs.nvt.smartcityhub.user.User
import rs.nvt.smartcityhub.user.dto.ClientRegisterDTO
import rs.nvt.smartcityhub.user.dto.CreateUserDTO
import java.io.PrintWriter
import java.util.*

private const val SUPERADMIN_PASS_FNAME = "./config/superadminpass"

const val TOPIC_TO_PUBLISH_TO = "property/1/devices/ack"

@Component
class ServerInitializer : ApplicationRunner {
    @Autowired private lateinit var userService: IUserService
    var logger: Logger = LoggerFactory.getLogger(ApplicationRunner::class.java)
    @Autowired private lateinit var propertyService: IPropertyService
    @Autowired private lateinit var deviceService: IDeviceService

    override fun run(applicationArguments: ApplicationArguments) {
        System.out.println(org.hibernate.Version.getVersionString());

        if (userService.shouldCreateSuperAdmin()) {
            val superAdminPassword = userService.createSuperAdmin()

            val writer = PrintWriter(SUPERADMIN_PASS_FNAME, "UTF-8")
            writer.println(superAdminPassword)
            writer.close()

            logger.info("Superadmin's password is {}", superAdminPassword)
        }

//        for (client in userService.findAllClients()) {
//            userService.addToElasicSearch(client)
//        }
//        println("Added all current clients in elasticsearch db")

        // val n = 10000
        // logger.warn("Generating $n users for load testing...")
        // userService.LOADTEST_createDummyUsers(n);
        //userService.LOADTEST_createAdmins(1000)
        //LOADTEST_add_properties_and_devices()
        //LOADTEST_add_n_devices_of_type(300, DeviceType.wash)
    }

    @Transactional
    fun LOADTEST_add_n_devices_of_type(n: Int, type: DeviceType) {
        logger.warn("Generating ${n} devices of type ${type}...")
        val p = propertyService.find(1)
        if (p != null) {
            deviceService.LOADTEST_generate(p, n, type)
        }
        logger.warn("Done!")
    }

    @Transactional
    fun LOADTEST_add_properties_and_devices() {
        logger.warn("Generating properties and devices for load testing...")
        val properties =    propertyService.LOADTEST_generate_for_each_user(1000)
        for (p in properties) {
            deviceService.LOADTEST_generate(p, 1000)
            logger.warn("Done p")
        }
        logger.warn("Done!")
    }

}