package rs.nvt.smartcityhub.device.dto

import java.util.*

enum class DeviceMeasurementHistoryMode {
    mode6h, mode12h, mode24h, mode1w, mode1m, modeCustom;
    companion object {
        fun toMiniString(value: DeviceMeasurementHistoryMode): String {
            return when (value) {
                mode6h -> "6h"
                mode12h -> "12h"
                mode24h -> "24h"
                mode1w -> "1w"
                mode1m -> "1mo"
                modeCustom -> "modeCustom"
            }
        }
    }
}

data class DeviceMeasurementHistoryRequestBodyDTO(
    val id: Long,
    val mode: DeviceMeasurementHistoryMode,
    val date1: Date?,
    val date2: Date?,
)