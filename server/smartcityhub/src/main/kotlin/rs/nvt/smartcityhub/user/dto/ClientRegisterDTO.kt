package rs.nvt.smartcityhub.user.dto

import rs.nvt.smartcityhub.util.validator.Base64MaxSize

data class ClientRegisterDTO(
    val email: String,
    val password: String,
    val name: String,
    val lastName: String,
    @Base64MaxSize(maxSizeInBytes = 1024 * 1024 * 500) val avatarB64: String
)