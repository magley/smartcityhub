package rs.nvt.smartcityhub.permission.dto

data class RevokePermissionDTO (
    val userId: Long,
    val entityId: Long,
    val entityType: PermissionTypeDTO
)