package rs.nvt.smartcityhub.config

import com.fasterxml.jackson.databind.ObjectMapper

class JSONUtil {
    companion object {
        // This is ugly. The object mapper *could* be a bean, but I wonder
        // if it would clash with whatever Spring uses. Also, the only reason
        // JSONUtil exists is that there's no clean one-liner in Jackson,
        // unlike in gson.

        val mapper_ = ObjectMapper()

        inline fun <reified T> from(s: String?): T {
            return mapper_.readValue(s, T::class.java)
        }

        inline fun <reified T> from(b: ByteArray): T {
            val s: String = String(b)
            return from(s)
        }
    }
}