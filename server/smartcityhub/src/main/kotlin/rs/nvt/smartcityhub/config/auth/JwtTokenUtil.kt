package rs.nvt.smartcityhub.config.auth

import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.security.Keys
import org.springframework.stereotype.Component


@Component
class JwtTokenUtil {
    private val secret = Keys.secretKeyFor(SignatureAlgorithm.HS512)

    fun generateJwt(username: String, id: Long, role: String, nagForPassword: Boolean): String {
        val claims: MutableMap<String, Any> = HashMap()
        claims["id"] = id
        claims["role"] = role
        claims["nagPassword"] = nagForPassword
        return generateJwt(claims, username)
    }

    fun getUsernameFromJwt(jwt: String): String {
        return getClaimFromJwt(jwt, Claims::getSubject)
    }

    private fun <T> getClaimFromJwt(jwt: String, claimsResolver: (Claims) -> T): T {
        val claims: Claims = getAllClaimsFromJtw(jwt)
        return claimsResolver.invoke(claims)
    }

    private fun getAllClaimsFromJtw(jwt: String): Claims {
        return Jwts.parserBuilder().setSigningKey(secret).build().parseClaimsJws(jwt).body
    }

    private fun generateJwt(claims: Map<String, Any?>, subject: String): String {
        return Jwts.builder()
            .setIssuer("smartcityhub")
            .setClaims(claims)
            .setSubject(subject)
            .signWith(SignatureAlgorithm.HS512, secret)
            .compact()
    }

    fun getJwtFromHeader(header: String?): String? {
        return if (header == null || !header.contains("Bearer")) {
            null
        } else try {
            val jwt = header.substring(header.indexOf("Bearer ") + 7)
            if (jwt.isEmpty()) {
                null
            } else jwt
        } catch (e: IndexOutOfBoundsException) {
            null
        }
    }
}