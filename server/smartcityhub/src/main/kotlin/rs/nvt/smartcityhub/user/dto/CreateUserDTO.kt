package rs.nvt.smartcityhub.user.dto


data class CreateUserDTO (
    var email: String,
    var password: String,
    var name: String,
    var lastName: String,
)