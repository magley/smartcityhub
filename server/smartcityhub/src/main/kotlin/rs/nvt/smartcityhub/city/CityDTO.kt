package rs.nvt.smartcityhub.city

data class CityDTO(val id: Long, val name: String) {
    constructor(city: City): this(city.id, city.name)
}