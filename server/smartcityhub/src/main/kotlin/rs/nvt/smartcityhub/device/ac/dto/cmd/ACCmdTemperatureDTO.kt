package rs.nvt.smartcityhub.device.ac.dto.cmd

/**
 * `ACCmdTemperatureDTO` is the HTTP request payload for a "set temperature" command.
 */
data class ACCmdTemperatureDTO (
    val id: Long,
    val uuid: String,
    val temperature: Double
)