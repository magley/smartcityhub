package rs.nvt.smartcityhub.device.dto

import rs.nvt.smartcityhub.device.Device
import rs.nvt.smartcityhub.device.DeviceType
import rs.nvt.smartcityhub.property.Property
import rs.nvt.smartcityhub.user.User

data class DeviceMetaDTO(val id: Long, val propertyId: Long, val propertyType: Property.Type, val type: DeviceType, val owned: Boolean) {
    constructor(device: Device, userWhoRequested: User): this(
        device.id,
        device.property.id,
        device.property.type,
        device.getType(),
        device.property.owner.id == userWhoRequested.id,
    )
}