package rs.nvt.smartcityhub.user

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import rs.nvt.smartcityhub.config.PasswordUtil
import rs.nvt.smartcityhub.exception.EntityNotFoundException
import rs.nvt.smartcityhub.exception.RequestException
import rs.nvt.smartcityhub.user.dto.ClientRegisterDTO
import rs.nvt.smartcityhub.user.dto.CreateUserDTO
import rs.nvt.smartcityhub.user.dto.PasswordChangeDTO
import rs.nvt.smartcityhub.util.ImageSaveMode
import rs.nvt.smartcityhub.util.saveBase64ToImage
import rs.nvt.smartcityhub.util.SendgridUtil
import rs.nvt.smartcityhub.verification.IVerificationTokenService

@Service
class UserService : IUserService {
    @Autowired private lateinit var userRepo: IUserRepo
    @Autowired private lateinit var passwordUtil: PasswordUtil
    @Autowired private lateinit var verificationTokenService: IVerificationTokenService
    @Autowired private lateinit var sendGridUtil: SendgridUtil
    @Autowired private lateinit var userRepoElastic: IUserRepoElastic

    override fun findAll(): MutableList<User> {
        return userRepo.findAll()
    }

    override fun findAllClients(): List<User> {
        return userRepo.findByRoleAndActivatedTrue(User.Role.CLIENT).toMutableList()
    }

    override fun findAllAdmins(): MutableList<User> {
        return userRepo.findAll().filter{u -> u.role == User.Role.ADMIN}.toMutableList()
    }

    override fun findAllAdmins(p: Pageable): List<User> {
        return userRepo.findByRoleAndActivatedTrue(User.Role.ADMIN, p)
    }

    override fun countAllAdmins(): Long {
        return userRepo.countByRoleAndActivatedTrue(User.Role.ADMIN)
    }

    override fun findByRoleAndIdIn(role: User.Role, ids: List<Long>): List<User> {
        return userRepo.findByRoleAndIdIn(role, ids)
    }

    override fun findByRoleAndIdBetween(role: User.Role, idFrom: Long, idTo: Long): List<User> {
        return userRepo.findByRoleAndIdBetween(role, idFrom, idTo)
    }

    override fun activateUser(user: User) {
        user.activated = true
        saveUser(user)
    }

    override fun shouldCreateSuperAdmin(): Boolean {
        return userRepo.findByEmail("admin").isEmpty
    }

    override fun changePassword(user: User, dto: PasswordChangeDTO) {
        if (!passwordUtil.doPasswordsMatch(dto.oldPassword, user.password)) {
            throw RequestException(HttpStatus.BAD_REQUEST, "Incorrect old password")
        }
        if (passwordUtil.doPasswordsMatch(dto.newPassword, user.password)) {
            throw RequestException(HttpStatus.BAD_REQUEST, "New password must be different")
        }
        user.password_ = dto.newPassword
        user.shouldChangeCurrentPassword = false
        encodeCurrentPassword(user)
        saveUser(user)
    }

    override fun find(userId: Long): User? {
        return userRepo.findById(userId).orElse(null)
    }

    override fun findByEmail(email: String): User? {
        return userRepo.findByEmail(email).orElse(null)
    }

    override fun get(id: Long): User {
        return userRepo.findById(id).orElseThrow { EntityNotFoundException(User::class, id) }
    }

    override fun remove(id: Long) {
        val userToDelete = get(id)
        if (userToDelete.role == User.Role.SUPER_ADMIN) {
            throw RequestException(HttpStatus.BAD_REQUEST, "Cannot delete the super admin!")
        }
        userRepo.delete(userToDelete)
    }

    override fun createSuperAdmin(): String {
        val superAdmin = User(CreateUserDTO(
                email = "admin",
                password = java.util.UUID.randomUUID().toString().substring(0,15),
                name = "",
                lastName = "",
            )
        )
        superAdmin.role = User.Role.SUPER_ADMIN
        superAdmin.shouldChangeCurrentPassword = true
        val passwordUnencrypted = superAdmin.password_

        superAdmin.activated = true
        encodeCurrentPassword(superAdmin)
        saveUser(superAdmin)

        return passwordUnencrypted
    }

    private fun ensureEmailNotTaken(newEmail: String) {
        if (findByEmail(newEmail) != null) {
            throw RequestException(HttpStatus.BAD_REQUEST, "Email already taken")
        }
    }

    override fun loadUserByUsername(username: String): UserDetails {
        return userRepo.findByEmail(username).orElseThrow { EntityNotFoundException(User::class, "User with username $username does not exist") }
    }

    override fun registerAdmin(dto: CreateUserDTO) {
        ensureEmailNotTaken(dto.email)
        val newUser = User(dto)
        newUser.role = User.Role.ADMIN
        newUser.activated = true
        encodeCurrentPassword(newUser)
        userRepo.save(newUser) // saveUser(newUser) Don't save in elasticsearch
    }

    override fun registerClient(dto: ClientRegisterDTO) {
        ensureEmailNotTaken(dto.email)
        val newUser = User(dto)
        newUser.role = User.Role.CLIENT
        encodeCurrentPassword(newUser)
        saveUser(newUser)

        saveBase64ToImage(dto.avatarB64, dto.email, ImageSaveMode.AVATAR)
        val token = verificationTokenService.create(newUser)

        sendGridUtil.sendVerificationEmail(token.token)
    }

    private fun encodeCurrentPassword(user: User): User {
        user.password_ = passwordUtil.encode(user.password_)
        return user
    }

    private fun saveUser(user: User) {
        userRepo.save(user)
        userRepoElastic.save(UserElastic(user))
    }

    // Use only during testing.
    override fun addToElasicSearch(user: User) {
        userRepoElastic.save(UserElastic(user))
    }

    override fun getClientSearch(searchFullName: String): List<User> {
        val li1 = userRepoElastic.findFuzzyByFullName(searchFullName)
        val li2: MutableList<User> = mutableListOf()
        for (u in li1) {
            val uu = find(u.id)
            if (uu != null) {
                li2.add(uu)
            }
        }
        return li2
    }

    @Transactional
    override fun LOADTEST_createDummyUsers(n: Int) {
        fun getRandomString(length: Int) : String {
            val allowedChars = ('A'..'Z') + ('a'..'z')
            return (1..length)
                .map { allowedChars.random() }
                .joinToString("")
        }

        for (i in 0..n) {
            val email = "   ${i}@gmail.com"
            val password = "123456_${i}"

            val u = User(CreateUserDTO(
                    email = email,
                    password = password,
                    name = getRandomString(5),
                    lastName = getRandomString(5),
                )
            )

            u.role = User.Role.CLIENT
            u.shouldChangeCurrentPassword = false
            u.activated = true
            encodeCurrentPassword(u)
            println(i)
            saveUser(u)
        }
    }

    @Transactional
    override fun LOADTEST_createAdmins(n: Int) {
        fun getRandomString(length: Int) : String {
            val allowedChars = ('A'..'Z') + ('a'..'z')
            return (1..length)
                .map { allowedChars.random() }
                .joinToString("")
        }

        for (i in 0..n) {
            val email = "admin${i}@gmail.com"
            val password = "123456_${i}"

            val u = User(CreateUserDTO(
                email = email,
                password = password,
                name = getRandomString(5),
                lastName = getRandomString(10),)
            )
            u.role = User.Role.ADMIN
            u.shouldChangeCurrentPassword = false
            u.activated = true
            encodeCurrentPassword(u)

            println(i)
            userRepo.save(u) // saveUser(newUser) Don't save in elasticsearch
        }
    }

    fun LOADTEST_createDummyUser(email: String, password: String): User {
        val u = User(CreateUserDTO(
            email = email,
            password = password,
            name = "LoadTest",
            lastName = "DoNotUse",)
        )
        u.role = User.Role.CLIENT
        u.shouldChangeCurrentPassword = false
        u.activated = true
        encodeCurrentPassword(u)
        return u
    }
}