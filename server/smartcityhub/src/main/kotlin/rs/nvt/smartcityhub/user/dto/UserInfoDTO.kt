package rs.nvt.smartcityhub.user.dto

import rs.nvt.smartcityhub.user.User

data class UserInfoDTO (val id: Long, val email: String, val name: String, val lastName: String, val role: String) {
    constructor(u: User) : this(u.id, u.email, u.name, u.lastName, u.role.toString())
}