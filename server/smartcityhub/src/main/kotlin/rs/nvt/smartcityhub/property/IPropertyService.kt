package rs.nvt.smartcityhub.property

import org.springframework.data.domain.Pageable

interface IPropertyService {
    fun findAllByOwnerId(ownerId: Long): List<Property>
    fun findAllApprovedByOwnerId(ownerId: Long): List<Property>
    fun findApprovedByOwnerId(ownerId: Long, p: Pageable): List<Property>
    fun findPendingByOwnerId(ownerId: Long, p: Pageable): List<Property>
    fun findAll(): List<Property>
    fun findAllPending(): List<Property>
    fun findAllPending(p: Pageable): List<Property>
    fun find(id: Long): Property?
    fun getApproved(id: Long): Property
    fun getPending(id: Long): Property
    fun get(id: Long): Property
    fun createProperty(property: Property): Property
    fun deny(propertyId: Long, reason: String)
    fun approve(propertyId: Long)
    fun LOADTEST_generate_for_each_user(howMany: Int): List<Property>
    fun countAllPending(): Long
}