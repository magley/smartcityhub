package rs.nvt.smartcityhub.user

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository

interface IUserRepoElastic : ElasticsearchRepository<UserElastic, Long> {
    fun findFuzzyByFullName(fullName: String): List<UserElastic>
}