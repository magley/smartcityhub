package rs.nvt.smartcityhub.device.ac

import jakarta.persistence.ElementCollection
import jakarta.persistence.Entity
import jakarta.persistence.EnumType
import jakarta.persistence.Enumerated
import rs.nvt.smartcityhub.device.Device
import rs.nvt.smartcityhub.device.DeviceType
import rs.nvt.smartcityhub.device.dto.DeviceRegisterDTO
import rs.nvt.smartcityhub.property.Property

enum class ACMode {
    heat,
    cool,
    auto,
    fan,
}

enum class ACCommandType {
    temperature,
    is_on,
    mode,
    config
}

@Entity
class AC : Device {
    constructor()
    constructor(dto: DeviceRegisterDTO, property: Property) : super(dto, property)
    override fun getType(): DeviceType = DeviceType.ac

    @ElementCollection(targetClass = ACMode::class)
    @Enumerated(EnumType.STRING)
    lateinit var supportedModes: List<ACMode>
    var minTemperature: Double = 0.0
    var maxTemperature: Double = 30.0
}