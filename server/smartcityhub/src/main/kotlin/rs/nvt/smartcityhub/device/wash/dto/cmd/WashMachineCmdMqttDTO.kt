package rs.nvt.smartcityhub.device.wash.dto.cmd

import rs.nvt.smartcityhub.device.dto.DeviceCmdMqttDTO
import rs.nvt.smartcityhub.device.wash.WashMachineCommandType

typealias WashMachineCmdMqttDTO = DeviceCmdMqttDTO<WashMachineCommandType>