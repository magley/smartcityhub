package rs.nvt.smartcityhub.util.validator

import jakarta.validation.Constraint
import jakarta.validation.ConstraintValidator
import jakarta.validation.ConstraintValidatorContext
import jakarta.validation.Payload
import kotlin.reflect.KClass

@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
@Constraint(validatedBy = [Base64MaxSizeValidator::class])
annotation class Base64MaxSize(
    val maxSizeInBytes: Int = 1024,
    val message: String = "Image too large.",
    val groups: Array<KClass<*>> = [],
    val payload: Array<KClass<out Payload>> = []
)

class Base64MaxSizeValidator : ConstraintValidator<Base64MaxSize, String> {
    private lateinit var annot: Base64MaxSize
    override fun initialize(constraintAnnotation: Base64MaxSize) {
        super.initialize(constraintAnnotation)
        annot = constraintAnnotation
    }

    override fun isValid(value: String?, context: ConstraintValidatorContext?): Boolean {
        if (value == null) {
            return false
        }
        return (value.length * 3 / 4) < annot.maxSizeInBytes
    }
}