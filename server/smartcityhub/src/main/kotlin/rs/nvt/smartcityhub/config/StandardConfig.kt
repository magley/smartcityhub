package rs.nvt.smartcityhub.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.json.JsonMapper
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import rs.nvt.smartcityhub.config.auth.AuthenticationFacade
import rs.nvt.smartcityhub.config.auth.IAuthenticationFacade
import rs.nvt.smartcityhub.util.SendgridUtil


@Configuration
class StandardConfig {
    @Bean
    fun passwordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }

    @Bean
    fun authFacade(): IAuthenticationFacade {
        return AuthenticationFacade()
    }

    @Bean
    fun sendgridUtil(): SendgridUtil {
        return SendgridUtil()
    }

    @Bean
    fun objectMapper(): ObjectMapper {
        return JsonMapper.builder()
            .findAndAddModules()
            .build()
    }
}