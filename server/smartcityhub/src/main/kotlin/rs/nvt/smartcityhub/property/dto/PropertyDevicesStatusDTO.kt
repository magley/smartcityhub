package rs.nvt.smartcityhub.property.dto

data class PropertyDeviceStatusDTO(val id: Long, val online: Boolean)

data class PropertyDevicesStatusDTO(val propertyId: Long, val devices: List<PropertyDeviceStatusDTO>)