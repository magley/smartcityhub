package rs.nvt.smartcityhub.permission

import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface IDevicePermissionRepo : JpaRepository<DevicePermission, Long> {
    fun findByUserIdAndDeviceId(userId: Long, deviceId: Long): DevicePermission?
    fun countByDeviceId(deviceId: Long): Long
    fun findByDeviceId(deviceId: Long): List<DevicePermission>
    fun findByDeviceId(deviceId: Long, pageable: Pageable): List<DevicePermission>
    fun findByUserId(userId: Long): List<DevicePermission>
    fun findByUserId(id: Long, pageable: Pageable): List<DevicePermission>
}