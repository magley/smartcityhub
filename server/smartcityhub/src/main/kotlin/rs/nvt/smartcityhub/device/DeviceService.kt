package rs.nvt.smartcityhub.device

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import rs.nvt.smartcityhub.config.influxdb.PingService
import rs.nvt.smartcityhub.device.ac.AC
import rs.nvt.smartcityhub.device.derived.*
import rs.nvt.smartcityhub.device.ac.ACService
import rs.nvt.smartcityhub.device.dht.DHT
import rs.nvt.smartcityhub.device.dto.*
import rs.nvt.smartcityhub.device.gate.Gate
import rs.nvt.smartcityhub.device.lamp.Lamp
import rs.nvt.smartcityhub.device.sprinklers.Sprinklers
import rs.nvt.smartcityhub.device.wash.WashMachine
import rs.nvt.smartcityhub.exception.EntityNotFoundException
import rs.nvt.smartcityhub.property.Property
import rs.nvt.smartcityhub.util.ImageSaveMode
import rs.nvt.smartcityhub.util.saveBase64ToImage

@Service
class DeviceService : IDeviceService {
    @Autowired lateinit var deviceRepo: IDeviceRepo
    @Autowired lateinit var pingService: PingService
    @Autowired lateinit var acService: ACService
    var logger: Logger = LoggerFactory.getLogger(DeviceService::class.java)

    override fun findAll(): List<Device> {
        return deviceRepo.findAll()
    }

    override fun find(id: Long): Device {
        return deviceRepo.findById(id).orElseThrow { EntityNotFoundException(Device::class, id) }
    }

    override fun findDevicesByPropertyId(id: Long): List<Device> {
        return deviceRepo.findAllDevicesByPropertyId(id)
    }

    override fun findDevicesByPropertyId(id: Long, p: Pageable): List<Device> {
        return deviceRepo.findByPropertyId(id, p)
    }

    override fun registerDevice(dto: DeviceRegisterDTO, propertyCached: Property): Device {
        val device: Device = when (dto.type) {
            DeviceType.dht -> DHT(dto, propertyCached)
            DeviceType.ac -> AC(dto, propertyCached)
            DeviceType.wash -> WashMachine(dto, propertyCached)
            DeviceType.lamp -> Lamp(dto, propertyCached)
            DeviceType.gate -> Gate(dto, propertyCached)
            DeviceType.sprinkler -> Sprinklers(dto, propertyCached)
            DeviceType.battery -> Battery(dto, propertyCached)
            DeviceType.solar -> SolarPanel(dto, propertyCached)
            DeviceType.charger -> Charger(dto, propertyCached)
        }
        deviceRepo.save(device)

        saveBase64ToImage(dto.imageB64, "device_${device.id}", ImageSaveMode.PHOTO)

        return device
    }

    @Scheduled(fixedRate = 5000)
    fun markOfflineDevices() {
        // TODO: If we have 100K devices, this becomes too slow.
        // The bottleneck would probably be fetching from the DB.
        // Optimization 1: Cache online devices in-memory. When
        // a device does online, add it to the list. When it goes
        // offline, remove it from the list. Won't help if most
        // devices are online most of the time.
        // Optimization 2: Write a custom HQL query. I don't think
        // hibernate can do getAllWhereIdNotIn(List<Long> ids).

        val onlineDevicesId = pingService.getOnlineDevices().map { d -> d.deviceId }
        val allDevices = findAll()
        val allDevicesId = allDevices.map { d -> d.id.toString() }
        val offlineDevicesId = allDevicesId.minus(onlineDevicesId.toSet())

        for (deviceId in offlineDevicesId) {
            val device = allDevices.find { d -> d.id.toString() == deviceId } ?: continue
            device.online = false
            deviceRepo.save(device)
        }
        for (deviceId in onlineDevicesId) {
            val device = allDevices.find { d -> d.id.toString() == deviceId } ?: continue
            device.online = true
            deviceRepo.save(device)
        }
    }

    @Transactional
    override fun LOADTEST_generate(property: Property, howMany: Int) {
        val img_b64 = "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII="
        for (i in 0..howMany) {
            val dto = DeviceRegisterDTO(property.id, DeviceType.ac, Device.PowerSupply.Network, 100, img_b64)
            registerDevice(dto, property)
        }
    }

    @Transactional
    override fun LOADTEST_generate(property: Property, howMany: Int, type: DeviceType) {
        val img_b64 = "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII="
        for (i in 0..howMany) {
            val dto = DeviceRegisterDTO(property.id, type, Device.PowerSupply.Network, 100, img_b64)
            registerDevice(dto, property)
        }
    }
}