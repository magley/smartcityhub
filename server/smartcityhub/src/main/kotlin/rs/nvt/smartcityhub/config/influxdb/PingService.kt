package rs.nvt.smartcityhub.config.influxdb

import com.influxdb.annotations.Column
import com.influxdb.annotations.Measurement
import com.influxdb.client.InfluxDBClient
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import rs.nvt.smartcityhub.device.DeviceType
import java.time.Instant


// TODO: Can this be a data class? It needs an empty ctor though.
@Measurement(name = "ping")
class PingMeasurement {
    @Column(name = "_measurement", measurement = true) var measurement: String = "What?!"
    @Column(name="device_type", tag = true) lateinit var deviceType: DeviceType
    @Column(name="device_id", tag = true) var deviceId: String = ""
    @Column(name="property_id", tag = true) var propertyId: String = ""
    @Column(name="_time", timestamp = true) lateinit var time: Instant
}

@Service
class PingService {
    @Autowired lateinit var influxDBClient: InfluxDBClient

    fun getOnlineDevices(): List<PingMeasurement> {
        val query = """
            from(bucket: "${InfluxDbConfig.BUCKET_NVT_VAL}")
                |> range(start: -20s)
                |> filter(fn: (r) => r._measurement == "ping")
                |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
                |> group(columns: ["device_id"])
                |> last(column: "device_id")
        """.trimIndent().format()

        return doQuery(query)
    }

    private fun doQuery(query: String): List<PingMeasurement> {
        return influxDBClient
            .queryApi
            .query(query, PingMeasurement::class.java)
    }
}