package rs.nvt.smartcityhub.permission

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import rs.nvt.smartcityhub.device.Device
import rs.nvt.smartcityhub.device.IDeviceService
import rs.nvt.smartcityhub.exception.EntityNotFoundException
import rs.nvt.smartcityhub.exception.RequestException
import rs.nvt.smartcityhub.permission.dto.NewPermissionDTO
import rs.nvt.smartcityhub.permission.dto.PermissionDTO
import rs.nvt.smartcityhub.permission.dto.PermissionTypeDTO
import rs.nvt.smartcityhub.permission.dto.RevokePermissionDTO
import rs.nvt.smartcityhub.property.IPropertyService
import rs.nvt.smartcityhub.property.Property
import rs.nvt.smartcityhub.user.IUserService
import rs.nvt.smartcityhub.user.User

@Service
class PermissionService : IPermissionService {
    @Autowired private lateinit var userService: IUserService
    @Autowired private lateinit var propertyService: IPropertyService
    @Autowired private lateinit var deviceService: IDeviceService
    @Autowired private lateinit var propertyPermissionRepo: IPropertyPermissionRepo
    @Autowired private lateinit var devicePermissionRepo: IDevicePermissionRepo

    override fun newPermission(dto: NewPermissionDTO) {
        val user = userService.find(dto.userId) ?: throw EntityNotFoundException(User::class)
        if (user.role != User.Role.CLIENT) {
            throw EntityNotFoundException(User::class)
        }

        when (dto.entityType) {
            PermissionTypeDTO.property -> newPropertyPermission(dto.entityId, user)
            PermissionTypeDTO.device -> newDevicePermission(dto.entityId, user)
        }
    }

    override fun revokePermission(dto: RevokePermissionDTO) {
        val user = userService.find(dto.userId) ?: throw EntityNotFoundException(User::class)
        if (user.role != User.Role.CLIENT) {
            throw EntityNotFoundException(User::class)
        }

        when (dto.entityType) {
            PermissionTypeDTO.property -> revokePropertyPermission(dto.entityId, user)
            PermissionTypeDTO.device -> revokeDevicePermission(dto.entityId, user)
        }
    }

    override fun getPropertyPermissions(property: Property): List<PermissionDTO> {
        return propertyPermissionRepo.findByPropertyId(property.id).map { p -> PermissionDTO(p) }
    }

    override fun getPropertyPermissions(property: Property, p: Pageable): List<PermissionDTO> {
        return propertyPermissionRepo.findByPropertyId(property.id, p).map { p -> PermissionDTO(p) }
    }

    override fun getPropertyPermissions(user: User): List<PropertyPermission> {
        return propertyPermissionRepo.findByUserId(user.id)
    }

    override fun getPropertyPermissions(user: User, p: Pageable): List<PropertyPermission> {
        return propertyPermissionRepo.findByUserId(user.id, p)
    }

    override fun getPropertyPermissionsCount(property: Property): Long {
        return propertyPermissionRepo.countByPropertyId(property.id)
    }

    override fun getDevicePermissions(device: Device): List<PermissionDTO> {
        return devicePermissionRepo.findByDeviceId(device.id).map { p -> PermissionDTO(p) }
    }

    override fun getDevicePermissions(device: Device, p: Pageable): List<PermissionDTO> {
        return devicePermissionRepo.findByDeviceId(device.id, p).map { p -> PermissionDTO(p) }
    }

    override fun getDevicePermissions(user: User): List<DevicePermission> {
        return devicePermissionRepo.findByUserId(user.id)
    }

    override fun getDevicePermissions(user: User, p: Pageable): List<DevicePermission> {
        return devicePermissionRepo.findByUserId(user.id, p)
    }

    override fun getDevicePermissionsCount(device: Device): Long {
        return devicePermissionRepo.countByDeviceId(device.id)
    }

    internal fun newPropertyPermission(id: Long, user: User) {
        if (propertyPermissionRepo.findByUserIdAndPropertyId(user.id, id) != null) {
            throw RequestException(HttpStatus.BAD_REQUEST, "User already has access to this property")
        }

        val property = propertyService.find(id) ?: throw EntityNotFoundException(Property::class)

        if (user.id == property.owner.id) {
            throw RequestException(HttpStatus.BAD_REQUEST, "User already has access to this property")
        }

        val permission = PropertyPermission()
        permission.user = user
        permission.property = property
        propertyPermissionRepo.save(permission)
    }

    internal fun newDevicePermission(id: Long, user: User) {
        if (devicePermissionRepo.findByUserIdAndDeviceId(user.id, id) != null) {
            throw RequestException(HttpStatus.BAD_REQUEST, "User already has access to this device")
        }

        val device = deviceService.find(id) // ?: throw EntityNotFoundException(Device::class)

        if (user.id == device.property.owner.id) {
            throw RequestException(HttpStatus.BAD_REQUEST, "User already has access to this device")
        }

        val permission = DevicePermission()
        permission.user = user
        permission.device = device
        devicePermissionRepo.save(permission)
    }

    @Transactional
    internal fun revokePropertyPermission(id: Long, user: User) {
        val permission = propertyPermissionRepo.findByUserIdAndPropertyId(user.id, id) ?: throw EntityNotFoundException(PropertyPermission::class)
        propertyPermissionRepo.delete(permission)

        for (device in deviceService.findDevicesByPropertyId(id)) {
            val perm = devicePermissionRepo.findByUserIdAndDeviceId(user.id, id) ?: continue
            devicePermissionRepo.delete(perm)
        }
    }

    internal fun revokeDevicePermission(id: Long, user: User) {
        val permission = devicePermissionRepo.findByUserIdAndDeviceId(user.id, id) ?: throw EntityNotFoundException(DevicePermission::class)
        devicePermissionRepo.delete(permission)
    }
}