package rs.nvt.smartcityhub.device.dht.dto

import java.time.Instant

/**
 * `DHTHistoryItemDTO` is a single item in the response body for a DHT history request.
 */
data class DHTHistoryItemDTO(val timestamp: Instant, val temperature: Double, val humidity: Double) {
    constructor(measurement: DHTHistoryItemMeasurementDTO) : this(measurement.timestamp, measurement.temperature, measurement.humidity) {}
}