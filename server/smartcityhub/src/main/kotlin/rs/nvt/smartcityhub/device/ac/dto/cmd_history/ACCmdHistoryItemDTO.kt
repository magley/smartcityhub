package rs.nvt.smartcityhub.device.ac.dto.cmd_history

import com.influxdb.annotations.Column
import rs.nvt.smartcityhub.device.ac.ACCommandType
import rs.nvt.smartcityhub.device.ac.ACMode
import java.time.Instant

/**
 * `ACCmdHistoryItemDTO` is a single command stored in the time-series database.
 */
class ACCmdHistoryItemDTO {
    @Column(name = "_time", timestamp = true) lateinit var timestamp: Instant
    @Column(name = "user_id") var userId: Long = -1
    @Column(name = "type", tag = true) lateinit var type: ACCommandType
    @Column(name = "uuid") lateinit var uuid: String
    @Column(name = "status", tag = true) var status: Boolean = false
    @Column(name = "is_on", tag = true) var isOn: Boolean? = null
    @Column(name = "mode", tag = true) var mode: ACMode? = null
    @Column(name = "temperature") var temperature: Double? = null
    @Column(name = "config") var config: String? = null
}