package rs.nvt.smartcityhub.device.common

import com.influxdb.client.InfluxDBClient
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import rs.nvt.smartcityhub.config.influxdb.InfluxDbConfig
import rs.nvt.smartcityhub.device.dto.DeviceMeasurementHistoryMode
import rs.nvt.smartcityhub.device.dto.DeviceMeasurementHistoryMode.*
import rs.nvt.smartcityhub.device.dto.DeviceMeasurementHistoryRequestBodyDTO
import rs.nvt.smartcityhub.exception.RequestException
import java.time.LocalDate
import java.time.Period
import java.time.ZoneId
import java.util.*


@Service
class DeviceMeasurementServiceCommon {
    @Autowired private lateinit var influxDBClient: InfluxDBClient

    private fun getMeasurementHistoryQueryFromNow(
        deviceId: Long,
        historyMode: DeviceMeasurementHistoryMode,
        measurementName: String,
        bucketName: String
    ): String {
        val timeLimit = DeviceMeasurementHistoryMode.toMiniString(historyMode)
        return """
        from(bucket: "${bucketName}")
            |> range(start: -$timeLimit)
            |> filter(fn: (r) => r._measurement == "$measurementName")
            |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
            |> filter(fn: (r) => r.device_id == "$deviceId")
        """.trimIndent().format()
    }

    private fun getMeasurementHistoryQueryInRange(
        deviceId: Long,
        date1: Date?,
        date2: Date?,
        measurementName: String,
        bucketName: String
    ): String {
        if (date1 == null || date2 == null) {
            throw RequestException(HttpStatus.BAD_REQUEST, "Please select a start and end date")
        }
        if (date1 == date2 || date1 > date2) {
            throw RequestException(
                HttpStatus.BAD_REQUEST,
                "Invalid range. Start date must be strictly BEFORE end date."
            )
        }
        val time1 = date1.time / 1000
        val time2 = date2.time / 1000

        return """
            from(bucket: "${bucketName}")
                |> range(start: $time1, stop: $time2)
                |> filter(fn: (r) => r._measurement == "$measurementName")
                |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
                |> filter(fn: (r) => r.device_id == "$deviceId")
            """.trimIndent().format()
    }

    private fun getMeasurementHistoryQuery(
        deviceId: Long,
        dto: DeviceMeasurementHistoryRequestBodyDTO,
        measurementName: String,
        bucketName: String
    ): String {
        if (dto.mode != modeCustom) {
            return getMeasurementHistoryQueryFromNow(deviceId, dto.mode, measurementName, bucketName)
        } else {
            return getMeasurementHistoryQueryInRange(deviceId, dto.date1, dto.date2, measurementName, bucketName)
        }
    }

    fun <T> getMeasurementHistory(
        deviceId: Long,
        dto: DeviceMeasurementHistoryRequestBodyDTO,
        measurementName: String,
        clazz: Class<T>,
        readFromDownsampled: Boolean = false
    ): List<T> {
        val bucket = when (readFromDownsampled) {
            true -> InfluxDbConfig.BUCKET_NVT_DOWNSAMPELD
            false -> InfluxDbConfig.BUCKET_NVT_VAL
        }

        val query = getMeasurementHistoryQuery(deviceId, dto, measurementName, bucket)
        return influxDBClient.queryApi.query(query, clazz)
    }

    private fun convertToLocalDateViaInstant(dateToConvert: Date): LocalDate {
        return dateToConvert.toInstant()
            .atZone(ZoneId.systemDefault())
            .toLocalDate()
    }

    fun <T> getMeasurementHistory2(deviceId: Long,
                                   dto: DeviceMeasurementHistoryRequestBodyDTO,
                                   measurementName: String,
                                   clazz: Class<T>) : List<T> {

        if (dto.mode != modeCustom) {
            val windowSize = when (dto.mode) {
                mode6h -> "29m"
                mode12h -> "29m"
                mode24h -> "29m"
                mode1w -> "5h"
                mode1m -> "23h"
                modeCustom -> TODO()
            }
            val query = """
                from(bucket: "nvtBucket")
                    |> range(start: -${DeviceMeasurementHistoryMode.toMiniString(dto.mode)})
                    |> filter(fn: (r) => r["_measurement"] == "${measurementName}")
                    |> filter(fn: (r) => r["device_id"] == "${deviceId}")
                    |> aggregateWindow(every: ${windowSize}, fn: mean, createEmpty: false)
                    |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")      
            """
            println(query)
            return influxDBClient.queryApi.query(query, clazz)
        } else {
            val time1 = dto.date1!!.time / 1000
            val time2 = dto.date2!!.time / 1000
            val duration = Period.between(convertToLocalDateViaInstant(dto.date1), convertToLocalDateViaInstant(dto.date2))

            var windowSize = "29m"
            if (duration.days > 1) {
                windowSize = "59m"
            }
            if (duration.days > 7) {
                windowSize = "5h"
            }
            if (duration.days > 13) {
                windowSize = "23h"
            }
            if (duration.days > 30) {
                windowSize = "3d"
            }
            if (duration.days > 150) {
                windowSize = "8d"
            }
            if (duration.years > 1) {
                windowSize = "11mo"
            }

            val query = """
                from(bucket: "nvtBucket")
                    |> range(start: ${time1}, stop: ${time2})
                    |> filter(fn: (r) => r["_measurement"] == "${measurementName}")
                    |> filter(fn: (r) => r["device_id"] == "${deviceId}")
                    |> aggregateWindow(every: ${windowSize}, fn: mean, createEmpty: false)
                    |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")      
            """
            println(query)
            return influxDBClient.queryApi.query(query, clazz)
        }
    }
}