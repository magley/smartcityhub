package rs.nvt.smartcityhub.property

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import rs.nvt.smartcityhub.city.CityService
import rs.nvt.smartcityhub.exception.EntityNotFoundException
import rs.nvt.smartcityhub.property.dto.RequestPropertyDTO
import rs.nvt.smartcityhub.user.IUserService
import rs.nvt.smartcityhub.user.User
import rs.nvt.smartcityhub.util.SendgridUtil

@Service
class PropertyService : IPropertyService {
    @Autowired lateinit var propertyRepo: IPropertyRepo
    @Autowired lateinit var sendgridUtil: SendgridUtil
    @Autowired lateinit var userService: IUserService
    @Autowired lateinit var cityService: CityService

    override fun findAllByOwnerId(ownerId: Long): List<Property> {
        return propertyRepo.findAllByOwnerId(ownerId)
    }
    override fun findAllApprovedByOwnerId(ownerId: Long): List<Property> {
        return propertyRepo.findAllByOwnerIdAndStatusState(ownerId, Status.State.APPROVED)
    }

    override fun findApprovedByOwnerId(ownerId: Long, p: Pageable): List<Property> {
        return propertyRepo.findByOwnerIdAndStatusState(ownerId, Status.State.APPROVED, p)
    }

    override fun findPendingByOwnerId(ownerId: Long, p: Pageable): List<Property> {
        return propertyRepo.findByOwnerIdAndStatusState(ownerId, Status.State.PENDING, p)
    }

    override fun findAll(): List<Property> = propertyRepo.findAll()
    override fun findAllPending(): List<Property> {
        return propertyRepo.findAllByStatusState(Status.State.PENDING)
    }

    override fun findAllPending(p: Pageable): List<Property> {
        return propertyRepo.findAllByStatusState(Status.State.PENDING, p)
    }

    override fun find(id: Long): Property? {
        return propertyRepo.findById(id).orElse(null)
    }
    override fun getApproved(id: Long): Property {
        return propertyRepo.findByIdAndStatusState(id, Status.State.APPROVED).orElseThrow { EntityNotFoundException(Property::class, id) }
    }
    override fun getPending(id: Long): Property {
        return propertyRepo.findByIdAndStatusState(id, Status.State.PENDING).orElseThrow { EntityNotFoundException(Property::class, id) }
    }
    override fun get(id: Long): Property {
        return propertyRepo.findById(id).orElseThrow { EntityNotFoundException(Property::class, id) }
    }

    override fun createProperty(property: Property): Property {
        return propertyRepo.save(property)
    }
    override fun deny(propertyId: Long, reason: String) {
        val property = this.getPending(propertyId)
        property.status = Status(Status.State.DENIED, reason)
        propertyRepo.save(property)
        sendgridUtil.sendPropertyRejectEmail(property.owner.name, property.address, reason)
    }
    override fun approve(propertyId: Long) {
        val property = this.getPending(propertyId)
        property.status = Status(Status.State.APPROVED, null)
        propertyRepo.save(property)
        sendgridUtil.sendPropertyApproveEmail(property.owner.name, property.address)
    }

    @Transactional
    override fun LOADTEST_generate_for_each_user(howMany: Int): List<Property> {
        var result: MutableList<Property> = mutableListOf()
        for (u in userService.findAllClients()) {
            val li = LOADTEST_generate_for_user(u, howMany)
            result = (result + li).toMutableList()
        }

        return result
    }

    override fun countAllPending(): Long {
        return propertyRepo.countByStatusState(Status.State.PENDING)
    }

    @Transactional
    fun LOADTEST_generate_for_user(u: User, howMany: Int): List<Property> {
        val city = cityService.findById(1)
        val result: MutableList<Property> = mutableListOf()
        for (i in 0..howMany) {
            val dto = RequestPropertyDTO(Property.Type.House, "", city.id, 123.0, 3, GeoCoordinate(), "")
            val p = Property(u, city, dto)
            p.status = Status(Status.State.APPROVED, "")
            createProperty(p)
            result.add(p)
        }

        return result
    }
}