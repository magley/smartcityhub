package rs.nvt.smartcityhub.device.ac

import io.swagger.v3.oas.annotations.Operation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import rs.nvt.smartcityhub.config.auth.IAuthenticationFacade
import rs.nvt.smartcityhub.device.*
import rs.nvt.smartcityhub.device.ac.dto.ACRegisterDTO
import rs.nvt.smartcityhub.device.ac.dto.cmd.ACCmdCustomModeDTO
import rs.nvt.smartcityhub.device.ac.dto.cmd.ACCmdModeDTO
import rs.nvt.smartcityhub.device.ac.dto.cmd.ACCmdStartDTO
import rs.nvt.smartcityhub.device.ac.dto.cmd.ACCmdTemperatureDTO
import rs.nvt.smartcityhub.device.ac.dto.cmd_history.ACCmdHistoryItemDTO
import rs.nvt.smartcityhub.device.common.DeviceServiceCommon
import rs.nvt.smartcityhub.device.dto.DeviceCmdHistoryMetaDTO
import rs.nvt.smartcityhub.device.dto.DeviceCmdHistoryRequestBodyDTO
import rs.nvt.smartcityhub.property.IPropertyService
import rs.nvt.smartcityhub.property.PropertyAccessControl

@RestController
@RequestMapping("/api/device/ac")
class ACController {
    @Autowired lateinit var deviceService: IDeviceService
    @Autowired lateinit var propertyService: IPropertyService
    @Autowired lateinit var propertyAccessControl: PropertyAccessControl
    @Autowired lateinit var authFacade: IAuthenticationFacade
    @Autowired lateinit var deviceAccessControl: DeviceAccessControl
    @Autowired lateinit var acService: ACService
    @Autowired lateinit var cmdService: DeviceServiceCommon

    @Operation(summary = "Register an air conditioner")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PostMapping()
    fun register(@RequestBody dto: ACRegisterDTO): ResponseEntity<Void> {
        val property = propertyService.getApproved(dto.base.propertyId)
        propertyAccessControl.assertOwnership(authFacade.getUser(), property)
        acService.registerAC(dto, property)
        return ResponseEntity(null, HttpStatus.NO_CONTENT)
    }

    @Operation(summary = "Set the configuration for this air conditioner")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PutMapping("/config")
    fun setCustomMode(@RequestBody dto: ACCmdCustomModeDTO): ResponseEntity<Void> {
        val device = getDeviceWithAssertion(dto.id)
        acService.setConfig(device, dto.customMode, dto.uuid)
        return ResponseEntity(null, HttpStatus.NO_CONTENT)
    }

    @Operation(summary = "Turn this air conditioner on/off")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PutMapping("/start")
    fun setStart(@RequestBody dto: ACCmdStartDTO): ResponseEntity<Void> {
        val device = getDeviceWithAssertion(dto.id)
        when (dto.start) {
            true -> acService.start(device, dto.uuid)
            false -> acService.stop(device, dto.uuid)
        }
        return ResponseEntity(null, HttpStatus.NO_CONTENT)
    }

    @Operation(summary = "Set the air conditioner's temperature")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PutMapping("/temperature")
    fun setTemperature(@RequestBody dto: ACCmdTemperatureDTO): ResponseEntity<Void> {
        val device = getDeviceWithAssertion(dto.id)
        acService.setTemperature(device, dto.temperature, dto.uuid)
        return ResponseEntity(null, HttpStatus.NO_CONTENT)
    }

    @Operation(summary = "Set the mode of operation for this air conditioner")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PutMapping("/mode")
    fun setMode(@RequestBody dto: ACCmdModeDTO): ResponseEntity<Void> {
        val device = getDeviceWithAssertion(dto.id)
        acService.setMode(device, dto.mode, dto.uuid)
        return ResponseEntity(null, HttpStatus.NO_CONTENT)
    }

    @Operation(summary = "Get air conditioner by ID")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @GetMapping("/{id}")
    fun get(@PathVariable id: Long): ResponseEntity<ACDTO> {
        val device = getDeviceWithAssertion(id)
        val result = ACDTO(device)
        return ResponseEntity(result, HttpStatus.OK)
    }

    @Operation(summary = "Get history of actions for this air conditioner")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PostMapping("/history")
    fun getHistory(@RequestBody dto: DeviceCmdHistoryRequestBodyDTO): ResponseEntity<*> {
        val device = getDeviceWithAssertion(dto.id)
        val result = acService.getHistory(device, dto)
        return ResponseEntity(result, HttpStatus.OK)
    }

    private fun getDeviceWithAssertion(id: Long): AC {
        val device = deviceService.find(id)
        deviceAccessControl.assertOwnership(authFacade.getUser(), device)
        deviceAccessControl.assertType(device, DeviceType.ac)
        return device as AC
    }
}