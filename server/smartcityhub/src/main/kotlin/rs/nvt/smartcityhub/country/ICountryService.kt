package rs.nvt.smartcityhub.country

interface ICountryService {
    fun findAll(): List<Country>
}