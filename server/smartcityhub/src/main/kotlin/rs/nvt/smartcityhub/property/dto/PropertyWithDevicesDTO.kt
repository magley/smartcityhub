package rs.nvt.smartcityhub.property.dto

import rs.nvt.smartcityhub.device.Device
import rs.nvt.smartcityhub.device.dto.DeviceDTO
import rs.nvt.smartcityhub.property.Property
import rs.nvt.smartcityhub.user.User

data class PropertyWithDevicesDTO(val property: PropertyDTO, val devices: List<DeviceDTO>) {
    constructor(property: Property, devices: List<Device>, requestSender: User): this(
        PropertyDTO(property, requestSender),
        devices.map { d -> DeviceDTO(d) }
    )
}