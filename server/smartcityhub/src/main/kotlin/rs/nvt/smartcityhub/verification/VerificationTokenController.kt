package rs.nvt.smartcityhub.verification

import io.swagger.v3.oas.annotations.Operation
import jakarta.annotation.security.PermitAll
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import rs.nvt.smartcityhub.user.IUserService

@RestController
@RequestMapping("/api/verification")
class VerificationTokenController {
    @Autowired
    lateinit var tokenService: IVerificationTokenService

    @Autowired
    lateinit var userService: IUserService

    @Operation(summary = "Verify user")
    @PermitAll
    @GetMapping("/{tokenKey}")
    fun verify(@PathVariable tokenKey: String): ResponseEntity<Void> {
        val token = tokenService.findByToken(tokenKey)

        tokenService.verify(token)
        userService.activateUser(token.user)

        return ResponseEntity(null, HttpStatus.NO_CONTENT)
    }
}