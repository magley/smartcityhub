package rs.nvt.smartcityhub.device.gate

import rs.nvt.smartcityhub.device.dto.DeviceCmdHistoryBaseDTO
import rs.nvt.smartcityhub.device.dto.DeviceCmdHistoryUserDataDTO

data class GateCmdHistoryOneOfDTO (
    var openState: GateOpen?,
    var mode: GateMode?,
    var override: GateOverride?,
    var plate: String?,
    var direction: GateDirection?,
    var event: GateSensorEvent?,
)

class GateCmdHistoryItemDTO_ : DeviceCmdHistoryBaseDTO {
    var type: GateCommandType
    var oneOf: GateCmdHistoryOneOfDTO

    constructor(dto: GateCmdHistoryItemDTO, u: DeviceCmdHistoryUserDataDTO) {
        timestamp = dto.timestamp
        status = dto.status
        uuid = dto.uuid
        user = u

        type = dto.type
        oneOf = GateCmdHistoryOneOfDTO(dto.openState, dto.mode, dto.override, dto.plate, dto.direction, dto.event)
    }
}