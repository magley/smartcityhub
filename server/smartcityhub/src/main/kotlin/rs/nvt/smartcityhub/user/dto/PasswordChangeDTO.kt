package rs.nvt.smartcityhub.user.dto

data class PasswordChangeDTO(val oldPassword: String, val newPassword: String)