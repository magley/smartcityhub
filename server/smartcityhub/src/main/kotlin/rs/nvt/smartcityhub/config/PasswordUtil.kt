package rs.nvt.smartcityhub.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component

@Component
class PasswordUtil {
    @Autowired
    lateinit var passwordEncoder: PasswordEncoder

    fun encode(plaintextPassword: String): String {
        return passwordEncoder.encode(plaintextPassword)
    }

    fun doPasswordsMatch(plaintextPassword: String, encodedPassword: String): Boolean {
        return passwordEncoder.matches(plaintextPassword, encodedPassword)
    }
}