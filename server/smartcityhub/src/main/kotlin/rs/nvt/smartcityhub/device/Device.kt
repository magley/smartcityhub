package rs.nvt.smartcityhub.device

import jakarta.persistence.*
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import rs.nvt.smartcityhub.device.dto.DeviceRegisterDTO
import rs.nvt.smartcityhub.property.Property

@Entity
@Cache(region = "Device", usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
abstract class Device {
    enum class PowerSupply {
        Battery, Network
    }

    constructor() {}

    constructor(dto: DeviceRegisterDTO, property: Property) {
        this.property = property
        this.powerSupply = dto.powerSupply
        this.powerDrainage = dto.powerDrainage
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    open var id: Long = 0

    @ManyToOne
    lateinit var property: Property

    @Column(nullable = false)
    open var powerSupply: PowerSupply = PowerSupply.Network

    @Column(nullable = false)
    open var powerDrainage: Int = 100

    @Column(nullable = false)
    open var online: Boolean = false

    abstract fun getType(): DeviceType
}