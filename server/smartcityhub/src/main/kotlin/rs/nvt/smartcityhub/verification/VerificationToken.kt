package rs.nvt.smartcityhub.verification

import jakarta.persistence.*
import rs.nvt.smartcityhub.user.User
import java.util.*

@Entity
class VerificationToken {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0

    @ManyToOne
    lateinit var user: User

    @Column(nullable = false)
    lateinit var token: String

    constructor() {}

    constructor(user: User) {
        this.user = user
        this.token = genStr()
    }

    private fun genStr(): String {
        val candidateChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        val sb = StringBuilder()
        val random = Random()
        for (i in 0 until 120) {
            sb.append(candidateChars[random.nextInt(candidateChars.length)])
        }

        return sb.toString()
    }
}