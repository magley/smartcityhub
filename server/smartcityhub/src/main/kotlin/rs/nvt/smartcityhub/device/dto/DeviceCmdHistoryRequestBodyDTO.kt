package rs.nvt.smartcityhub.device.dto

import rs.nvt.smartcityhub.util.InfluxPaginator
import java.util.*

/**
 * `DeviceCmdHistoryRequestBodyDTO` is the body of the HTTP request sent by the user to see the command history.
 */
data class DeviceCmdHistoryRequestBodyDTO (
    /**
     * ID of the device.
     */
    val id: Long,
    /**
     * List of users whose commands must be included in the response.
     *
     * **If empty**, all users are included, i.e. no additional filtering is done.
     *
     * **If non-empty**, the query filters by the issuing users.
     */
    val users: List<DeviceCmdHistoryUserDataDTO>,
    val pagination: InfluxPaginator,
    val minDate: Date,
    val maxDate: Date,
)