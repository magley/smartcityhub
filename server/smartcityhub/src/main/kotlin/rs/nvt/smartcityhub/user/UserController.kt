package rs.nvt.smartcityhub.user

import io.swagger.v3.oas.annotations.Operation
import jakarta.annotation.security.PermitAll
import jakarta.validation.Valid
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import rs.nvt.smartcityhub.config.auth.IAuthenticationFacade
import rs.nvt.smartcityhub.user.dto.ClientRegisterDTO
import rs.nvt.smartcityhub.user.dto.CreateUserDTO
import rs.nvt.smartcityhub.user.dto.PasswordChangeDTO
import rs.nvt.smartcityhub.user.dto.UserInfoDTO
import rs.nvt.smartcityhub.util.CollectionResultDTO

@RestController
@RequestMapping("/api/user")
class UserController {
    @Autowired
    private lateinit var userService: IUserService
    @Autowired
    private lateinit var authFacade: IAuthenticationFacade

    @Operation(summary = "Search for clients by name and last name")
    @PreAuthorize("hasRole('CLIENT')")
    @GetMapping("/all/clients/search")
    fun searchClients(@RequestParam(name = "search") searchQuery: String): ResponseEntity<Collection<UserInfoDTO>> {
        val result = userService.getClientSearch(searchQuery).map { u -> UserInfoDTO(u) }
        return ResponseEntity(result, HttpStatus.OK)
    }

    @Operation(summary = "[OBSOLETE] Get all clients")
    @PreAuthorize("hasRole('CLIENT')")
    @GetMapping("/all/clients")
    fun getAllClients(): ResponseEntity<List<UserInfoDTO>> {
        val result = userService.findAllClients().map{u -> UserInfoDTO(u) }
        return ResponseEntity(result, HttpStatus.OK)
    }

    @Operation(summary = "[OBSOLETE] Get all admins")
    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @GetMapping("/all/admins")
    fun getAllAdmins(): ResponseEntity<List<UserInfoDTO>> {
        val result = userService.findAllAdmins().map{u -> UserInfoDTO(u) }
        return ResponseEntity(result, HttpStatus.OK)
    }

    @Operation(summary = "Get admins")
    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @GetMapping("/all/admins/p")
    fun getAllAdminsPaginated(p: Pageable): ResponseEntity<CollectionResultDTO<UserInfoDTO>> {
        val result = userService.findAllAdmins(p).map{u -> UserInfoDTO(u) }
        val totalCount = userService.countAllAdmins()
        return ResponseEntity(CollectionResultDTO(totalCount, result), HttpStatus.OK)
    }

    @Operation(summary = "[INTERNAL] Get user by ID")
    @PermitAll
    @GetMapping("{id}")
    fun getUserById(@PathVariable id: Long): ResponseEntity<UserInfoDTO> {
        val result = UserInfoDTO(userService.get(id))
        return ResponseEntity(result, HttpStatus.OK)
    }

    @Operation(summary = "Register a new admin")
    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @PostMapping("/admin")
    fun registerAdmin(@RequestBody dto: CreateUserDTO): ResponseEntity<Void> {
        userService.registerAdmin(dto)
        return ResponseEntity(null, HttpStatus.NO_CONTENT)
    }

    @Operation(summary = "Register a new client")
    @PermitAll()
    @PostMapping("/client")
    fun registerClient(@Valid @RequestBody dto: ClientRegisterDTO): ResponseEntity<Void> {
        userService.registerClient(dto)
        return ResponseEntity(null, HttpStatus.NO_CONTENT)
    }

    @Operation(summary = "Delete a user")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    @DeleteMapping("{id}")
    fun deleteUser(@PathVariable id: Long): ResponseEntity<Void> {
        userService.remove(id)
        return ResponseEntity(null, HttpStatus.NO_CONTENT)
    }

    @Operation(summary = "Change my password")
    @PreAuthorize("hasAnyRole('ADMIN', 'SUPER_ADMIN', 'CLIENT')")
    @PostMapping("/changepass")
    fun changePassword(@RequestBody dto: PasswordChangeDTO): ResponseEntity<Void> {
        val user = authFacade.getUser()
        userService.changePassword(user, dto)
        return ResponseEntity(null, HttpStatus.NO_CONTENT)
    }
}