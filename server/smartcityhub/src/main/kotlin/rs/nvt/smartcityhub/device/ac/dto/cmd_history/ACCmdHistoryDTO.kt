package rs.nvt.smartcityhub.device.ac.dto.cmd_history

import rs.nvt.smartcityhub.device.ac.ACCommandType
import rs.nvt.smartcityhub.device.ac.ACMode
import rs.nvt.smartcityhub.device.dto.DeviceCmdHistoryBaseDTO
import rs.nvt.smartcityhub.device.dto.DeviceCmdHistoryUserDataDTO

data class ACCmdHistoryOneOfDTO (
    var isOn: Boolean?,
    var mode: ACMode?,
    var temperature: Double?,
    var config: String?,
)

 class ACCmdHistoryItemDTO_ : DeviceCmdHistoryBaseDTO {
    var type: ACCommandType
    var oneOf: ACCmdHistoryOneOfDTO

    constructor(dto: ACCmdHistoryItemDTO, u: DeviceCmdHistoryUserDataDTO) {
        timestamp = dto.timestamp
        status = dto.status
        uuid = dto.uuid
        user = u

        type = dto.type
        oneOf = ACCmdHistoryOneOfDTO(dto.isOn, dto.mode, dto.temperature, dto.config)
    }
}