package rs.nvt.smartcityhub.util

import java.awt.Color
import java.awt.image.BufferedImage
import java.io.ByteArrayInputStream
import java.io.File
import java.util.*
import javax.imageio.ImageIO

enum class ImageSaveMode {
    AVATAR,
    PHOTO,
}

// TODO: Externalize so that both nginx and the server use the same var.
private const val IMAGES_DIR = "D:/nvt-data/images"

private fun getFilePath(filenamePartial: String, saveMode: ImageSaveMode, type: String): String {
    return "${IMAGES_DIR}/${saveMode.toString().lowercase()}/${filenamePartial}.${type}"
}

fun saveBase64ToImage(base64img: String, filenamePartial: String, saveMode: ImageSaveMode) {
    val image: ByteArray = Base64.getDecoder().decode(base64img.split(",").last())
    val img = ImageIO.read(ByteArrayInputStream(image))
    // https://stackoverflow.com/a/60677340
    val imgNoTransparent = BufferedImage(img.width, img.height, BufferedImage.TYPE_INT_RGB)
    imgNoTransparent.createGraphics().drawImage(img, 0, 0, Color.WHITE, null)

    val file = File(getFilePath(filenamePartial, saveMode, "jpg"))
    ImageIO.write(imgNoTransparent, "jpg", file)
}
