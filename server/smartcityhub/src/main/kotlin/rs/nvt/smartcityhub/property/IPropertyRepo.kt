package rs.nvt.smartcityhub.property

import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface IPropertyRepo : JpaRepository<Property, Long> {
    fun findAllByOwnerId(ownerId: Long): List<Property>
    fun findAllByOwnerIdAndStatusState(ownerId: Long, state: Status.State): List<Property>
    fun findByOwnerIdAndStatusState(ownerId: Long, state: Status.State, pageable: Pageable): List<Property>
    fun findByIdAndStatusState(id: Long, state: Status.State): Optional<Property>
    fun findAllByStatusState(state: Status.State): List<Property>
    fun findAllByStatusState(state: Status.State, pageable: Pageable): List<Property>
    fun countByStatusState(state: Status.State): Long
}