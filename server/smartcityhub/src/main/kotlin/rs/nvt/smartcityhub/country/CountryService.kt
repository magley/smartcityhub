package rs.nvt.smartcityhub.country

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CountryService: ICountryService {

    @Autowired
    lateinit var countryRepo: ICountryRepo

    override fun findAll(): List<Country> {
        return countryRepo.findAll()
    }
}