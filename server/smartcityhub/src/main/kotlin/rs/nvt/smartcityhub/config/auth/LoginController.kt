package rs.nvt.smartcityhub.config.auth

import io.swagger.v3.oas.annotations.Operation
import jakarta.annotation.security.PermitAll
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import rs.nvt.smartcityhub.user.User

@RestController
@RequestMapping("/api/session")
class LoginController {
    @Autowired
    private lateinit var authManager: AuthenticationManager

    @Autowired
    private lateinit var jwtTokenUtil: JwtTokenUtil

    @Operation(summary = "Sign in")
    @PermitAll
    @PostMapping("/login")
    fun login(@RequestBody loginDTO: LoginDTO): ResponseEntity<String> {
        try {
            val authToken = UsernamePasswordAuthenticationToken(loginDTO.username, loginDTO.password)
            val auth = authManager.authenticate(authToken)
            val user: User = auth.principal as User

            val jwt: String = jwtTokenUtil.generateJwt(user.username, user.id, user.role.toString(), user.shouldChangeCurrentPassword)

            return ResponseEntity.ok(jwt)
        } catch (ex: BadCredentialsException) {
            return ResponseEntity(null, HttpStatus.NOT_FOUND)
        }
    }
}