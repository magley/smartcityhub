package rs.nvt.smartcityhub.device.ac.dto.cmd

import rs.nvt.smartcityhub.device.ac.dto.ACCustomModeItemDTO

/**
 * `ACCmdCustomModeDTO` is the HTTP request payload for a "set custom mode" command.
 */
data class ACCmdCustomModeDTO (
    val id: Long,
    val customMode: List<ACCustomModeItemDTO>,
    val uuid: String
)