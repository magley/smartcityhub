package rs.nvt.smartcityhub.city

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ICityRepo: JpaRepository<City, Long>