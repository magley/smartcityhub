package rs.nvt.smartcityhub.verification

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import rs.nvt.smartcityhub.exception.EntityNotFoundException
import rs.nvt.smartcityhub.user.User

@Service
class VerificationTokenService : IVerificationTokenService {
    @Autowired
    lateinit var verificationTokenRepo: IVerificationTokenRepo

    override fun findFor(user: User): VerificationToken {
        return verificationTokenRepo.findByUser(user).orElseThrow{ EntityNotFoundException(VerificationToken::class, "No token for this user") }
    }

    override fun findByToken(token: String): VerificationToken {
        return verificationTokenRepo.findByToken(token).orElseThrow{ EntityNotFoundException(VerificationToken::class, "Invalid or expired token") }
    }

    override fun verify(token: VerificationToken) {
        verificationTokenRepo.delete(token)
    }

    override fun create(user: User): VerificationToken {
        val token = VerificationToken(user)
        verificationTokenRepo.save(token)
        return token
    }
}