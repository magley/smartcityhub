package rs.nvt.smartcityhub.device.wash

import jakarta.persistence.ElementCollection
import jakarta.persistence.Entity
import jakarta.persistence.EnumType
import jakarta.persistence.Enumerated
import rs.nvt.smartcityhub.device.Device
import rs.nvt.smartcityhub.device.dto.DeviceRegisterDTO
import rs.nvt.smartcityhub.device.DeviceType
import rs.nvt.smartcityhub.device.ac.ACMode
import rs.nvt.smartcityhub.property.Property

enum class WashMachineMode {
    off,
    cotton_95,
    cotton_60,
    cotton_40,
    synthetic_60,
    synthetic_40,
    wool_40,
}

enum class WashMachineCommandType {
    mode,
    schedule,
    removeSchedule
}

@Entity
class WashMachine : Device {
    constructor() {
    }

    constructor(dto: DeviceRegisterDTO, property: Property) : super(dto, property) {
    }
    override fun getType(): DeviceType = DeviceType.wash

    @ElementCollection(targetClass = WashMachineMode::class)
    @Enumerated(EnumType.STRING)
    lateinit var supportedModes: List<WashMachineMode>
}