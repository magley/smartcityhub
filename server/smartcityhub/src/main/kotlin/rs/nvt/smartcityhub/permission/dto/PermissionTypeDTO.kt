package rs.nvt.smartcityhub.permission.dto

enum class PermissionTypeDTO {
    property,
    device,
}