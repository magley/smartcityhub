package rs.nvt.smartcityhub.device.sprinklers

import rs.nvt.smartcityhub.device.dto.DeviceCmdHistoryBaseDTO
import rs.nvt.smartcityhub.device.dto.DeviceCmdHistoryUserDataDTO

data class SprinklersCmdHistoryOneOfDTO (
    var is_on: Boolean?,
    var triggers: String?,
)

class SprinklersCmdHistoryItemDTO_ : DeviceCmdHistoryBaseDTO {
    var type: SprinklersCommandType
    var oneOf: SprinklersCmdHistoryOneOfDTO

    constructor(dto: SprinklersCmdHistoryItemDTO, u: DeviceCmdHistoryUserDataDTO) {
        timestamp = dto.timestamp
        status = dto.status
        uuid = dto.uuid
        user = u

        type = dto.type
        oneOf = SprinklersCmdHistoryOneOfDTO(dto.isOn, dto.triggers)
    }
}