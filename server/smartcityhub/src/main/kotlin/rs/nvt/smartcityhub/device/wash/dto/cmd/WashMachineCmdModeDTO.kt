package rs.nvt.smartcityhub.device.wash.dto.cmd

import rs.nvt.smartcityhub.device.wash.WashMachineMode

data class WashMachineCmdModeDTO (
    val id: Long,
    val uuid: String,
    val mode: WashMachineMode
)
