package rs.nvt.smartcityhub.device.dto

import jakarta.validation.constraints.NotNull
import rs.nvt.smartcityhub.device.Device
import rs.nvt.smartcityhub.device.DeviceType
import rs.nvt.smartcityhub.util.validator.Base64MaxSize

data class DeviceRegisterDTO(
    @NotNull val propertyId: Long,
    @NotNull val type: DeviceType,
    @NotNull val powerSupply: Device.PowerSupply,
    @NotNull val powerDrainage: Int,
    @NotNull @Base64MaxSize(maxSizeInBytes = 1024 * 1024 * 5) val imageB64: String,
)