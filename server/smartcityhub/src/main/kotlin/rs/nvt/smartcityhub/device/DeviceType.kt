package rs.nvt.smartcityhub.device

enum class DeviceType {
    ac,
    dht,
    wash,
    lamp,
    gate,
    sprinkler,
    battery,
    solar,
    charger,
}