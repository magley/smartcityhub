package rs.nvt.smartcityhub.util

import com.sendgrid.*
import okhttp3.internal.notify
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.Marker
import org.slf4j.MarkerFactory
import java.io.File
import java.io.IOException
import java.lang.RuntimeException
import java.util.Dictionary


enum class EmailTemplateKey {
    Verification,
    PropertyApprove,
    PropertyDeny,
    None
}

class EmailTemplatePair (
    val fname: String,
    val fnameTxt: String,
    var template: String?,
    var templateTxt: String?,
)

class SendgridUtil {
    private val env_vars: MutableMap<String, String>

    private var logger: Logger = LoggerFactory.getLogger(SendgridUtil::class.java)
    private var marker: Marker = MarkerFactory.getMarker("Email")

    private var templateDict: Map<EmailTemplateKey, EmailTemplatePair> = mapOf(
        EmailTemplateKey.Verification to EmailTemplatePair("data/email/verification.html", "data/email/verification.txt", null, null),
        EmailTemplateKey.PropertyApprove to EmailTemplatePair("data/email/propertyApprove.html", "data/email/propertyApprove.txt",null, null),
        EmailTemplateKey.PropertyDeny to EmailTemplatePair("data/email/propertyDeny.html", "data/email/propertyDeny.txt",null, null),
    )

    companion object {
        private const val KEY_APIKEY = "KEY"
        private const val KEY_FROM = "FROM"
        private const val ENV_LOCATION = "./config/sendgrid.env"
        private const val ACTUALLY_SEND = false
    }

    init {
        env_vars = HashMap()

        val allLines = File(ENV_LOCATION).useLines { it.toList() }
        for (line in allLines) {
            val parts = line.split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            env_vars[parts[0]] = parts[1]
        }
    }

    private fun buildAndSendMail(key: EmailTemplateKey, replaceKey: List<String>, replaceValue: List<String>, subject: String) {
        val t = templateDict[key] ?: throw RuntimeException("No template for ${key}")
        var templateHtml = t.template
        var templateTxt = t.templateTxt
        if (templateHtml == null) {
            templateHtml = File(t.fname).readText(Charsets.UTF_8)
            t.template = templateHtml
        }
        if (templateTxt == null) {
            templateTxt = File(t.fnameTxt).readText(Charsets.UTF_8)
            t.template = templateTxt
        }

        if (replaceKey.size != replaceValue.size) {
            throw IllegalArgumentException("Email template has non-matching pairs of replaceKey and replaceValue!")
        }

        var bodyHtml = templateHtml!!
        var bodyTxt = templateTxt!!
        for (i in replaceKey.indices) {
            bodyHtml = bodyHtml.replace(replaceKey[i], replaceValue[i])
            bodyTxt = bodyTxt.replace(replaceKey[i], replaceValue[i])
        }
        sendMultipartEmail(subject, bodyHtml, bodyTxt)
    }

    private fun sendMultipartEmail(subject: String, bodyHtml: String, bodyTxt: String) {
        logger.warn(marker, "Logging email contents:")
        logger.warn(marker, bodyHtml)
        logger.warn(marker, bodyTxt)

        if (!ACTUALLY_SEND) {
            logger.warn(marker, "ACTUALLY_SEND is false: Not sending the email.")
            return
        }
        val to_email = env_vars[KEY_FROM] // We test with dummy emails so send everything to 1 email.
        val from = Email(env_vars[KEY_FROM])
        val to = Email(to_email)
        val contentTxt = Content("text/plain", bodyTxt)
        val contentHtml = Content("text/html", bodyHtml)
        val mail = Mail(from, subject, to, contentTxt)
        mail.addContent(contentHtml)
        val sg = SendGrid(env_vars[KEY_APIKEY])
        val request = Request()
        request.method = Method.POST
        request.endpoint = "mail/send"
        try {
            request.body = mail.build()
            val response: Response = sg.api(request)
            logger.info(marker, "Sending email to $to_email of subject $subject")
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    fun sendVerificationEmail(token: String) {
        buildAndSendMail(EmailTemplateKey.Verification, listOf("[TOKEN]"), listOf(token), "Smartcityhub | Verify your account")
    }

    fun sendPropertyRejectEmail(username: String, address: String, rejectReason: String) {
        buildAndSendMail(EmailTemplateKey.PropertyDeny, listOf("[[USER]]", "[[ADDRESS]]", "[[REASON]]"), listOf(username, address, rejectReason), "Smartcityhub | Your property request was denied")
    }

    fun sendPropertyApproveEmail(username: String, address: String) {
        buildAndSendMail(EmailTemplateKey.PropertyApprove, listOf("[[USER]]", "[[ADDRESS]]"), listOf(username, address), "Smartcityhub | Your property request was approved")
    }
}