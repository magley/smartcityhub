package rs.nvt.smartcityhub.device.dto

const val DeviceCmdHistoryUserData_Automatic_ID = -1L
const val DeviceCmdHistoryUserData_Automatic_Name = "Automatic"
const val DeviceCmdHistoryUserData_Automatic_LastName = ""

/**
 * `DeviceCmdHistoryUserDataDTO` is the minimal data describing a user that has performed a command on a device.
 */
data class DeviceCmdHistoryUserDataDTO (
    /**
     * ID of the user. If the command is sent by the simulator, this should equal `-1`.
     */
    val id: Long,
    /**
     * Name of the user. If the command is sent by the simulator, this should be `"Automatic"`.
     */
    val name: String,
    /**
     * Last name of the user. If the command is sent by the simulator, this should be an empty string.
     */
    val lastName: String,
)