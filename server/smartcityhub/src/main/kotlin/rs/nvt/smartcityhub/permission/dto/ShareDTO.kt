package rs.nvt.smartcityhub.permission.dto

data class ShareDTO (
    val isShared: Boolean,
    val sharedByName: String,
    val sharedByLastName: String,
)