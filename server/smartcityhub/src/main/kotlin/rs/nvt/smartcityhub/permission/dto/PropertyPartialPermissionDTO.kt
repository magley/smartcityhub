package rs.nvt.smartcityhub.permission.dto

data class PropertyPartialPermissionDTO(val partialPermission: Boolean)
