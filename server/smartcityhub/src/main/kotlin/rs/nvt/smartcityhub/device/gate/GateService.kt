package rs.nvt.smartcityhub.device.gate

import com.fasterxml.jackson.databind.ObjectMapper
import org.eclipse.paho.mqttv5.common.MqttMessage
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import rs.nvt.smartcityhub.config.auth.IAuthenticationFacade
import rs.nvt.smartcityhub.config.mqtt.MqttClientWrapper
import rs.nvt.smartcityhub.device.IDeviceRepo
import rs.nvt.smartcityhub.device.ac.dto.cmd_history.ACCmdHistoryItemDTO
import rs.nvt.smartcityhub.device.common.DeviceServiceCommon
import rs.nvt.smartcityhub.device.dto.*
import rs.nvt.smartcityhub.device.lamp.LampCmdHistoryItemDTO
import rs.nvt.smartcityhub.property.Property
import rs.nvt.smartcityhub.user.IUserService
import rs.nvt.smartcityhub.user.User
import rs.nvt.smartcityhub.util.ImageSaveMode
import rs.nvt.smartcityhub.util.saveBase64ToImage

@Service
class GateService {
    @Autowired lateinit var mqttClient: MqttClientWrapper
    @Autowired lateinit var deviceRepo: IDeviceRepo
    @Autowired lateinit var authFacade: IAuthenticationFacade
    @Autowired lateinit var objectMapper: ObjectMapper
    @Autowired lateinit var cmdService: DeviceServiceCommon
    @Autowired lateinit var userService: IUserService

    fun registerGate(dto: GateRegisterDTO, property: Property): Gate {
        val device = Gate(dto.base, property)
        device.plates = dto.plates
        deviceRepo.save(device)
        saveBase64ToImage(dto.base.imageB64, "device_${device.id}", ImageSaveMode.PHOTO)
        return device
    }

    fun opening(device: Gate, uuid: String) {
        publishCommand(device, GateCommandType.open_state, GateOpen.opening, uuid)
    }

    fun closing(device: Gate, uuid: String) {
        publishCommand(device, GateCommandType.open_state, GateOpen.closing, uuid)
    }

    fun overrideYes(device: Gate, uuid: String) {
        publishCommand(device, GateCommandType.override, GateOverride.yes, uuid)
    }

    fun overrideNo(device: Gate, uuid: String) {
        publishCommand(device, GateCommandType.override, GateOverride.no, uuid)
    }

    fun setMode(device: Gate, mode: GateMode, uuid: String) {
        publishCommand(device, GateCommandType.mode, mode, uuid)
    }

    private fun publishCommand(device: Gate, type: GateCommandType, value: Any, uuid: String) {
        val userId = authFacade.getUser().id
        val cmd = GateCmdMqttDTO(userId, type, value, uuid)
        val msg = MqttMessage(objectMapper.writeValueAsBytes(cmd))

        mqttClient.publish("gate/cmd/${device.id}", msg)
    }

    fun getHistory(device: Gate, dto: DeviceCmdHistoryRequestBodyDTO): DeviceCmdHistoryDTO<GateCmdHistoryItemDTO_> {
        val items = cmdService.getCommandHistory(device, dto, GateCmdHistoryItemDTO::class.java, "gate_w")
        val meta = userService
            .findByRoleAndIdIn(User.Role.CLIENT, items.list.map { u -> u.userId })
            .map{u -> DeviceCmdHistoryUserDataDTO(u.id, u.name, u.lastName)}

        val items2 = items.list.map { o ->
            val u = meta.find { ud -> ud.id == o.userId } ?: DeviceCmdHistoryUserDataDTO(
                DeviceCmdHistoryUserData_Automatic_ID,
                DeviceCmdHistoryUserData_Automatic_Name,
                DeviceCmdHistoryUserData_Automatic_LastName
            )
            GateCmdHistoryItemDTO_(o, u)
        }

        return DeviceCmdHistoryDTO(items2, items.totalItems, items.pagination)
    }
}