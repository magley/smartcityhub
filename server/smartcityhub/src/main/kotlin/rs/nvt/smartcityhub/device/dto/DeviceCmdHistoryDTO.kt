package rs.nvt.smartcityhub.device.dto

import rs.nvt.smartcityhub.util.InfluxPaginator

/**
 * `DeviceCmdHistoryDTO` is the result body for a command history request.
 */
class DeviceCmdHistoryDTO<CmdHistoryItemDTO> (
    val list: List<CmdHistoryItemDTO>,
    val totalItems: Long,
    /**
     * Pagination data which must be equal to the one received in the http request.
     */
    val pagination: InfluxPaginator
)