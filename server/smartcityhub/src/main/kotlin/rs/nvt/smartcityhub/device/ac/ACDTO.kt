package rs.nvt.smartcityhub.device.ac

import rs.nvt.smartcityhub.device.dto.DeviceDTO

data class ACDTO (val base: DeviceDTO, val minTemperature: Double, val maxTemperature: Double, val supportedModes: List<ACMode>) {
    constructor(device: AC) : this(DeviceDTO(device), device.minTemperature, device.maxTemperature, device.supportedModes)
}