package rs.nvt.smartcityhub.device.common

import com.fasterxml.jackson.databind.ObjectMapper
import com.influxdb.annotations.Column
import com.influxdb.client.InfluxDBClient
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import rs.nvt.smartcityhub.config.influxdb.InfluxDbConfig
import rs.nvt.smartcityhub.device.Device
import rs.nvt.smartcityhub.device.dto.*
import rs.nvt.smartcityhub.exception.RequestException
import rs.nvt.smartcityhub.user.IUserService
import rs.nvt.smartcityhub.user.User
import rs.nvt.smartcityhub.util.InfluxPaginator
import java.text.SimpleDateFormat
import java.time.LocalDateTime


class InfluxMapperResult_Count {
    @Column(name = "count")
    var count: Long = 0
}

class InfluxMapperResult_UserId {
    @Column(name = "user_id")
    var userId: Long = -1
}

@Service
class DeviceServiceCommon {

    @Autowired private lateinit var userService: IUserService
    @Autowired private lateinit var influxDBClient: InfluxDBClient
    @Autowired private lateinit var objectMapper: ObjectMapper

    fun getCommandHistoryRowCount(baseQuery: String): Long {
        val queryRowCount = """
            $baseQuery
            |> set(key: "count", value: "") // Horrible hack.
            |> count(column: "count")
        """
        val totalLi = influxDBClient.queryApi.query(queryRowCount, InfluxMapperResult_Count::class.java)

        var total = InfluxMapperResult_Count()
        if (totalLi.size > 0) {
            total = totalLi[0]
        }

        return total.count
    }

    fun getUsersForQuery(userSearchQueries: List<Long>): List<User> {
        return userService.findByRoleAndIdIn(User.Role.CLIENT, userSearchQueries)
    }

    fun getCommandHistoryRequestBaseQuery(
        device: Device,
        dto: DeviceCmdHistoryRequestBodyDTO,
        influxMeasurementName: String
    ): String {
        val queryIds = dto.users.map { u -> u.id }
        val userIds = getUsersForQuery(queryIds).map { u -> u.id }.toMutableList()
        if (queryIds.contains(DeviceCmdHistoryUserData_Automatic_ID)) userIds.add(DeviceCmdHistoryUserData_Automatic_ID)
        val userIdsFluxArr = objectMapper.writeValueAsString(userIds)
        val shouldFilterByUser = dto.users.isNotEmpty()
        val userFilterString = "|> filter(fn: (r) => contains(set: $userIdsFluxArr, value: r.user_id))"
        val userFilterStringConditional = if (shouldFilterByUser) userFilterString else ""

        return """
        from(bucket: "${InfluxDbConfig.BUCKET_NVT_VAL}")
            |> range(start: ${dto.minDate.time / 1000}, stop: ${dto.maxDate.time / 1000})
            |> filter(fn: (r) => r._measurement == "$influxMeasurementName")
            |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
            |> filter(fn: (r) => r.device_id == "${device.id}")
            $userFilterStringConditional
            |> group(columns: ["device_id"])
            |> drop(columns: ["_start", "_stop", "host", "topic", "_measurement", "device_id"]) 
            |> map(fn: (r) => ({ r with is_on: bool(v: r.is_on) }))
        """
    }

    fun <T_DTO> getCommandHistory(baseQuery: String, pagination: InfluxPaginator, clazz: Class<T_DTO>): List<T_DTO> {
        val query = """
            $baseQuery
            |> sort(columns: ["${pagination.sortColumn}"], desc: ${pagination.sortDesc})
            |> limit(offset: ${pagination.offset}, n: ${pagination.pageSize}) 
        """

        println(query)

        return influxDBClient.queryApi.query(query, clazz)
    }

    fun <T_DTO> getCommandHistory(
        device: Device,
        dto: DeviceCmdHistoryRequestBodyDTO,
        clazz: Class<T_DTO>,
        influxMeasurementName: String
    ): DeviceCmdHistoryDTO<T_DTO> {
        if (dto.minDate == dto.maxDate || dto.minDate > dto.maxDate) {
            throw RequestException(
                HttpStatus.BAD_REQUEST,
                "Invalid range. Start date must be strictly BEFORE end date."
            )
        }

        val baseQuery = getCommandHistoryRequestBaseQuery(device, dto, influxMeasurementName)
        val records = getCommandHistory(baseQuery, dto.pagination, clazz)
        val totalRows = getCommandHistoryRowCount(baseQuery)

        return DeviceCmdHistoryDTO(records, totalRows, dto.pagination)
    }
}