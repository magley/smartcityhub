package rs.nvt.smartcityhub.property

import jakarta.persistence.*
import jakarta.validation.Valid
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import rs.nvt.smartcityhub.city.City
import rs.nvt.smartcityhub.property.dto.RequestPropertyDTO
import rs.nvt.smartcityhub.user.User

@Embeddable
data class Status(var state: State, var denyReason: String?) {
    enum class State {
        PENDING, APPROVED, DENIED
    }
}

@Entity
@Table(name = "properties")
@Cache(region = "Property", usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
class Property {
    enum class Type {
        House, Apartment
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0

    @ManyToOne
    lateinit var owner: User

    @Enumerated
    lateinit var type: Type

    lateinit var address: String

    @ManyToOne
    lateinit var city: City

    var size: Double = 0.0

    var floors: Int = 0

    @Valid
    @Embedded
    lateinit var location: GeoCoordinate

    @Embedded
    lateinit var status: Status

    constructor() {}

    constructor(owner: User, city: City, dto: RequestPropertyDTO) {
        this.owner = owner
        this.type = dto.type
        this.address = dto.address
        this.city = city
        this.size = dto.size
        this.floors = dto.floors
        this.location = dto.location
        this.status = Status(Status.State.PENDING, null)
    }
}