package rs.nvt.smartcityhub.device.dht

import io.swagger.v3.oas.annotations.Operation
import jakarta.validation.Valid
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import rs.nvt.smartcityhub.config.auth.IAuthenticationFacade
import rs.nvt.smartcityhub.device.DeviceAccessControl
import rs.nvt.smartcityhub.device.DeviceType
import rs.nvt.smartcityhub.device.IDeviceService
import rs.nvt.smartcityhub.device.dht.dto.DHTHistoryItemDTO
import rs.nvt.smartcityhub.device.dto.DeviceMeasurementHistoryRequestBodyDTO

@RestController
@RequestMapping("/api/device/dht")
class DHTController {
    @Autowired lateinit var deviceService: IDeviceService
    @Autowired lateinit var authFacade: IAuthenticationFacade
    @Autowired lateinit var deviceAccessControl: DeviceAccessControl
    @Autowired lateinit var dhtService: DHTService

    @Operation(summary = "Get history of temperature and humidity readings for this sensor")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PutMapping("/history")
    fun getDHTHistory(@Valid @RequestBody dto: DeviceMeasurementHistoryRequestBodyDTO): ResponseEntity<Collection<DHTHistoryItemDTO>> {
        val device = deviceService.find(dto.id)
        deviceAccessControl.assertOwnership(authFacade.getUser(), device)
        deviceAccessControl.assertType(device, DeviceType.dht)
        return ResponseEntity(dhtService.getHistoricalData(device, dto), HttpStatus.OK)
    }
}