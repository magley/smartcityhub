package rs.nvt.smartcityhub.permission

import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface IPropertyPermissionRepo : JpaRepository<PropertyPermission, Long> {
    fun findByUserIdAndPropertyId(userId: Long, propertyId: Long): PropertyPermission?
    fun countByPropertyId(deviceId: Long): Long
    fun findByPropertyId(propertyId: Long): List<PropertyPermission>
    fun findByPropertyId(propertyId: Long, pageable: Pageable): List<PropertyPermission>
    fun findByUserId(id: Long): List<PropertyPermission>
    fun findByUserId(id: Long, pageable: Pageable): List<PropertyPermission>
}