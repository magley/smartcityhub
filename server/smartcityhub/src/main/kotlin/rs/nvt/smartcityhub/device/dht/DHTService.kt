package rs.nvt.smartcityhub.device.dht

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import rs.nvt.smartcityhub.device.Device
import rs.nvt.smartcityhub.device.common.DeviceMeasurementServiceCommon
import rs.nvt.smartcityhub.device.dht.dto.DHTHistoryItemDTO
import rs.nvt.smartcityhub.device.dht.dto.DHTHistoryItemMeasurementDTO
import rs.nvt.smartcityhub.device.dto.DeviceMeasurementHistoryRequestBodyDTO

@Service
class DHTService {
    @Autowired lateinit var measurementService: DeviceMeasurementServiceCommon
    fun getHistoricalData(device: Device, dto: DeviceMeasurementHistoryRequestBodyDTO): List<DHTHistoryItemDTO> {
        return measurementService.getMeasurementHistory2(
            device.id,
            dto,
            "dht",
            DHTHistoryItemMeasurementDTO::class.java).map { d -> DHTHistoryItemDTO(d) }
    }
}