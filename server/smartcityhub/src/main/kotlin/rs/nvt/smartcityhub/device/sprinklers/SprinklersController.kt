package rs.nvt.smartcityhub.device.sprinklers

import io.swagger.v3.oas.annotations.Operation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import rs.nvt.smartcityhub.config.auth.IAuthenticationFacade
import rs.nvt.smartcityhub.device.DeviceAccessControl
import rs.nvt.smartcityhub.device.DeviceType
import rs.nvt.smartcityhub.device.IDeviceService
import rs.nvt.smartcityhub.device.common.DeviceServiceCommon
import rs.nvt.smartcityhub.device.dto.DeviceCmdHistoryDTO
import rs.nvt.smartcityhub.device.dto.DeviceCmdHistoryRequestBodyDTO

@RestController
@RequestMapping("/api/device/sprinklers")
class SprinklersController {
    @Autowired lateinit var sprinklersService: SprinklersService
    @Autowired lateinit var deviceService: IDeviceService
    @Autowired lateinit var authFacade: IAuthenticationFacade
    @Autowired lateinit var deviceAccessControl: DeviceAccessControl
    @Autowired lateinit var cmdService: DeviceServiceCommon

    @Operation(summary = "Get history of actions for these sprinklers")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PostMapping("/command-history")
    fun getHistory(@RequestBody dto: DeviceCmdHistoryRequestBodyDTO): ResponseEntity<DeviceCmdHistoryDTO<SprinklersCmdHistoryItemDTO_>> {
        val device = getDeviceWithAssertion(dto.id)
        val result = sprinklersService.getHistory(device, dto)
        return ResponseEntity(result, HttpStatus.OK)
    }

    @Operation(summary = "Turn the sprinklers on")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PutMapping("/start")
    fun setStart(@RequestBody dto: SprinklersCmdStartDTO): ResponseEntity<Void> {
        val device = getDeviceWithAssertion(dto.id)
        when (dto.start) {
            true -> sprinklersService.start(device, dto.uuid)
            false -> sprinklersService.stop(device, dto.uuid)
        }
        return ResponseEntity(null, HttpStatus.NO_CONTENT)
    }

    @Operation(summary = "Submit automatic triggers for these sprinklers")
    @PreAuthorize("hasAnyRole('CLIENT')")
    @PutMapping("/triggers")
    fun setTriggers(@RequestBody dto: SprinklersCmdTriggersDTO): ResponseEntity<Void> {
        val device = getDeviceWithAssertion(dto.id)
        sprinklersService.setTriggers(device, dto.triggers, dto.uuid)
        return ResponseEntity(null, HttpStatus.NO_CONTENT)
    }

    private fun getDeviceWithAssertion(id: Long): Sprinklers {
        val device = deviceService.find(id)
        deviceAccessControl.assertOwnership(authFacade.getUser(), device)
        deviceAccessControl.assertType(device, DeviceType.sprinkler)
        return device as Sprinklers
    }
}