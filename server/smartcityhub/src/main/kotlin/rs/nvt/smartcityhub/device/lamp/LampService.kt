package rs.nvt.smartcityhub.device.lamp

import com.fasterxml.jackson.databind.ObjectMapper
import org.eclipse.paho.mqttv5.common.MqttMessage
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import rs.nvt.smartcityhub.config.auth.IAuthenticationFacade
import rs.nvt.smartcityhub.config.mqtt.MqttClientWrapper
import rs.nvt.smartcityhub.device.Device
import rs.nvt.smartcityhub.device.ac.dto.cmd_history.ACCmdHistoryItemDTO
import rs.nvt.smartcityhub.device.ac.dto.cmd_history.ACCmdHistoryItemDTO_
import rs.nvt.smartcityhub.device.common.DeviceMeasurementServiceCommon
import rs.nvt.smartcityhub.device.common.DeviceServiceCommon
import rs.nvt.smartcityhub.device.dto.*
import rs.nvt.smartcityhub.user.IUserService
import rs.nvt.smartcityhub.user.User

@Service
class LampService {
    @Autowired lateinit var mqttClient: MqttClientWrapper
    @Autowired lateinit var authFacade: IAuthenticationFacade
    @Autowired lateinit var objectMapper: ObjectMapper
    @Autowired lateinit var measurementService: DeviceMeasurementServiceCommon
    @Autowired lateinit var cmdService: DeviceServiceCommon
    @Autowired lateinit var userService: IUserService

    fun getMeasurementHistoricalData(device: Device, dto: DeviceMeasurementHistoryRequestBodyDTO): List<LampMeasurementHistoryItemDTO> {
        return measurementService.getMeasurementHistory2(
            device.id,
            dto,
            "lamp_m",
            LampHistoryItemMeasurementDTO::class.java).map { d -> LampMeasurementHistoryItemDTO(d) }
    }

    fun start(device: Lamp, uuid: String) {
        publishCommand(device, LampCommandType.is_on, true, uuid)
    }

    fun stop(device: Lamp, uuid: String) {
        publishCommand(device, LampCommandType.is_on, false, uuid)
    }

    fun setMode(device: Lamp, mode: LampMode, threshold: Double, uuid: String) {
        publishCommand(device, LampCommandType.mode, arrayOf(mode, threshold), uuid)
    }

    private fun publishCommand(device: Lamp, type: LampCommandType, value: Any, uuid: String) {
        val userId = authFacade.getUser().id
        val cmd = LampCmdMqttDTO(userId, type, value, uuid)
        val msg = MqttMessage(objectMapper.writeValueAsBytes(cmd))

        mqttClient.publish("lamp/cmd/${device.id}", msg)
    }

    fun getHistory(device: Lamp, dto: DeviceCmdHistoryRequestBodyDTO): DeviceCmdHistoryDTO<LampCmdHistoryItemDTO_> {
        val items = cmdService.getCommandHistory(device, dto, LampCmdHistoryItemDTO::class.java, "lamp_w")
        val meta = userService
            .findByRoleAndIdIn(User.Role.CLIENT, items.list.map { u -> u.userId })
            .map{u -> DeviceCmdHistoryUserDataDTO(u.id, u.name, u.lastName)}

        val items2 = items.list.map { o ->
            val u = meta.find { ud -> ud.id == o.userId } ?: DeviceCmdHistoryUserDataDTO(
                DeviceCmdHistoryUserData_Automatic_ID,
                DeviceCmdHistoryUserData_Automatic_Name,
                DeviceCmdHistoryUserData_Automatic_LastName
            )
            LampCmdHistoryItemDTO_(o, u)
        }

        return DeviceCmdHistoryDTO(items2, items.totalItems, items.pagination)
    }
}