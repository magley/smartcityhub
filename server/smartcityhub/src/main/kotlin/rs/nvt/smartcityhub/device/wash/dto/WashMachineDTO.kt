package rs.nvt.smartcityhub.device.wash.dto

import rs.nvt.smartcityhub.device.ac.AC
import rs.nvt.smartcityhub.device.ac.ACMode
import rs.nvt.smartcityhub.device.dto.DeviceDTO
import rs.nvt.smartcityhub.device.wash.WashMachine
import rs.nvt.smartcityhub.device.wash.WashMachineMode

data class WashMachineDTO(val base: DeviceDTO, val supportedModes: List<WashMachineMode>) {
    constructor(device: WashMachine) : this(DeviceDTO(device), device.supportedModes)
}