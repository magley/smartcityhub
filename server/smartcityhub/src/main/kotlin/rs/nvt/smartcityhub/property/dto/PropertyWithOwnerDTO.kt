package rs.nvt.smartcityhub.property.dto

import rs.nvt.smartcityhub.property.Property
import rs.nvt.smartcityhub.user.dto.UserInfoDTO

data class PropertyWithOwnerDTO(val owner: UserInfoDTO, val property: PropertyDTO) {
    constructor(p: Property) : this(UserInfoDTO(p.owner), PropertyDTO(p, null))
}