import argparse
import os
parser = argparse.ArgumentParser()
parser.add_argument("superadminpass",  type=str, default=False)
args = parser.parse_args()

if args.superadminpass:
    fname = "./config/superadminpass"
    try:
        with open(fname, 'r') as file:
            data = file.read()
            print(data)
    except FileNotFoundError as e:
        print("Admin config does not exist")