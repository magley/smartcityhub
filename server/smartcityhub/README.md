# Backend

The backend is powered by Spring Boot in Kotlin, built using Gradle.

Make sure the program is synced with the latest gradle file.

# Building the backend

1) Navigate to `server/smartcityhub`
2) Build the server: `gradlew build -x test`
3) Navigate to `build/libs`
4) There are several files needed during runtime. Copy them and put them in the same directory as the `.jar`
    - `redisson.yaml` - there is currently an issue with loading it directly from a .jar because of the library and `application.properties`
    - `data/` directory - we opted for keeping the email templates outside `resources` because of volatility
    - `config/` directory - see below
3) Run the server: `java -jar smartcityhub-0.0.1-SNAPSHOT.jar` 

# Config directory

The app uses external configuration files containing secrets and API keys:

- `config/influxdb.env`

```
URL=http://127.0.0.1:8086
ORGANIZATION=nvt
TOKEN=[token]
BUCKET_NVT=nvtBucket
```

- `config/sendgrid.env`

```
KEY=[sendgrid private key]
FROM=[email]
```

- `config/superadminpass`

This is created by Smartcityhub. Run the `manage.py superadminpass` script (must be in same directory as `config/`) to view remotely.

# Database

[MariaDB version 10.11.5](https://mariadb.org/download/?t=mariadb&p=mariadb&r=10.11.5&os=windows&cpu=x86_64&pkg=msi&m=bme)

Setting up:

1) Download and install MariaDB
2) You will be prompted to enter root username and password. 
Those credentials must match the ones in `application.properties`.
3) 
    - Add the MariaDB's `/bin` folder to PATH if the installer doesn't do it automatically
    - **OR**
    - open a terminal in the `/bin` folder and:
4) Run `mysql -u root -p`. You will be asked to enter the password.
5) Once you connect, type `create database nvtDB;`.
The name of the database must match the one in `spring.datasource.url` from `application.properties`.

Viewing the database:

1) Open HeidiSQL (should be installed with MariaDB)
2) Create a new connection. Enter the proper username, password and database name. Leave everything else as-is.

# API

OpenAPI 3 compliant.

Swagger is at: http://localhost:8080/swagger-ui/index.html.

Api yaml is at: http://localhost:8080/v3/api-docs